/*
 * Video On Demand Samples
 *
 * Copyright (C) 2015 Microchip Technology Germany II GmbH & Co. KG
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * You may also obtain this software under a propriety license from Microchip.
 * Please contact Microchip for further information.
 *
 */

/*----------------------------------------------------------*/
/*! \file
 *  \brief  This file contains the CSafeVector class.
 */
/*----------------------------------------------------------*/

#ifndef SAFE_VECTOR_H
#define SAFE_VECTOR_H

#include <stdbool.h>
#include <stdint.h>
#include <stddef.h>
#include <semaphore.h>
#include <vector>
#include "SafeVector.h"

using namespace std;

/*----------------------------------------------------------*/
/*! \brief Thread safe vector class.
 */
/*----------------------------------------------------------*/
template<typename T>
class CSafeVector
{
private:
    bool initialized;
    std::vector<T> vec;
    sem_t sem;
public:
	/*----------------------------------------------------------*/
    /*! \brief Default constructor of CSafeVector.
     */
    /*----------------------------------------------------------*/
    CSafeVector() : initialized(false) 
    { 
        sem_init( &sem, 0, 1 ); //Mutex, initialized to 1 => sem_wait will block at 2nd call
    }
    
	/*----------------------------------------------------------*/
    /*! \brief Default destructor of CSafeVector.
     */
    /*----------------------------------------------------------*/
    ~CSafeVector()
    {
        vec.empty();
        sem_destroy( &sem);
    }
    
	/*----------------------------------------------------------*/
    /*! \brief Stores the given object into the last postion of the vector.
     */
    /*----------------------------------------------------------*/
    void PushBack(T obj)
    {
        if( NULL == obj )
            return;
        sem_wait( &sem );
        vec.push_back( obj );
        initialized = true;
        sem_post( &sem );
    }
	
	/*----------------------------------------------------------*/
    /*! \brief Gets the first element from the vector and dequeue it.
     *  \return The first object if exists, otherwise NULL,
     */
    /*----------------------------------------------------------*/
    T PopFront()
    {
        T obj = NULL;
        sem_wait( &sem );
        if (vec.size() >= 1)
        {
            obj = vec[0];
            vec.erase( vec.begin() );
        }
        sem_post( &sem );
        return obj;
    }
    
	/*----------------------------------------------------------*/
    /*! \brief Gets the first element from the vector and dequeue it.
     *  \return The first object if exists, otherwise NULL,
     */
    /*----------------------------------------------------------*/
    T operator[](uint32_t p)
    {
        if( !initialized )
            return NULL;
        T obj = NULL;
        sem_wait( &sem );
        uint32_t s = vec.size();
        if (0 != s && p < s)
        {
            obj = vec[p];
        }
        sem_post( &sem );
        return obj;
    }
    
	/*----------------------------------------------------------*/
    /*! \brief Gets the amount of objects stored in the vector.
     *  \return The amount of objects available.
     */
    /*----------------------------------------------------------*/
    uint32_t Size()
    {
        uint32_t s;
        if( !initialized )
            return 0;
        
        sem_wait( &sem );
        s = vec.size();
        sem_post( &sem );
        return s;
    }
    
	/*----------------------------------------------------------*/
    /*! \brief Safley removes the given object from the vector.
     *  \note If not found, it will be silently ignored.
	 *  \note The memory of the object will NOT be freed!
     */
    /*----------------------------------------------------------*/
    void Remove(T obj)
    {
        int32_t deleteIndex = -1;
        if( !initialized || NULL == obj )
            return;

        sem_wait( &sem );
        for( uint32_t i = 0; i < vec.size(); i++ )
        {
            if( obj == vec[i] )
            {
                deleteIndex = i;
                break;
            }
        }
        if( -1 != deleteIndex )
        {
            vec.erase( vec.begin() + deleteIndex );
        }
        sem_post( &sem );
    }
    
	
	/*----------------------------------------------------------*/
    /*! \brief Safley removes the given object from the vector.
     *  \note If not found, it will be silently ignored.
	 *  \param destroy - if set to true, the memory will be freed for each deleted object.
     */
    /*----------------------------------------------------------*/
    void RemoveAll(bool destroy)
    {
        if( !initialized )
            return;
        sem_wait( &sem );
        for( uint32_t i = 0; destroy && i < vec.size(); i++ )
        {
            T obj = vec[i];
            delete obj;
        }
        vec.clear();
        sem_post( &sem );
    }
};

#endif //SAFE_VECTOR_H