/*
 * Video On Demand Samples
 *
 * Copyright (C) 2015 Microchip Technology Germany II GmbH & Co. KG
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * You may also obtain this software under a propriety license from Microchip.
 * Please contact Microchip for further information.
 *
 */

/*----------------------------------------------------------*/
/*! \file
 *  \brief  This file contains the CIndustrialStack class (Generic commands).
 */
/*----------------------------------------------------------*/

#ifndef INDUSTRIAL_STACK_API_GENERIC_H
#define INDUSTRIAL_STACK_API_GENERIC_H

#include "IndustrialStack.h"
#include "Network.h"
#include "Types.h"

class CGeneric_SendConfigOk : public CISSendMostMsgElement
{
public:
    CGeneric_SendConfigOk(bool isOkay)
    {
        ElementName = "CGeneric_SendConfigOk";
        
        Request.IsValid = true;
        Request.SourceAddress = 0x2;
        Request.TargetAddress = 0x3C8;
        Request.FBlock = 0x2;
        Request.Func = 0xA00;
        Request.Inst = 0x00;
        Request.OpType = CISOpType_STATUS;
        Request.PayloadLen = 1;
        Request.Payload[0] = isOkay;
    }
};

class CGeneric_GetMacAddress : public CISSendMostMsgElement
{
public:
    CGeneric_GetMacAddress(uint16_t nodeAddress)
    {
        ElementName = "CGeneric_GetMacAddress";
        
        Request.IsValid = true;
        Request.SourceAddress = 0x2;
        Request.TargetAddress = nodeAddress;
        Request.FBlock = 0x1;
        Request.Func = 0x013;
        Request.Inst = 0x00;
        Request.OpType = CISOpType_GET;
        Request.PayloadLen = 0;
    }
};

class CGeneric_GetGroupAddress : public CISSendMostMsgElement
{
public:
    CGeneric_GetGroupAddress(uint16_t nodeAddress)
    {
        ElementName = "CGeneric_GetGroupAddress";
        
        Request.IsValid = true;
        Request.SourceAddress = 0x2;
        Request.TargetAddress = nodeAddress;
        Request.FBlock = 0x1;
        Request.Func = 0x004;
        Request.Inst = 0x00;
        Request.OpType = CISOpType_GET;
        Request.PayloadLen = 0;
    }
};

class CGeneric_SendMostMessage : public CISSendMostMsgElement
{
public:
    CGeneric_SendMostMessage(const uint8_t *pPayload, uint32_t payloadLength,
        uint16_t nodeAddress, uint8_t fblockId, uint8_t instanceId, 
        uint16_t functionId, uint8_t opTypeRequest, uint8_t opTypeResponse) 
    {
        if (payloadLength > sizeof(Request.Payload))
        {
            ConsolePrintf(PRIO_ERROR, RED"CGeneric_SendMostMessage was called"\
                " with to big payload size"RESETCOLOR"\n");
            payloadLength = sizeof(Request.Payload);
        }
        ElementName = "CGeneric_SendMostMessage";
        
        Request.IsValid = true;
        Request.SourceAddress = 0x2;
        Request.TargetAddress = nodeAddress;
        Request.FBlock = fblockId;
        Request.Func = functionId;
        Request.Inst = instanceId;
        Request.OpType = (CISOpType_t)opTypeRequest;
        Request.PayloadLen = payloadLength;
        if (0 != payloadLength)
            memcpy(Request.Payload, pPayload, payloadLength);
        if( 0xFF != opTypeResponse )
        {
            WaitForResponse = true;
            WaitForResponseOpType = (CISOpType_t)opTypeResponse;
        }
    }
};

#endif //INDUSTRIAL_STACK_API_GENERIC_H
