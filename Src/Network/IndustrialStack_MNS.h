/*
 * Video On Demand Samples
 *
 * Copyright (C) 2015 Microchip Technology Germany II GmbH & Co. KG
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * You may also obtain this software under a propriety license from Microchip.
 * Please contact Microchip for further information.
 *
 */

/*----------------------------------------------------------*/
/*! \file
 *  \brief  This file contains the CIndustrialStack class (NetService part).
 */
/*----------------------------------------------------------*/

#ifndef INDUSTRIAL_STACK_MNS_H
#define INDUSTRIAL_STACK_MNS_H

#include <assert.h>
#include "IndustrialStack_Types.h"
#include "IndustrialStack_LLD.h"
#include "mns_types_cfg.h"
#include "mnsl.h"

#define MNSW_TESTPATTERN ((uint32_t)0xA1B2C3D4)

class IISNetServiceWrapperCB
{
public:
    virtual void OnReceivedMostMessage(CISMostMsg *rcvMessage) = 0;
    virtual void OnSyncStateChanged(bool isSynced) = 0;
    virtual void OnControlReadEnd() = 0;
};

class CISNetServiceWrapper : public CIndustrialStackLldCB
{
public:
    const uint32_t testPattern;
    bool inicWriteError;
    Mns_Lld_Api_t *pMnsInterface;
    void *pMnsInst; 
    CTransceiver *icm_inst_ptr;
    CTransceiver *rcm_inst_ptr;
    CTransceiver *mcm_inst_ptr;
private:
    CMnsl mnsl;
    Mns_Lld_Callbacks_t pLldCb;
    bool isSynced;
    IISNetServiceWrapperCB *wrapperCB;
    CIndustrialStackLld *lld;

public:    
    CISNetServiceWrapper(uint8_t deviceApi, int controlRxHandle, int controlTxHandle);
    virtual ~CISNetServiceWrapper();
    void ServiceMns();
    void AddListener(IISNetServiceWrapperCB *rcvListener);
    
    bool SendMostMessage(CISMostMsg *sndMessage);
    void Unsynchronize();
    
public:
    //Will be called from LLD, do not call directly
    virtual void OnReadThreadEnd(CIndustrialStackLld *lld);
    //All this functions will be called by the C wrappers, do not call them directly
    void OnCtrlTxTransmit( Mns_Lld_TxMsg_t *msg_ptr);
    void OnMnslEvent( Mnsl_Event_t event_code );
    void OnMessage( Msg_MostTel_t *pRx, CTransceiver *pTr );
};

#endif //INDUSTRIAL_STACK_MNS_H
