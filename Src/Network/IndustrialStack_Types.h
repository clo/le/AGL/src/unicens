/*
 * Video On Demand Samples
 *
 * Copyright (C) 2015 Microchip Technology Germany II GmbH & Co. KG
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * You may also obtain this software under a propriety license from Microchip.
 * Please contact Microchip for further information.
 *
 */

/*----------------------------------------------------------*/
/*! \file
 *  \brief  This file contains the CIndustrialStack class (type defnitions and common classes).
 */
/*----------------------------------------------------------*/

#ifndef INDUSTRIAL_STACK_TYPES_H
#define INDUSTRIAL_STACK_TYPES_H

#include <stdint.h>
#include <stdbool.h>
#include <stddef.h>
#include "SafeVector.h"
typedef enum
{
   ISReturn_NoChange,  
   ISReturn_Success,
   ISReturn_Failure,
   ISReturn_Timeout
} ISReturn_t;

typedef enum
{
    CISOpType_INVALID = 0xFF,
    CISOpType_SET = 0x0,
    CISOpType_GET = 0x1,
    CISOpType_SETGET = 0x2,
    CISOpType_INC = 0x3,
    CISOpType_DEC = 0x4,
    CISOpType_GETINTERFACE = 0x5,
    CISOpType_STATUS = 0xC,
    CISOpType_INTERFACE = 0xE,
    CISOpType_ERROR = 0xF,
    CISOpType_START = 0x0,
    CISOpType_ABORT = 0x1,
    CISOpType_STARTRESULT = 0x2,
    CISOpType_STARTRESULTACK = 0x6,
    CISOpType_ABORTACK = 0x7,
    CISOpType_STARTACK = 0x8,
    CISOpType_ERRORACK = 0x9,
    CISOpType_PROCESSINGACK = 0xA,
    CISOpType_PROCESSING = 0xB,
    CISOpType_RESULT = 0xC,
    CISOpType_RESULTACK = 0xD,
    CISOpType_REPORTS = 0x9
} CISOpType_t;

class CIndustrialStack;
class IISElement;
class IISElementCallback;
class CISWaitElement;
class CISMostMsg;
class CSInternalEvent;
class CISSendMostMsgElement;
class CISDeviceQueue;
class CIndustrialStackLldCB;
class CV1_OnMostRx;
class CV2_OnMostRx;
class CV3_OnMostRx;

class IISElement
{
private:
    int32_t refCount;
public:
    const char *ElementName;
    IISElementCallback *Callback;
    IISElement() : refCount(1), ElementName("Not set"), Callback(NULL)  { }
    virtual ~IISElement() {}
    
    virtual ISReturn_t Service(CIndustrialStack *iStack, uint32_t time) = 0;
    virtual ISReturn_t OnMostMessage(CIndustrialStack *iStack, CISMostMsg *rcvMessage) = 0;
    
    void AddReference()
    {
        ++refCount;
    }

    void RemoveReference()
    {
        if( 0 == --refCount )
            delete this;
    }
};

class CISMostMsg
{
#define MAX_PAYLOAD_SIZE 45
public:
    bool IsValid;
    uint32_t SourceAddress;
    uint32_t TargetAddress;
    uint32_t FBlock;
    uint32_t Func;
    uint32_t Inst;
    CISOpType_t OpType;
    uint32_t PayloadLen;
    uint8_t Payload[MAX_PAYLOAD_SIZE];
    
    CISMostMsg() : IsValid(false), SourceAddress(0xFFFFFFFF), TargetAddress(0xFFFFFFFF),
        FBlock(0xFFFFFFFF), Func(0xFFFFFFFF), Inst(0xFFFFFFFF), 
        OpType(CISOpType_INVALID), PayloadLen(0)
    { }
    
    void DeepCopy(CISMostMsg *msg)
    {
        IsValid = msg->IsValid;
        SourceAddress = msg->SourceAddress;
        TargetAddress = msg->TargetAddress;
        FBlock = msg->FBlock;
        Func = msg->Func;
        Inst = msg->Inst;
        OpType = msg->OpType;
        PayloadLen = msg->PayloadLen;
        memcpy(Payload, msg->Payload, PayloadLen);
    }
};

class IISElementCallback
{
public:
    virtual void ElementProcessed(CIndustrialStack *iStack, ISReturn_t result, IISElement *element) = 0;
};

class CISDeviceQueue
{
private:
    uint16_t nodeAddress;
public:
    CSafeVector<IISElement *> elements;
    
    CISDeviceQueue(uint16_t address) : nodeAddress(address) { }
    uint16_t GetNodeAddress() { return nodeAddress; }
};

#endif //INDUSTRIAL_STACK_TYPES_H