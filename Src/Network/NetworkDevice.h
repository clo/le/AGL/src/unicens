/*
 * Video On Demand Samples
 *
 * Copyright (C) 2015 Microchip Technology Germany II GmbH & Co. KG
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * You may also obtain this software under a propriety license from Microchip.
 * Please contact Microchip for further information.
 *
 */

/*----------------------------------------------------------*/
/*! \file
 *  \brief  This file contains the CNetworkDevice class.
 */
/*----------------------------------------------------------*/
#ifndef _NETWORKDEVICE_H_
#define _NETWORKDEVICE_H_

#include <stdint.h>
#include <stdbool.h>
#include <semaphore.h>
#include "SafeVector.h"
#include "ScriptXml.h"
#include "Types.h"
#include "Thread.h"
#include "NetworkDeviceListener.h"
#include "IndustrialStack.h"
#include "IndustrialStack_Types.h"

typedef enum
{
    NetworkState_Unknown,
    NetworkState_Unavailable,
    NetworkState_Available,
    NetworkState_ShutdownInProgress
} NetworkState_t;

/*----------------------------------------------------------*/
/*! \brief This class represents an INIC instance.
 *         It will be multiple instanced, for each connected OS81118.
 *         It generates INIC requests and parses INIC responses.
 */
/*----------------------------------------------------------*/
class CNetworkDevice : public CThread, public CSInternalEvent
{
private:
    CIndustrialStack *iStack;
    CV1_OnMostRx *v1Events;
    CV3_OnMostRx *v3Events;
    bool promiscuous;
    NetworkState_t netState;
    void *pInic;
    void *pScriptMemoryArea;
    bool isRunning;
    bool isActionPending;
    uint8_t deviceIndex;
    uint8_t deviceApi;
    bool isTimingMaster;
    uint32_t asyncBandwidth;
    sem_t methodMutex;
    int controlEndpointRxHandle;
    int controlEndpointTxHandle;
    char *responseToWaitFor[64];
    char deviceNameControlRx[64];
    char deviceNameControlTx[64];
    CSafeVector<CNetworkDeviceListener *> allListeners;

    uint16_t startV3autoForced;
    uint16_t startV3packetBW;

    bool OpenDevice( const char *systemNameControlRx, const char *systemNameControlTx, const char *linkNameControlRx,
        const char *linkNameControlTx );
    bool OpenMostControlChannel( void );
    void CloseMostControlChannel( void );
    void WaitForIpcResponse( const char *responseMessage );
    void ExecuteMcmScript( uint16_t nodeAddress, CSciptXml *scriptXml );
public:
    /*----------------------------------------------------------*/
    /*! \brief Constructor of CNetworkDevice.
     *  \param deviceIndex - The index of the physical device instance of the server device.
     *                      Starting at 0 for the first device instance.
     *  \param deviceApi - 1: OS81092, OS81110; 2: OS81118-C-Rev; 3:OS81118-D-Rev 
     *  \param isTimingMaster - true, if this device shall act as timing master, otherwise it is timing slave
     *  \param asyncBandwidth - if set as timing master, how many bytes shall be reserved for the asynchronous channel
     *  \param promiscuousMode - true, promiscuous mode. false, perfect match filter with multicast and broadcast enabled. If enabled, tools like Wireshark will capture every packet.
     */
    /*----------------------------------------------------------*/
    CNetworkDevice( uint8_t deviceIndex, uint8_t deviceApi, bool isTimingMaster, uint32_t asyncBandwidth,
        bool promiscuousMode );


    /*----------------------------------------------------------*/
    /*! \brief Destructor of CNetworkDevice.
     */
    /*----------------------------------------------------------*/
    virtual ~CNetworkDevice();


    /*----------------------------------------------------------*/
    /*! \brief Function of background thread.
     *  \note Do never call this method. It is used by internal threads.
     */
    /*----------------------------------------------------------*/
    void Run();


    /*----------------------------------------------------------*/
    /*! \brief Opens the USB connection to the INIC.
     *  \param controlRxEndpointAddress - The USB endpoint address of the RX direction.
     *  \param controlTxEndpointAddress - The USB endpoint address of the TX direction.
     *  \return true, if successful. false, otherwise.
     */
    /*----------------------------------------------------------*/
    bool OpenUsb( uint8_t controlRxEndpointAddress, uint8_t controlTxEndpointAddress );




    /*----------------------------------------------------------*/
    /*! \brief Opens the MLB connection to the INIC.
     *  \param controlRxChannelAddress - The MLB channel address of the RX direction.
     *  \param controlTxChannelAddress - The MLB channel address of the TX direction.
     *  \return true, if successful. false, otherwise.
     */
    /*----------------------------------------------------------*/
    bool OpenMlb( uint8_t controlRxChannelAddress, uint8_t controlTxChannelAddress );


    /*----------------------------------------------------------*/
    /*! \brief Opens the I2C connection to the INIC.
     *  \param inicApi - INIC API Version
     *  \return true, if successful. false, otherwise.
     */
    /*----------------------------------------------------------*/
    bool OpenI2C();


    /*----------------------------------------------------------*/
    /*! \brief Closes the USB connection to the INIC.
     */
    /*----------------------------------------------------------*/
    void Close();

    /*----------------------------------------------------------*/
    /*! \brief Returns the used API of the INIC.
     *  \return INIC API Version 1: OS81092, OS81110; 2: OS81118-C-Rev; 3:OS81118-D-Rev 
     */
    /*----------------------------------------------------------*/
    uint8_t GetDeviceApi();

    NetworkState_t GetNetState();
    void SetNetstate( NetworkState_t newState );
    void SetTimingMaster(bool tm);
    bool IsTimingMaster();
    void SetAsyncBandwidth(uint32_t bw);
    uint32_t GetAsyncBandwidth();

    /*----------------------------------------------------------*/
    /*! \brief Determines if there are jobs currently in the working queue.
     *  \note This method will block until the result is queried from the worker process.
     *  \return true, if there are pending actions enqueued.
     */
    /*----------------------------------------------------------*/
    bool ActionsPending();



    /*----------------------------------------------------------*/
    /*! \brief Remove all pending actions from the working queue.
     */
    /*----------------------------------------------------------*/
    void ClearAllPendingActions();



    /*----------------------------------------------------------*/
    /*! \brief Configure the Linux character device driver to handle an USB connection.
     *  \note This overloaded function is used to configure synchronous channels.
     *  \param aimType - Specifies the type of used Linux Driver Application Interface Module.
     *  \param endpointAddress - The USB endpoint address.
     *  \param epType - The data type, which then will be transmitted on this endpoint.
     *  \param epDir - The Direction of the stream. (EPDIR_IN == Data from the INIC to the EHC, EPDIR_OUT == Data from the EHC to the INIC)
     *  \param deviceName - On successful configuration, this method stores the name of the character device into the given buffer.
     *  \param deviceNameBufLen - The length of the given buffer.
     *  \param amountOfBuffers - The amount of buffers used in the driver.
     *  \param bufferSize - The amount of bytes used in a single buffer.
     *  \param subbufferSize - The amount of bytes used for a single subbuffer. For isoc channel this maybe 188 or 196 byte.
     *  \param packetsPerTransaction - The amount of packets per transaction.
     *  \return true, if successful. false, otherwise.
     */
    /*----------------------------------------------------------*/
    bool ConfigureUsbEndpoint( AimType_t aimType, uint8_t endpointAddress, EPDataType_t epType, EPDirection_t epDir,
        char *deviceName, uint32_t deviceNameBufLen, uint32_t amountOfBuffers, uint32_t bufferSize,
        uint32_t subbufferSize, uint32_t packetsPerTransaction );



    /*----------------------------------------------------------*/
    /*! \brief Configure the Linux character device driver to handle an USB connection.
     *  \param endpointAddress - The USB endpoint address.
     *  \param epType - The data type, which then will be transmitted on this endpoint.
     *  \param epDir - The Direction of the stream. (EPDIR_IN == Data from the INIC to the EHC, EPDIR_OUT == Data from the EHC to the INIC)
     *  \param deviceName - On successful configuration, this method stores the name of the character device into the given buffer.
     *  \param deviceNameBufLen - The length of the given buffer.
     *  \param amountOfBuffers - The amount of buffers used in the driver.
     *  \param bufferSize - The amount of bytes used in a single buffer.
     *  \return true, if successful. false, otherwise.
     */
    /*----------------------------------------------------------*/
    bool ConfigureUsbEndpoint( uint8_t endpointAddress, EPDataType_t epType, EPDirection_t epDir, char *deviceName,
        uint32_t deviceNameBufLen, uint32_t amountOfBuffers, uint32_t bufferSize );


    /*----------------------------------------------------------*/
    /*! \brief Configure the Linux character device driver to handle an MLB connection.
     *  \note This overloaded function is used to configure synchronous channels.
     *  \param aimType - Specifies the type of used Linux Driver Application Interface Module.
     *  \param mlbChannelAddress - The MLB channel address
     *  \param epType - The data type, which then will be transmitted on this endpoint.
     *  \param epDir - The Direction of the stream. (EPDIR_IN == Data from the INIC to the EHC, EPDIR_OUT == Data from the EHC to the INIC)
     *  \param deviceName - On successful configuration, this method stores the name of the character device into the given buffer.
     *  \param deviceNameBufLen - The length of the given buffer.
     *  \param amountOfBuffers - The amount of buffers used in the driver.
     *  \param bufferSize - The amount of bytes used in a single buffer.
     *  \param subbufferSize - The amount of bytes used for a single subbuffer. For isoc channel this maybe 188 or 196 byte.
     *  \param packetsPerTransaction - The amount of packets per transaction.
     *  \return true, if successful. false, otherwise.
     */
    /*----------------------------------------------------------*/
    bool ConfigureMlbChannel( AimType_t aimType, uint8_t mlbChannelAddress, EPDataType_t epType, EPDirection_t epDir,
        char *deviceName, uint32_t deviceNameBufLen, uint32_t amountOfBuffers, uint32_t bufferSize,
        uint32_t subbufferSize, uint32_t packetsPerTransaction );


    /*----------------------------------------------------------*/
    /*! \brief Configure the Linux character device driver to handle an MLB connection.
     *  \param mlbChannelAddress - The MLB channel address
     *  \param epType - The data type, which then will be transmitted on this endpoint.
     *  \param epDir - The Direction of the stream. (EPDIR_IN == Data from the INIC to the EHC, EPDIR_OUT == Data from the EHC to the INIC)
     *  \param deviceName - On successful configuration, this method stores the name of the character device into the given buffer.
     *  \param deviceNameBufLen - The length of the given buffer.
     *  \param amountOfBuffers - The amount of buffers used in the driver.
     *  \param bufferSize - The amount of bytes used in a single buffer.
     *  \return true, if successful. false, otherwise.
     */
    /*----------------------------------------------------------*/
    bool ConfigureMlbChannel( uint8_t mlbChannelAddress, EPDataType_t epType, EPDirection_t epDir, char *deviceName,
        uint32_t deviceNameBufLen, uint32_t amountOfBuffers, uint32_t bufferSize );



    /*----------------------------------------------------------*/
    /*! \brief Adds a callback class, which derives CNetworkDeviceListener.
     *         This class will then be informed about any results and changes of MOST transactions.
     *  \note It is possible to register multiple listeners.
     *  \param listener - The class, which wants to be registered for MOST events.
     */
    /*----------------------------------------------------------*/
    void AddListener( CNetworkDeviceListener *listener );



    /*----------------------------------------------------------*/
    /*! \brief Retrieves the amount of registered listeners.
     *  \note Use this method in combination of GetListener()
     *  \return The amount of registered listeners.
     */
    /*----------------------------------------------------------*/
    uint32_t GetAmountOfListners();


    /*----------------------------------------------------------*/
    /*! \brief Gets a pointer to the class implementing the CNetworkDeviceListener interface.
     *  \note Use this method in combination of GetAmountOfListners()
     *  \return The pointer to the given instance of Listener.
     */
    /*----------------------------------------------------------*/
    CNetworkDeviceListener *GetListener( uint32_t instance );


    /*----------------------------------------------------------*/
    /*! \brief Gets the MOST instance index of the server device.
     *  \return The MOST instance index, starting at 0 for the first instance.
     */
    /*----------------------------------------------------------*/
    uint8_t GetDeviceIndex();



    /*----------------------------------------------------------*/
    /*! \brief Wait let the scripting engine wait for the given amount of time.
     *  \param timeInMillis - Time value in milliseconds.
     */
    /*----------------------------------------------------------*/
    void ScriptPause( uint32_t timeInMillis );



    /*----------------------------------------------------------*/
    /*! \brief Execute a bunch of MCM send and receive actions, as descibed in the XML file.
     *  \param nodeAddress - The MOST node address (e.g. 0x101).
     *  \param pPathToScriptXml - Path to the XML file.
     */
    /*----------------------------------------------------------*/
    void ExecuteMcmScript( uint16_t nodeAddress, const char *pPathToScriptXml );



    /*----------------------------------------------------------*/
    /*! \brief Execute a bunch of MCM send and receive actions, as descibed in the XML zero terminated string.
     *  \param nodeAddress - The MOST node address (e.g. 0x101).
     *  \param pPathToScriptXml - Path to the XML file.
     */
    /*----------------------------------------------------------*/
    void ExecuteMcmScript( uint16_t nodeAddress, const char *pStringBuffer, uint32_t bufferLength );


    /*----------------------------------------------------------*/
    /*! \brief Sends a NetworkMaster.NOT_OK and NetworkMaster.OK, in order resolve address conflicts.
     */
    /*----------------------------------------------------------*/
    void ToggleNotOk();



    /*----------------------------------------------------------*/
    /*! \brief Retrieves the GroupAddresses of all available MOST devices in this MOST instance.
     *  \note The result of this request is reported in the callback method OnMostDeviceType of the CNetworkDeviceListener class.
     *        Use AddListener to get the result.
     */
    /*----------------------------------------------------------*/
    void GetGroupAddresses( uint8_t maxNodePos );




    /*----------------------------------------------------------*/
    /*! \brief Retrieves the MOST MAC address for the given node address in this MOST instance.
     *  \note The result of this request is reported in the callback method OnMostMacAddress of the CNetworkDeviceListener class.
     *        Use AddListener to get the result.
     *  \param nodeAddress - The MOST node address (e.g. 0x101).
     */
    /*----------------------------------------------------------*/
    void GetMacAddress( uint16_t nodeAddress );


    /*----------------------------------------------------------*/
    /*! \brief Starts up the MOST on the local INIC of the server device.
     *  \param isTimingMaster - true, if the INIC shall act as a timing master. false, if the device shall act as a timing slave.
     *  \param packetBandWidth - The amount of Bytes, which are reserved for asynchronous data (MEP, MHP).
     *  \note This is an INIC API Version 1 command. Use it for e.g. OS81110
     */
    /*----------------------------------------------------------*/
    void StartMostV1( bool isTimingMaster, uint16_t packetBandWidth );



    /*----------------------------------------------------------*/
    /*! \brief Performs a network shutdown sequence on the local INIC attached to the server device.
     *  \note This method will cause a chain reaction on the MOST network. All devices will switch of their output signal on MOST.
     *  \note This is an INIC API Version 1 command. Use it for e.g. OS81110
     */
    /*----------------------------------------------------------*/
    void MostNetworkShutdownV1();


    /*----------------------------------------------------------*/
    /*! \brief Enables the usage of the MLB bus for the device on the given node address.
     *  \param nodeAddress - The MOST node address (e.g. 0x101).
     *  \param mlbSpeed - The used MLB speed.
     *  \note This is an INIC API Version 1 command. Use it for e.g. OS81110
     */
    /*----------------------------------------------------------*/
    void OpenMlbV1( uint16_t nodeAddress, MlbPortSpeed_t mlbSpeed );




    /*----------------------------------------------------------*/
    /*! \brief Sets the MOST MAC address for the device on the given node address.
     *  \param nodeAddress - The MOST node address (e.g. 0x101).
     *  \param macAddress1 - The 1st byte of the MAC address.
     *  \param macAddress2 - The 2nd byte of the MAC address.
     *  \param macAddress3 - The 3rd byte of the MAC address.
     *  \param macAddress4 - The 4th byte of the MAC address.
     *  \param macAddress5 - The 5th byte of the MAC address.
     *  \param macAddress6 - The 6th byte of the MAC address.
     *  \param persistent - true, if the given MAC address shall be stored persistent into the INIC flash memory.
     *  \note This is an INIC API Version 1 command. Use it for e.g. OS81110
     */
    /*----------------------------------------------------------*/
    void SetMostMacAddressV1( uint16_t nodeAddress, uint8_t macAddress1, uint8_t macAddress2, uint8_t macAddress3,
        uint8_t macAddress4, uint8_t macAddress5, uint8_t macAddress6, bool persistent );

     
    /*----------------------------------------------------------*/
    /*! \brief Enables the usage of the MLB bus for the device on the given node address.
     *  \param nodeAddress - The MOST node address (e.g. 0x101).
     *  \param tsiPortId - The instance id of the used TSI port.
     *  \param tsiPortMode - The used mode for this TSI port.
     *  \note This is an INIC API Version 1 command. Use it for OS81110 only.
     */
    /*----------------------------------------------------------*/
    void OpenTsiV1( uint16_t nodeAddress, V1TsiPortInstance_t tsiPortId, V1TsiPortMode tsiPortMode );

    
    /*----------------------------------------------------------*/
    /*! \brief Creates a TSI socket on the given node address.
     *  \param nodeAddress - The MOST node address (e.g. 0x101).
     *  \param portInst - The port instance id of the TSI port.
     *  \param epType - The data type, which then will be transmitted on this endpoint.
     *  \param epDir - The Direction of the stream. (EPDIR_IN == Data from the INIC to the EHC, EPDIR_OUT == Data from the EHC to the INIC)
     *  \param blockWidthTsi -The block width in bytes to be allocated on the MLB bus.
     *  \param tag - Any 32 bit value. This value will be unmodified passed back in the corresponding callback method.
     *  \note This is an INIC API Version 1 command. Use it for e.g. OS81110
     */
    /*----------------------------------------------------------*/
    void CreateTsiSocketV1( uint16_t nodeAddress, V1TsiPortInstance_t portInst, 
        EPDataType_t epType, EPDirection_t epDir,uint16_t blockWidthTsi, uint32_t tag );

    /*----------------------------------------------------------*/
    /*! \brief Creates a MLB socket on the given node address.
     *  \param nodeAddress - The MOST node address (e.g. 0x101).
     *  \param epType - The data type, which then will be transmitted on this endpoint.
     *  \param epDir - The Direction of the stream. (EPDIR_IN == Data from the INIC to the EHC, EPDIR_OUT == Data from the EHC to the INIC)
     *  \param mlbChannelAddress - The MLB channel address (even value).
     *  \param blockWidthMlb -The block width in bytes to be allocated on the MLB bus.
     *  \param tag - Any 32 bit value. This value will be unmodified passed back in the corresponding callback method.
     *  \note This is an INIC API Version 1 command. Use it for e.g. OS81110
     */
    /*----------------------------------------------------------*/
    void CreateMlbSocketV1( uint16_t nodeAddress, EPDataType_t epType, EPDirection_t epDir, uint16_t mlbChannelAddress,
        uint16_t blockWidthMlb, uint32_t tag );



    /*----------------------------------------------------------*/
    /*! \brief Creates a MLB socket on the given node address.
     *  \param nodeAddress - The MOST node address (e.g. 0x101).
     *  \param epType - The data type, which then will be transmitted on this endpoint.
     *  \param epDir - The Direction of the stream. (EPDIR_IN == Data from the INIC to the EHC, EPDIR_OUT == Data from the EHC to the INIC)
     *  \param mlbChannelAddress - The MLB channel address (even value).
     *  \param blockWidthMlb -The block width in bytes of this singular splitted socked.
     *  \param splittedOffset - Offset in between the splitted socket.
     *  \param blockWidthCombined - The block width in bytes of all splitted sockets combined.
     *  \param tag - Any 32 bit value. This value will be unmodified passed back in the corresponding callback method.
     *  \note This is an INIC API Version 1 command. Use it for e.g. OS81110
     */
    /*----------------------------------------------------------*/
    void CreateSplittedMlbSocketV1( uint16_t nodeAddress, EPDataType_t epType, EPDirection_t epDir,
        uint16_t mlbChannelAddress, uint16_t blockWidthMlb, uint16_t splittedOffset, uint16_t blockWidthCombined,
        uint32_t tag );


    /*----------------------------------------------------------*/
    /*! \brief Creates a MOST socket on the given node address.
     *  \param nodeAddress - The MOST node address (e.g. 0x101).
     *  \param epType - The data type, which then will be transmitted on this endpoint.
     *  \param epDir - The Direction of the stream. (EPDIR_IN == Data from the INIC to the EHC, EPDIR_OUT == Data from the EHC to the INIC)
     *  \param connectionLabel - Only in case of if epDir is "EPDIR_IN": The connection label, which this socket will be bound.
     *                           Otherwise, the value will be ignored.
     *  \param blockwidthMost - The block width in bytes to be allocated on the MOST bus.
     *  \param tag - Any 32 bit value. This value will be unmodified passed back in the corresponding callback method.
     *  \note This is an INIC API Version 1 command. Use it for e.g. OS81110
     */
    /*----------------------------------------------------------*/
    void CreateMostSocketV1( uint16_t nodeAddress, EPDataType_t epType, EPDirection_t epDir, uint16_t connectionLabel,
        uint16_t blockwidthMost, uint32_t tag );




    /*----------------------------------------------------------*/
    /*! \brief  on the given node address.
     *  \param nodeAddress - The MOST node address (e.g. 0x101).
     *  \param handle - The handle of socket (will be reported by the callback callback methods of the create socket methods).
     *  \param tag - Any 32 bit value. This value will be unmodified passed back in the corresponding callback method.
     *  \note This is an INIC API Version 1 command. Use it for e.g. OS81110
     */
    /*----------------------------------------------------------*/
    void DestroySocketV1( uint16_t nodeAddress, uint8_t handle, uint32_t tag );



    /*----------------------------------------------------------*/
    /*! \brief  on the given node address.
     *  \param nodeAddress - The MOST node address (e.g. 0x101).
     *  \param handle - The handle of socket (will be reported by the callback callback methods of the connect socket method).
     *  \param tag - Any 32 bit value. This value will be unmodified passed back in the corresponding callback method.
     *  \note This is an INIC API Version 1 command. Use it for e.g. OS81110
     */
    /*----------------------------------------------------------*/
    void DisconnectSocketsV1( uint16_t nodeAddress, uint8_t handle, uint32_t tag );




    /*----------------------------------------------------------*/
    /*! \brief Connects two different sockets, which then is called a connection, on the given node address.
     *  \param nodeAddress - The MOST node address (e.g. 0x101).
     *  \param inHandle - The handle of the in-socket (will be reported by the callback callback methods of the create socket methods).
     *  \param outHandle - * The handle of out-socket (will be reported by the callback callback methods of the create socket methods).
     *  \param tag - Any 32 bit value. This value will be unmodified passed back in the corresponding callback method.
     *  \note This is an INIC API Version 1 command. Use it for e.g. OS81110
     */
    /*----------------------------------------------------------*/
    void ConnectSocketsV1( uint16_t nodeAddress, uint8_t inHandle, uint8_t outHandle, uint32_t tag );

    /*----------------------------------------------------------*/
    /*! \brief Configures an I2S port on the given node address.
     *  \param nodeAddr -The device with this MOST node address raised this event.
     *  \param tag - Any 32 bit value. This value will be unmodified passed back in the corresponding callback method.
     *  \note This is an INIC API Version 3 command. Use it for e.g. OS81110
     */
    /*----------------------------------------------------------*/
    void OpenStreamPortV1( uint16_t nodeAddress, V1I2SPortClkDriveMode_t portMode,
        V1I2SStreamingPortMode_t streamPortMode, V1I2SStreamingDataFormat_t format, uint32_t tag );


    /*----------------------------------------------------------*/
    /*! \brief Creates an I2S socket on the given node address.
     *  \param nodeAddr -The device with this MOST node address raised this event.
     *  \param portInstance - The I2S port instance. 0 = Port A, 1 = Port B.
     *  \param epDir - he Direction of the stream. (EPDIR_IN == Data from the INIC to the EHC, EPDIR_OUT == Data from the EHC to the INIC)
     *  \param blockWidthI2S - The block width in bytes allocated on the I2S bus.
     *  \param pin - The physical I2S data pin of the INIC.
     *  \param tag - Any 32 bit value. This value will be unmodified passed back in the corresponding callback method.
     *  \note This is an INIC API Version 3 command. Use it for e.g. OS81110
     */
    /*----------------------------------------------------------*/
    void CreateStreamSocketV1( uint16_t nodeAddress, EPDirection_t epDir, uint16_t blockWidthI2S, V1I2SPin_t pin,
        uint32_t tag );

    /*----------------------------------------------------------*/
    /*! \brief Creates an I2S socket on the given node address.
     *  \param nodeAddr -The device with this MOST node address raised this event.
     *  \param portInstance - The I2S port instance. 0 = Port A, 1 = Port B.
     *  \param epDir - he Direction of the stream. (EPDIR_IN == Data from the INIC to the EHC, EPDIR_OUT == Data from the EHC to the INIC)
     *  \param blockWidthI2S - The block width in bytes allocated on the I2S bus.
     *  \param splittedOffset - Offset in between the splitted socket.
     *  \param blockWidthCombined - The block width in bytes of all splitted sockets combined.
     *  \param pin - The physical I2S data pin of the INIC.
     *  \param tag - Any 32 bit value. This value will be unmodified passed back in the corresponding callback method.
     *  \note This is an INIC API Version 3 command. Use it for e.g. OS81110
     */
    /*----------------------------------------------------------*/
    void CreateSplittedStreamSocketV1( uint16_t nodeAddress, EPDirection_t epDir, uint16_t blockWidthI2S,
        uint16_t splittedOffset, uint16_t blockWidthCombined, V1I2SPin_t pin, uint32_t tag );

    /*----------------------------------------------------------*/
    /*! \brief Attach to a local INIC on the server device.
     *  \note This command has to be called before any other commands to the local INIC.
     *  \note This is an INIC API Version 3 command. Use it for e.g. OS81118
     */
    /*----------------------------------------------------------*/
    void AttachV3();



    /*----------------------------------------------------------*/
    /*! \brief Destroyes any resource on the given node address.
     *  \param nodeAddress - The MOST node address (e.g. 0x101).
     *  \param sync - true, if the device shall be used afterwise. false, if the device is going to suspend (shutdown).
     *  \note In any case (regardless if sync is true or false), all ports, sockets and connects are going to be destroyed).
     *  \note This is an INIC API Version 3 command. Use it for e.g. OS81118
     */
    /*----------------------------------------------------------*/
    void DeviceSyncV3( uint16_t nodeAddress, bool sync );



    /*----------------------------------------------------------*/
    /*! \brief Performs a network startup sequence on the local INIC attached to the server device.
     *  \note This is an INIC API Version 3 command. Use it for e.g. OS81118-D Revision
     */
    /*----------------------------------------------------------*/
    void MostNetworkStartupV3( uint16_t autoForcedNotAvailable, uint16_t packetBW );



    /*----------------------------------------------------------*/
    /*! \brief Performs a network startup sequence on the local INIC attached to the server device, after MostNetworkStartupV3 with parameters had failed.
     *  \note This is an INIC API Version 3 command. Use it for e.g. OS81118-D Revision
     *  \note Call this method only if the MostNetworkStartupV3( uint16_t autoForcedNotAvailable, uint16_t packetBW ) had failed. This will be treated as retry.
     */
    /*----------------------------------------------------------*/
    void MostNetworkStartupV3();


    /*----------------------------------------------------------*/
    /*! \brief Performs a network shutdown sequence on the local INIC attached to the server device.
     *  \note This method will cause a chain reaction on the MOST network. All devices will switch of their output signal on MOST.
     *  \note This is an INIC API Version 3 command. Use it for e.g. OS81118
     */
    /*----------------------------------------------------------*/
    void MostNetworkShutdownV3();


    
    /*----------------------------------------------------------*/
    /*! \brief Closes the IPC connection to local attached INIC.
     *  \note This will also cleanup any used resource in the INIC.
     *  \note This is an INIC API Version 3 command. Use it for e.g. OS81118
     */
    /*----------------------------------------------------------*/
    void InicUnsychronizeV3();




    /*----------------------------------------------------------*/
    /*! \brief Sets the MOST MAC address for the device on the given node address.
     *  \param nodeAddress - The MOST node address (e.g. 0x101).
     *  \param macAddress1 - The 1st byte of the MAC address.
     *  \param macAddress2 - The 2nd byte of the MAC address.
     *  \param macAddress3 - The 3rd byte of the MAC address.
     *  \param macAddress4 - The 4th byte of the MAC address.
     *  \param macAddress5 - The 5th byte of the MAC address.
     *  \param macAddress6 - The 6th byte of the MAC address.
     *  \note This is an INIC API Version 3 command. Use it for e.g. OS81118-D Rev
     */
    /*----------------------------------------------------------*/
    void SetMostMacAddressV3( uint16_t nodeAddress, uint8_t macAddress1, uint8_t macAddress2, uint8_t macAddress3,
        uint8_t macAddress4, uint8_t macAddress5, uint8_t macAddress6 );


    /*----------------------------------------------------------*/
    /*! \brief Enables the usage of the MLB bus for the device on the given node address.
     *  \param nodeAddress - The MOST node address (e.g. 0x101).
     *  \param mlbSpeed - The speed of the MLB bus.
     *  \note This is an INIC API Version 3 command. Use it for e.g. OS81118
     */
    /*----------------------------------------------------------*/
    void OpenMlbV3( uint16_t nodeAddress, MlbPortSpeed_t mlbSpeed );


    /*----------------------------------------------------------*/
    /*! \brief Creates an USB socket on the given node address.
     *  \param nodeAddress - The MOST node address (e.g. 0x101).
     *  \param epType - The data type, which then will be transmitted on this endpoint.
     *  \param epDir - The Direction of the stream. (EPDIR_IN == Data from the INIC to the EHC, EPDIR_OUT == Data from the EHC to the INIC)
     *  \param endPointAddress - The USB endpoint address.
     *  \param tag - Any 32 bit value. This value will be unmodified passed back in the corresponding callback method.
     *  \param packetsPerFrame - The amount of packets stored in a single USB frame (512 byte).
     *  \note This is an INIC API Version 3 command. Use it for e.g. OS81118
     */
    /*----------------------------------------------------------*/
    void CreateUsbSocketV3( uint16_t nodeAddress, EPDataType_t epType, EPDirection_t epDir, uint8_t endPointAddress,
        uint16_t packetsPerFrame, uint32_t tag );


    /*----------------------------------------------------------*/
    /*! \brief Creates an splitted USB socket on the given node address to use with Combiner or Splitter.
     *  \param nodeAddress - The MOST node address (e.g. 0x101).
     *  \param epType - The data type, which then will be transmitted on this endpoint.
     *  \param epDir - The Direction of the stream. (EPDIR_IN == Data from the INIC to the EHC, EPDIR_OUT == Data from the EHC to the INIC)
     *  \param endPointAddress - The USB endpoint address.
     *  \param tag - Any 32 bit value. This value will be unmodified passed back in the corresponding callback method.
     *  \param packetsPerFrame - The amount of packets stored in a single USB frame (512 byte).
     *  \note This is an INIC API Version 3 command. Use it for e.g. OS81118
     */
    /*----------------------------------------------------------*/
    void CreateSplittedUsbSocketV3( uint16_t nodeAddress, EPDataType_t epType, EPDirection_t epDir,
        uint8_t endPointAddress, uint16_t packetsPerFrame, uint16_t bytesPerPacket, uint32_t tag );



    /*----------------------------------------------------------*/
    /*! \brief Creates a MLB socket on the given node address.
     *  \param nodeAddress - The MOST node address (e.g. 0x101).
     *  \param epType - The data type, which then will be transmitted on this endpoint.
     *  \param epDir - The Direction of the stream. (EPDIR_IN == Data from the INIC to the EHC, EPDIR_OUT == Data from the EHC to the INIC)
     *  \param mlbChannelAddress - The MLB channel address (even value).
     *  \param blockwidthMlb - The block width in bytes to be allocated on the MLB bus.
     *  \param tag - Any 32 bit value. This value will be unmodified passed back in the corresponding callback method.
     *  \note This is an INIC API Version 3 command. Use it for e.g. OS81118
     */
    /*----------------------------------------------------------*/
    void CreateMlbSocketV3( uint16_t nodeAddress, uint16_t mlbPortHandle, EPDataType_t epType, EPDirection_t epDir,
        uint16_t mlbChannelAddress, uint16_t blockwidthMlb, uint32_t tag );



    /*----------------------------------------------------------*/
    /*! \brief Creates a splitted MLB socket on the given node address to use with Combiner or Splitter.
     *  \param nodeAddress - The MOST node address (e.g. 0x101).
     *  \param epType - The data type, which then will be transmitted on this endpoint.
     *  \param epDir - The Direction of the stream. (EPDIR_IN == Data from the INIC to the EHC, EPDIR_OUT == Data from the EHC to the INIC)
     *  \param mlbChannelAddress - The MLB channel address (even value).
     *  \param blockwidthMlb - The block width in bytes to be allocated on the MLB bus.
     *  \param bytesPerPacket - The total number of data bytes to be transfered each MOST frame.
     *  \param tag - Any 32 bit value. This value will be unmodified passed back in the corresponding callback method.
     *  \note This is an INIC API Version 3 command. Use it for e.g. OS81118
     */
    /*----------------------------------------------------------*/
    void CreateSplittedMlbSocketV3( uint16_t nodeAddress, uint16_t mlbPortHandle, EPDataType_t epType,
        EPDirection_t epDir, uint16_t mlbChannelAddress, uint16_t blockwidthMlb, uint16_t bytesPerPacket,
        uint32_t tag );


    /*----------------------------------------------------------*/
    /*! \brief Creates a MOST socket on the given node address.
     *  \param nodeAddress - The MOST node address (e.g. 0x101).
     *  \param epType - The data type, which then will be transmitted on this MOST channel.
     *  \param epDir - The Direction of the stream. (EPDIR_IN == Data from the INIC to the EHC, EPDIR_OUT == Data from the EHC to the INIC)
     *  \param connectionLabel - Only in case of if epDir is "EPDIR_IN": The connection label, which this socket will be bound.
     *                          Otherwise, the value will be ignored.
     *  \param blockwidthMost - The block width in bytes to be allocated on the MOST bus.
     *  \param tag - Any 32 bit value. This value will be unmodified passed back in the corresponding callback method.
     *  \note This is an INIC API Version 3 command. Use it for e.g. OS81118
     */
    /*----------------------------------------------------------*/
    void CreateMostSocketV3( uint16_t nodeAddress, EPDataType_t epType, EPDirection_t epDir, uint16_t connectionLabel,
        uint16_t blockwidthMost, uint32_t tag );




    /*----------------------------------------------------------*/
    /*! \brief Connects two different sockets, which then is called a connection, on the given node address.
     *  \param nodeAddress - The MOST node address (e.g. 0x101).
     *  \param epType - The data type, which then will be transmitted on this connection.
     *  \param inHandle - The handle of the in-socket (will be reported by the callback callback methods of the create socket methods).
     *  \param outHandle - * The handle of out-socket (will be reported by the callback callback methods of the create socket methods).
     *  \param tag - Any 32 bit value. This value will be unmodified passed back in the corresponding callback method.
     *  \note This is an INIC API Version 3 command. Use it for e.g. OS81118
     */
    /*----------------------------------------------------------*/
    void ConnectSocketsV3( uint16_t nodeAddress, EPDataType_t epType, uint16_t inHandle, uint16_t outHandle,
        uint16_t offset, uint32_t tag );




    /*----------------------------------------------------------*/
    /*! \brief Destroyes up to three resources (Ports or connections) on the given node address.
     *  \param nodeAddress - The MOST node address (e.g. 0x101).
     *  \param amountOfHandles - The amount of resources to be destroyed. (value must be between 1 to 3).
     *  \param handle1 - The first resource to be destroyed.
     *  \param handle2 - The second resource to be destroyed.
     *  \param handle3 - The third resource to be destroyed.
     *  \param tag - Any 32 bit value. This value will be unmodified passed back in the corresponding callback method.
     *  \note This is an INIC API Version 3 command. Use it for e.g. OS81118
     */
    /*----------------------------------------------------------*/
    void ResourceDestroyV3( uint16_t nodeAddress, uint8_t amountOfHandles, const uint16_t *pHandle, uint32_t tag );



    /*----------------------------------------------------------*/
    /*! \brief Configures an I2S port on the given node address.
     *  \param nodeAddr -The device with this MOST node address raised this event.
     *  \param portInstance - The I2S port instance. 0 = Port A, 1 = Port B.
     *  \param option - The I2S port option.
     *  \param mode - The I2S clock mode.
     *  \param delay - The I2S delay mode.
     *  \param tag - Any 32 bit value. This value will be unmodified passed back in the corresponding callback method.
     *  \note This is an INIC API Version 3 command. Use it for e.g. OS81118
     */
    /*----------------------------------------------------------*/
    void ConfigureStreamPortV3( uint16_t nodeAddress, uint8_t portInstance, V3I2SPortOption_t option,
        V3I2SClockMode_t mode, V3I2SDelayMode_t delay, uint32_t tag );



    /*----------------------------------------------------------*/
    /*! \brief Creates an I2S port on the given node address.
     *  \param nodeAddr -The device with this MOST node address raised this event.
     *  \param portInstance - The I2S port instance. 0 = Port A, 1 = Port B.
     *  \param clock - The I2S port speed in multiple of MOST base clock.
     *  \param align - The I2S data format alignment.
     *  \param tag - Any 32 bit value. This value will be unmodified passed back in the corresponding callback method.
     *  \note This is an INIC API Version 3 command. Use it for e.g. OS81118
     */
    /*----------------------------------------------------------*/
    void CreateStreamPortV3( uint16_t nodeAddress, uint8_t portInstance, V3I2SPortSpeed_t clock,
        V3I2SAlignment_t align, uint32_t tag );



    /*----------------------------------------------------------*/
    /*! \brief Creates an I2S socket on the given node address.
     *  \param nodeAddr -The device with this MOST node address raised this event.
     *  \param portInstance - The I2S port instance. 0 = Port A, 1 = Port B.
     *  \param epDir - he Direction of the stream. (EPDIR_IN == Data from the INIC to the EHC, EPDIR_OUT == Data from the EHC to the INIC)
     *  \param blockWidthI2S - The block width in bytes allocated on the I2S bus.
     *  \param pin - The physical I2S data pin of the INIC.
     *  \param tag - Any 32 bit value. This value will be unmodified passed back in the corresponding callback method.
     *  \note This is an INIC API Version 3 command. Use it for e.g. OS81118
     */
    /*----------------------------------------------------------*/
    void CreateStreamSocketV3( uint16_t nodeAddress, uint8_t portInstance, EPDirection_t epDir, uint16_t blockWidthI2S,
        V3I2SPin_t pin, uint32_t tag );


    /*----------------------------------------------------------*/
    /*! \brief Creates an splitted I2S socket on the given node address  to use with Combiner or Splitter.
     *  \param nodeAddr -The device with this MOST node address raised this event.
     *  \param portInstance - The I2S port instance. 0 = Port A, 1 = Port B.
     *  \param epDir - he Direction of the stream. (EPDIR_IN == Data from the INIC to the EHC, EPDIR_OUT == Data from the EHC to the INIC)
     *  \param blockWidthI2S - The block width in bytes allocated on the I2S bus.
     *  \param pin - The physical I2S data pin of the INIC.
     *  \param bytesPerPacket - The total number of data bytes to be transfered each MOST frame.
     *  \param tag - Any 32 bit value. This value will be unmodified passed back in the corresponding callback method.
     *  \note This is an INIC API Version 3 command. Use it for e.g. OS81118
     */
    /*----------------------------------------------------------*/
    void CreateSplittedStreamSocketV3( uint16_t nodeAddress, uint8_t portInstance, EPDirection_t epDir,
        uint16_t blockWidthI2S, V3I2SPin_t pin, uint16_t bytesPerPacket, uint32_t tag );


    /*----------------------------------------------------------*/
    /*! \brief Creates an I2S socket on the given node address.
     *  \param nodeAddr -The device with this MOST node address raised this event.
     *  \param tag - Any 32 bit value. This value will be unmodified passed back in the corresponding callback method.
     *  \note This is an INIC API Version 3 command. Use it for e.g. OS81118
     */
    /*----------------------------------------------------------*/
    void GetRingBreakResultV3( uint16_t nodeAddress, uint32_t tag );



    /*----------------------------------------------------------*/
    /*! \brief Sends the given Control Message out to MOST ring.
     *  \param targetAddr - The MOST target address
     *  \param nFBlock - The MOST Function Block Identifier
     *  \param nInst - The MOST Function Block Instance Identifier
     *  \param nFunc - The MOST Function Block Function Identifier
     *  \param nPayloadLen - The amount of bytes stored in Payload
     *  \param Payload - The pointer to the payload byte array.
     */
    /*----------------------------------------------------------*/
    bool SendMostControlMessage( uint8_t devInst, uint32_t targetAddr, uint32_t nFBlock, uint32_t nInst,
        uint32_t nFunc, uint8_t nOpType, uint32_t nPayloadLen, const uint8_t *Payload );
    
    /*----------------------------------------------------------*/
    /*! \brief Gets the device information for the given node address.
     *  \param nodeAddress - The MOST node address (e.g. 0x101).
     *  \note The corresponding callback to this request is OnDeviceVersion.
     *  \note This is an INIC API Version 3 command. Use it for e.g. OS81118
     */
    /*----------------------------------------------------------*/
    void GetDeviceVersion( uint16_t nodeAddress );

    /* Callback from underlying layers */
    virtual ISReturn_t OnMostMessage(CIndustrialStack *iStack, CISMostMsg *r);
};

#endif //_NETWORKDEVICE_H_
