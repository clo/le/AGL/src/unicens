/*
 * Video On Demand Samples
 *
 * Copyright (C) 2015 Microchip Technology Germany II GmbH & Co. KG
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * You may also obtain this software under a propriety license from Microchip.
 * Please contact Microchip for further information.
 *
 */

/*----------------------------------------------------------*/
/*! \file
 * \brief This component provides helper tools to deal with USB endpoints.
 */
/*----------------------------------------------------------*/
#ifndef DRIVERCONFIGURATION_H
#define DRIVERCONFIGURATION_H

#include <stdint.h>
#include <stdbool.h>
#include "Types.h"

#ifdef __cplusplus
extern "C"
{
#endif

    /*----------------------------------------------------------*/
    /*! \brief Checks if the given USB device instance exists.
     *
     * \param deviceInstance    - The device instance of the USB INIC, starting at 0 for the first instance.
     *
     * \return true: Device exists.
     * \return false: Device does not exist.
     */
    /*----------------------------------------------------------*/
    bool ExistsUsbDeviceInstance( uint8_t deviceInstance );

    /*----------------------------------------------------------*/
    /*! \brief Checks if the given MLB device instance exists.
     *
     * \param deviceInstance    - The device instance of the MLB INIC, starting at 0 for the first instance.
     *
     * \return true: Device exists.
     * \return false: Device does not exist.
     */
    /*----------------------------------------------------------*/
    bool ExistsMlbDeviceInstance( uint8_t deviceInstance );

    /*----------------------------------------------------------*/
    /*! \brief Checks if the given I2C device instance exists.
     *
     * \param deviceInstance    - The device instance of the I2C INIC, starting at 0 for the first instance.
     *
     * \return true: Device exists.
     * \return false: Device does not exist.
     */
    /*----------------------------------------------------------*/
    bool ExistsI2CDeviceInstance( uint8_t deviceInstance );

    /*----------------------------------------------------------*/
    /*! \brief Retreives the Linux character device names for the given USB device instance and the endpoint address.
     *
     * \note If the given buffer is too small to hold the whole path, the string is truncated and not useable!
     *
     * \param deviceInstance    - The device instance of the USB INIC, starting at 0 for the first instance.
     * \param endpointAddress   - The USB endpoint address, RX endpoints starting with upper bit set (0x8X).
     * \param deviceName        - [in,out] Given buffer will be filled with device name (starting with /dev/).
     * \param deviceNameLength  - The length of the given deviceName buffer in bytes.
     * \param systemName        - [in,out] Given buffer will be filled with system name (starting with /sys/).
     * \param systemNameLength  - The length of the given systemName buffer in bytes.
     * \param linkName          - [in,out] Given buffer will be filled with the string needed to register this device to an AIM
     * \param linkNameLength    - The length of the given linkName buffer in bytes.
     *
     * \return true: Operation was successful.
     * \return false: Operation failed, device or endpoint does not exist.
     */
    /*----------------------------------------------------------*/
    bool GetUsbDeviceNames( uint8_t deviceInstance, uint8_t endpointAddress, char *deviceName,
        uint16_t deviceNameLength, char *systemName, uint16_t systemNameLength, char *linkName,
        uint16_t linkNameLength );


    /*----------------------------------------------------------*/
    /*! \brief Retreives the Linux character device names for the given MLB device instance and the endpoint address.
     *
     * \note If the given buffer is too small to hold the whole path, the string is truncated and not useable!
     *
     * \param deviceInstance    - The device instance of the USB INIC, starting at 0 for the first instance.
     * \param mlbChannelAddress - The MLB channel address
     * \param deviceName        - [in,out] Given buffer will be filled with device name (starting with /dev/).
     * \param deviceNameLength  - The length of the given deviceName buffer in bytes.
     * \param systemName        - [in,out] Given buffer will be filled with system name (starting with /sys/).
     * \param systemNameLength  - The length of the given systemName buffer in bytes.
     * \param linkName          - [in,out] Given buffer will be filled with the string needed to register this device to an AIM
     * \param linkNameLength    - The length of the given linkName buffer in bytes.
     *
     * \return true: Operation was successful.
     * \return false: Operation failed, device or endpoint does not exist.
     */
    /*----------------------------------------------------------*/
    bool GetMlbDeviceNames( uint8_t deviceInstance, uint8_t mlbChannelAddress, char *deviceName,
        uint16_t deviceNameLength, char *systemName, uint16_t systemNameLength, char *linkName,
        uint16_t linkNameLength );


    /*----------------------------------------------------------*/
    /*! \brief Retreives the Linux character device names for the given I2C device instance and the endpoint address.
     *
     * \note If the given buffer is too small to hold the whole path, the string is truncated and not useable!
     *
     * \param deviceInstance    - The device instance of the I2C INIC, starting at 0 for the first instance.
     * \param isTx              - true, if the names for transmission path should be retrieved, otherwise for the reception path.
     * \param deviceName        - [in,out] Given buffer will be filled with device name (starting with /dev/).
     * \param deviceNameLength  - The length of the given deviceName buffer in bytes.
     * \param systemName        - [in,out] Given buffer will be filled with system name (starting with /sys/).
     * \param systemNameLength  - The length of the given systemName buffer in bytes.
     * \param linkName          - [in,out] Given buffer will be filled with the string needed to register this device to an AIM
     * \param linkNameLength    - The length of the given linkName buffer in bytes.
     *
     * \return true: Operation was successful.
     * \return false: Operation failed, device or endpoint does not exist.
     */
    /*----------------------------------------------------------*/
    bool GetI2CDeviceNames( uint8_t deviceInstance, bool isTx, char *deviceName, uint16_t deviceNameLength,
        char *systemName, uint16_t systemNameLength, char *linkName, uint16_t linkNameLength );

    /*----------------------------------------------------------*/
    /*! \brief Closes the MOST channel.
     *
     * \param device    - The system path to configure the MOST device (starting with "/sys/").
     *
     * \return True if no error
     */
    /*----------------------------------------------------------*/
    bool CloseMostChannel( const char *device );

    /*----------------------------------------------------------*/
    /*! \brief Configures the MOST channel.
     *
     * \param device    - The system path to configure the MOST device (starting with "/sys/").
     * \param mostType  - The MOST data type (control, asynchron, synchron, isochron).
     * \param direction - The direction of the stream (directionRX, directionTX).
     * \param numBuf    - The amount of buffers.
     * \param bufSize   - The size of each buffer in bytes.
     *
     * \return True if no error
     */
    /*----------------------------------------------------------*/
    bool ConfigureMostChannel( const char *device, EPDataType_t mostType, EPDirection_t direction, uint32_t numBuf,
        uint32_t bufSize );

    /*----------------------------------------------------------*/
    /*! \brief Configures the isochronous channel of a device.
     *  \note This function has to be called before ConfigureMostChannel!
     *
     * \param device    - The system path to configure the MOST device (starting with "/sys/").
     * \param subbufferSize - The amount of bytes for a single subbuffer (eg. 188 or 196 bytes).
     * \param packetsPerTransaction - The amount of ischronous packets per transaction.
     *
     * \return True if no error
     */
    /*----------------------------------------------------------*/
    bool ConfigureIsocChannel( const char *device, uint32_t subbufferSize, uint32_t packetsPerTransaction );

    /*----------------------------------------------------------*/
    /*! \brief Configures the Synchronous channel of a device.
     *  \note This function has to be called before ConfigureMostChannel!
     *
     * \param device    - The system path to configure the MOST device (starting with "/sys/").
     * \param syncBlockWidth - The bandwidth of a single synchronous frame. Ignored if not sync channel.
     * \param amounOfSyncFrames - The amount of synchronous frames per USB packet. Ignored if not sync channel.
     *
     * \return True if no error
     */
    /*----------------------------------------------------------*/
    bool ConfigureSyncChannel( const char *device, uint32_t syncBlockWidth, uint32_t amounOfSyncFrames );


    /*----------------------------------------------------------*/
    /*! \brief Creates a character device for the given device's link name.
     *
     * \param linkName          - [in] Link name, which is needed to register this device to an AIM.
     * \param deviceName        - [in,out] Device name (starting with /dev/).
     *
     * \return True if no error
     */
    /*----------------------------------------------------------*/
    bool LinkToCharacterDevice( const char *linkName, const char *deviceName );


    /*----------------------------------------------------------*/
    /*! \brief Creates a audio device (ALSA) for the given device's link name.
     *
     * \param linkName          - [in] Link name, which is needed to register this device to an AIM.
     * \param subBufferSize     - [in] The amount of bytes for all channels of this connection on the MOST.
     *
     * \return True if no error
     */
    /*----------------------------------------------------------*/
    bool LinkToAudioDevice( const char *linkName, uint16_t subBufferSize );

    /*----------------------------------------------------------*/
    /*! \brief Creates a Video for Linux device (V4L) for the given device's link name.
     *
     * \param linkName          - [in] Link name, which is needed to register this device to an AIM.
     *
     * \return True if no error
     */
    /*----------------------------------------------------------*/
    bool LinkToVideoForLinuxDevice( const char *linkName );


    /*----------------------------------------------------------*/
    /*! \brief Parsed the global errno variable and generate a human readable error description out of it.
     *
     * \return Zero terminated error description string.
     */
    /*----------------------------------------------------------*/
    const char *GetErrnoString();

#ifdef __cplusplus
}
#endif

#endif //DRIVERCONFIGURATION_H
