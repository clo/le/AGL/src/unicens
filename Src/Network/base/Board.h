/*
 * Video On Demand Samples
 *
 * Copyright (C) 2015 Microchip Technology Germany II GmbH & Co. KG
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * You may also obtain this software under a propriety license from Microchip.
 * Please contact Microchip for further information.
 *
 */

/*----------------------------------------------------------*/
/*! \file
 *  \brief This component abstracts platform specific functionalities.
 */
/*----------------------------------------------------------*/
#ifndef _BOARD_H_
#define _BOARD_H_

#ifdef __cplusplus
extern "C"
{
#endif

#include <stdint.h>

    /*----------------------------------------------------------*/
    /*! \brief reads the ms tick counter.
     *
     * \return value of the ms counter.
     */
    /*----------------------------------------------------------*/
    uint32_t GetTickCount( void );




    /*----------------------------------------------------------*/
    /*! \brief reads the ms tick counter
     *
     * \return value of the ms counter as word
     */
    /*----------------------------------------------------------*/
    uint16_t GetTickCountWord( void );


#ifdef __cplusplus
}
#endif

#endif // _BOARD_H_
