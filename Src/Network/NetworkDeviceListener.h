/*
 * Video On Demand Samples
 *
 * Copyright (C) 2015 Microchip Technology Germany II GmbH & Co. KG
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * You may also obtain this software under a propriety license from Microchip.
 * Please contact Microchip for further information.
 *
 */

/*----------------------------------------------------------*/
/*! \file
 *  \brief  This file contains the CNetworkDeviceListener class.
 */
/*----------------------------------------------------------*/
#ifndef _NETWORKDEVICELISTENER_H_
#define _NETWORKDEVICELISTENER_H_

#include <stdint.h>
#include "Types.h"

/*----------------------------------------------------------*/
/*! \brief This class defines callbacks methods which will be called by CNetworkDevice to any
 *         registered listener. Also this class implements empty implementation of each method.
 *         So the integrator has only to override methods, he is interessted in.
 */
/*----------------------------------------------------------*/
class CNetworkDeviceListener
{
public:
    /*----------------------------------------------------------*/
    /*! \brief Callback, when ever a sync event has been detected on the server network device.
     *  \param source - Cast this pointer to (CNetworkDevice*), it points to server network device, which received this event.
     *  \note This is an INIC API Version 3 event. It is raised from e.g. OS81118
     */
    /*----------------------------------------------------------*/
    virtual void OnSync( void *source, bool isSynced );



    /*----------------------------------------------------------*/
    /*! \brief Callback, when ever a Network State event has been detected on the server network device.
     *  \param source - Cast this pointer to (CNetworkDevice*), it points to server network device, which received this event.
     *  \param mpValChanged - true, there was a change in the Maximum Position Information. false, no change.
     *  \param systemNotOk - true, system state changes to NotOk. false, no change.
     *  \param mostAvailable - true, the MOST network is available. false, The MOST network is not available for sending control/packet data.
     *  \param availableSubState - 0=Regular, 2=Diagnosis, 4=RxOnBlocked, 6=ForcedNA, 0x10=Unstable, 0x11=Stable
     *  \param availableTransition - 0=Command, 1=RxActivity, 2=DiagnosisReady, 0x10=Normal, 0x11=SuddenSignalOff, 0x12=CriticalUnlock
     *                               0x13=ErrorSystem, 0xFF=NoTransition
     *  \param nodeAddress - The assigned MOST node address.
     *  \param nodePos - The position in the MOST ring.
     *  \param maxPos - The maximum amount of devices in the MOST ring.
     *  \param packetBW - The amount of bytes reserved for packet channel (MEP/MHP)
     *  \note This is an INIC API Version 3 event. It is raised from e.g. OS81118
     */
    /*----------------------------------------------------------*/
    virtual void OnNetworkState( void *source, bool mpValChanged, bool systemNotOk,
        bool mostAvailable, uint8_t availableSubState, uint8_t availableTransition, uint16_t nodeAddress,
        uint8_t nodePos, uint8_t maxPos, uint16_t packetBW );



    virtual void OnInitCompleteV1( void *source );
    virtual void OnUnlockV1( void *source );
    virtual void OnNetOnV1( void *source, bool isMaster );
    virtual void OnShutDownV1( void *source );
    virtual void OnNprV1( void *source, uint8_t npr );
    virtual void OnMprV1( void *source, uint8_t oldMpr, uint8_t newMpr );



    /*----------------------------------------------------------*/
    /*! \brief Callback, when ever the MOST had been tried to start.
     *  \param success - true, when the operation was successful.
     *  \note This is an INIC API Version 3 event. It is raised from e.g. OS81118 D-Rev
     */
    /*----------------------------------------------------------*/
    virtual void OnNetworkStartupV3( void *source, bool success );


    
    /*----------------------------------------------------------*/
    /*! \brief Callback, when ever the MOST had been tried to shutdown.
     *  \param success - true, when the operation was successful.
     *  \note This is an INIC API Version 3 event. It is raised from e.g. OS81118  D-Rev
     */
    /*----------------------------------------------------------*/
    virtual void OnNetworkShutdownV3( void *source, bool success );
    

    /*----------------------------------------------------------*/
    /*! \brief Callback, when ever a MAC address has been changed in the network.
     *  \param source - Cast this pointer to (CNetworkDevice*), it points to server network device, which received this event.
     *  \param success - true, when the operation was successful.
     *  \param nodeAddress - The device with this MOST node address raised this event.
     *  \param macAddress1 - The 1st byte of the MAC address.
     *  \param macAddress2 - The 2nd byte of the MAC address.
     *  \param macAddress3 - The 3rd byte of the MAC address.
     *  \param macAddress4 - The 4th byte of the MAC address.
     *  \param macAddress5 - The 5th byte of the MAC address.
     *  \param macAddress6 - The 6th byte of the MAC address.
     *  \note This is event will be raised by all INIC APIS (V1 & V2).
     */
    /*----------------------------------------------------------*/
    virtual void OnMostMacAddress( void *source, bool success, uint16_t nodeAddress, uint8_t macAddress1,
        uint8_t macAddress2, uint8_t macAddress3, uint8_t macAddress4, uint8_t macAddress5, uint8_t macAddress6 );



    /*----------------------------------------------------------*/
    /*! \brief Callback, when ever a Device type has been detected.
     *  \param source - Cast this pointer to (CNetworkDevice*), it points to server network device, which received this event.
     *  \param nodeAddress - The device with this MOST node address raised this event.
     *  \param deviceType - Identifier which sorts the MOST device to a specific function group.
     *  \note In this implementation the Device Type is simply derived from the Group Address set in the INIC config string!
     *  \note This is event will be raised by all INIC APIS (V1 & V2).
     */
    /*----------------------------------------------------------*/
    virtual void OnMostDeviceType( void *source, bool success, uint16_t nodeAddress, uint16_t deviceType );


    /*----------------------------------------------------------*/
    /*! \brief Callback, when ever the TSI bus was opened.
     *  \param source - Cast this pointer to (CNetworkDevice*), it points to server network device, which received this event.
     *  \param success - true, when the operation was successful.
     *  \param nodeAddress - The device with this MOST node address raised this event.
     *  \note This is an INIC API Version 1 event. It is raised from e.g. OS81110
     */
    /*----------------------------------------------------------*/
    virtual void OnOpenTsiV1( void *source, bool success, uint16_t nodeAddress );

    /*----------------------------------------------------------*/
    /*! \brief Callback, when ever a TSI socket was created.
     *  \param source - Cast this pointer to (CNetworkDevice*), it points to server network device, which received this event.
     *  \param success - true, when the operation was successful.
     *  \param nodeAddr - The device with this MOST node address raised this event.
     *  \param tsiPortInst - The use TSI port.
     *  \param epType - The data type, which then will be transmitted on this endpoint.
     *  \param epDir - The Direction of the stream. (EPDIR_IN == Data from the INIC to the EHC, EPDIR_OUT == Data from the EHC to the INIC)
     *  \param blockWidthMlb - The block width in bytes allocated on the MLB bus.
     *  \param mlbChannelAddress - The MLB channel address (even value).
     *  \param socketHandle - Unique handle for this socket. Use this for creating connections or closing this socket again.
     *  \param tag - Any 32 bit value. This value was passed to the corresponding function call of the CNetworkDevice class.
     *  \note This is an INIC API Version 1 event. It is raised from e.g. OS81110
     */
    /*----------------------------------------------------------*/
    virtual void OnCreateTsiSocketV1( void *source, bool success, uint16_t nodeAddr, V1TsiPortInstance_t tsiPortInst,
        EPDataType_t epType, EPDirection_t epDir, uint16_t blockWidthTsi, uint16_t socketHandle, uint32_t tag );

    /*----------------------------------------------------------*/
    /*! \brief Callback, when ever the MLB bus was opened.
     *  \param source - Cast this pointer to (CNetworkDevice*), it points to server network device, which received this event.
     *  \param success - true, when the operation was successful.
     *  \param nodeAddress - The device with this MOST node address raised this event.
     *  \note This is an INIC API Version 1 event. It is raised from e.g. OS81110
     */
    /*----------------------------------------------------------*/
    virtual void OnOpenMlbV1( void *source, bool success, uint16_t nodeAddress );



    /*----------------------------------------------------------*/
    /*! \brief Callback, when ever a MLB socket was created.
     *  \param source - Cast this pointer to (CNetworkDevice*), it points to server network device, which received this event.
     *  \param success - true, when the operation was successful.
     *  \param nodeAddr - The device with this MOST node address raised this event.
     *  \param epType - The data type, which then will be transmitted on this endpoint.
     *  \param epDir - The Direction of the stream. (EPDIR_IN == Data from the INIC to the EHC, EPDIR_OUT == Data from the EHC to the INIC)
     *  \param blockWidthMlb - The block width in bytes allocated on the MLB bus.
     *  \param mlbChannelAddress - The MLB channel address (even value).
     *  \param socketHandle - Unique handle for this socket. Use this for creating connections or closing this socket again.
     *  \param tag - Any 32 bit value. This value was passed to the corresponding function call of the CNetworkDevice class.
     *  \note This is an INIC API Version 1 event. It is raised from e.g. OS81110
     */
    /*----------------------------------------------------------*/
    virtual void OnCreateMlbSocketV1( void *source, bool success, uint16_t nodeAddr, EPDataType_t epType,
        EPDirection_t epDir, uint16_t blockWidthMlb, uint8_t mlbChannelAddress, uint16_t socketHandle, uint32_t tag );



    /*----------------------------------------------------------*/
    /*! \brief Callback, when ever a MOST socket was created.
     *  \param source - Cast this pointer to (CNetworkDevice*), it points to server network device, which received this event.
     *  \param success - true, when the operation was successful.
     *  \param nodeAddr - The device with this MOST node address raised this event.
     *  \param epType - The data type, which then will be transmitted on this endpoint.
     *  \param epDir - The Direction of the stream. (EPDIR_IN == Data from the INIC to the EHC, EPDIR_OUT == Data from the EHC to the INIC)
     *  \param blockwidthMost - The block width in bytes allocated on the MOST bus.
     *  \param connectionLabel - The MOST connection label, which this socket will be bound.
     *  \param socketHandle - Unique handle for this socket. Use this for creating connections or closing this socket again.
     *  \param tag - Any 32 bit value. This value was passed to the corresponding function call of the CNetworkDevice class.
     *  \note This is an INIC API Version 1 event. It is raised from e.g. OS81110
     */
    /*----------------------------------------------------------*/
    virtual void OnCreateMostSocketV1( void *source, bool success, uint16_t nodeAddr, EPDataType_t epType,
        EPDirection_t epDir, uint16_t blockwidthMost, uint16_t connectionLabel, uint16_t socketHandle, uint32_t tag );



    /*----------------------------------------------------------*/
    /*! \brief Callback, when ever two MOST sockets where connected.
     *  \param source - Cast this pointer to (CNetworkDevice*), it points to server network device, which received this event.
     *  \param success - true, when the operation was successful.
     *  \param nodeAddr - The device with this MOST node address raised this event.
     *  \param inSocketHandle - The user given IN-handle.
     *  \param outSocketHandle - The user given OUT-handle.
     *  \param connectionHandle - Unique handle for this connection. Use this for disconnecting this connection again.
     *  \param tag - Any 32 bit value. This value was passed to the corresponding function call of the CNetworkDevice class.
     *  \note This is an INIC API Version 1 event. It is raised from e.g. OS81110
     */
    /*----------------------------------------------------------*/
    virtual void OnConnectSocketsV1( void *source, bool success, uint16_t nodeAddr, uint16_t inSocketHandle,
        uint16_t outSocketHandle, uint16_t connectionHandle, uint32_t tag );



    /*----------------------------------------------------------*/
    /*! \brief Callback, when ever a MOST socket was deestroyed.
     *  \param source - Cast this pointer to (CNetworkDevice*), it points to server network device, which received this event.
     *  \param success - true, when the operation was successful.
     *  \param nodeAddress - The device with this MOST node address raised this event.
     *  \param handle - The unique handle for this socket. After this call, the handle is invalid.
     *  \param tag - Any 32 bit value. This value was passed to the corresponding function call of the CNetworkDevice class.
     *  \note This is an INIC API Version 1 event. It is raised from e.g. OS81110
     */
    /*----------------------------------------------------------*/
    virtual void OnDestroySocketV1( void *source, bool success, uint16_t nodeAddr, uint16_t handle, uint32_t tag );



    /*----------------------------------------------------------*/
    /*! \brief Callback, when ever a MOST connection was disconnected.
     *  \param source - Cast this pointer to (CNetworkDevice*), it points to server network device, which received this event.
     *  \param success - true, when the operation was successful.
     *  \param nodeAddress - The device with this MOST node address raised this event.
     *  \param handle - The unique handle for this connection. After this call, the handle is invalid.
     *  \param tag - Any 32 bit value. This value was passed to the corresponding function call of the CNetworkDevice class.
     *  \note This is an INIC API Version 1 event. It is raised from e.g. OS81110
     */
    /*----------------------------------------------------------*/
    virtual void OnDisconnectSocketsV1( void *source, bool success, uint16_t nodeAddr, uint16_t handle, uint32_t tag );




    /*----------------------------------------------------------*/
    /*! \brief Callback, when ever a I2S port has been configured.
     *  \param source - Cast this pointer to (CNetworkDevice*), it points to server network device, which received this event.
     *  \param success - true, when the operation was successful.
     *  \param nodeAddr - The device with this MOST node address raised this event.
     *  \param tag - Any 32 bit value. This value was passed to the corresponding function call of the CNetworkDevice class.
     *  \note This is an INIC API Version 3 event. It is raised from e.g. OS81118
     */
    /*----------------------------------------------------------*/
    virtual void OnOpenI2SPortV1( void *source, bool success, uint16_t nodeAddress, V1I2SPortClkDriveMode_t portMode,
        V1I2SStreamingDataFormat_t format, uint32_t tag );




    /*----------------------------------------------------------*/
    /*! \brief Callback, when ever a I2S socket has been created.
     *  \param source - Cast this pointer to (CNetworkDevice*), it points to server network device, which received this event.
     *  \param success - true, when the operation was successful.
     *  \param nodeAddr - The device with this MOST node address raised this event.
     *  \param portInstance - The I2S port instance. 0 = Port A, 1 = Port B.
     *  \param epDir - he Direction of the stream. (EPDIR_IN == Data from the INIC to the EHC, EPDIR_OUT == Data from the EHC to the INIC)
     *  \param blockWidthI2S - The block width in bytes allocated on the I2S bus.
     *  \param pin - The physical I2S data pin of the INIC.
     *  \param socketHandle - Unique handle for this socket. Use this for creating connections or destroying this socket again.
     *  \param tag - Any 32 bit value. This value was passed to the corresponding function call of the CNetworkDevice class.
     *  \note This is an INIC API Version 3 event. It is raised from e.g. OS81118
     */
    /*----------------------------------------------------------*/
    virtual void OnCreateI2SSocketV1( void *source, bool success, uint16_t nodeAddr, EPDirection_t epDir,
        uint16_t blockWidthI2S, V1I2SPin_t pin, uint16_t socketHandle, uint32_t tag );




    /*----------------------------------------------------------*/
    /*! \brief Callback, when ever the MLB bus was opened.
     *  \param source - Cast this pointer to (CNetworkDevice*), it points to server network device, which received this event.
     *  \param success - true, when the operation was successful.
     *  \param nodeAddress - The device with this MOST node address raised this event.
     *  \param mlbPortHandle - The device with this MOST node address raised this event.
     *  \note This is an INIC API Version 3 event. It is raised from e.g. OS81118
     */
    /*----------------------------------------------------------*/
    virtual void OnOpenMlbV3( void *source, bool success, uint16_t nodeAddress, uint16_t mlbPortHandle );




    /*----------------------------------------------------------*/
    /*! \brief Callback, when ever a USB socket was created.
     *  \param source - Cast this pointer to (CNetworkDevice*), it points to server network device, which received this event.
     *  \param success - true, when the operation was successful.
     *  \param nodeAddr - The device with this MOST node address raised this event.
     *  \param epType - The data type, which then will be transmitted on this endpoint.
     *  \param epDir - The Direction of the stream. (EPDIR_IN == Data from the INIC to the EHC, EPDIR_OUT == Data from the EHC to the INIC)
     *  \param endPointAddress - The USB endpoint address, which is now usable.
     *  \param socketHandle - Unique handle for this socket. Use this for creating connections or closing this socket again.
     *  \param tag - Any 32 bit value. This value was passed to the corresponding function call of the CNetworkDevice class.
     *  \note This is an INIC API Version 3 event. It is raised from e.g. OS81118
     */
    /*----------------------------------------------------------*/
    virtual void OnCreateUsbSocketV3( void *source, bool success, uint16_t nodeAddr, EPDataType_t epType,
        EPDirection_t epDir, uint8_t endPointAddress, uint16_t socketHandle, uint32_t tag );



    /*----------------------------------------------------------*/
    /*! \brief Callback, when ever a USB socket was created, which uses a Splitter or a Combiner.
     *  \param source - Cast this pointer to (CNetworkDevice*), it points to server network device, which received this event.
     *  \param success - true, when the operation was successful.
     *  \param nodeAddr - The device with this MOST node address raised this event.
     *  \param epType - The data type, which then will be transmitted on this endpoint.
     *  \param epDir - The Direction of the stream. (EPDIR_IN == Data from the INIC to the EHC, EPDIR_OUT == Data from the EHC to the INIC)
     *  \param endPointAddress - The USB endpoint address, which is now usable.
     *  \param usbHandle - Unique handle for this USB socket. Do not use this for creating connections!
     *  \param splitterHandle - Unique handle for this Splitter socket. Use this for creating connections or closing this socket again!
     *  \param tag - Any 32 bit value. This value was passed to the corresponding function call of the CNetworkDevice class.
     *  \note This is an INIC API Version 3 event. It is raised from e.g. OS81118
     */
    /*----------------------------------------------------------*/
    virtual void OnCreateSplittedUsbSocketV3( void *source, bool success, uint16_t nodeAddr, EPDataType_t epType,
        EPDirection_t epDir, uint8_t endPointAddress, uint16_t usbHandle, uint16_t splitterHandle, uint32_t tag );


    /*----------------------------------------------------------*/
    /*! \brief Callback, when ever a MLB socket was created.
     *  \param source - Cast this pointer to (CNetworkDevice*), it points to server network device, which received this event.
     *  \param success - true, when the operation was successful.
     *  \param nodeAddr - The device with this MOST node address raised this event.
     *  \param epType - The data type, which then will be transmitted on this endpoint.
     *  \param epDir - The Direction of the stream. (EPDIR_IN == Data from the INIC to the EHC, EPDIR_OUT == Data from the EHC to the INIC)
     *  \param blockWidthMlb - The block width in bytes allocated on the MLB bus.
     *  \param mlbChannelAddress - The MLB channel address (even value).
     *  \param socketHandle - Unique handle for this socket. Use this for creating connections or closing this socket again.
     *  \param tag - Any 32 bit value. This value was passed to the corresponding function call of the CNetworkDevice class.
     *  \note This is an INIC API Version 3 event. It is raised from e.g. OS81118
     */
    /*----------------------------------------------------------*/
    virtual void OnCreateMlbSocketV3( void *source, bool success, uint16_t nodeAddr, EPDataType_t epType,
        EPDirection_t epDir, uint16_t blockWidthMlb, uint8_t mlbChannelAddress, uint16_t socketHandle, uint32_t tag );


    /*----------------------------------------------------------*/
    /*! \brief Callback, when ever a  MLB socket was created, which uses a Splitter or a Combiner.
     *  \param source - Cast this pointer to (CNetworkDevice*), it points to server network device, which received this event.
     *  \param success - true, when the operation was successful.
     *  \param nodeAddr - The device with this MOST node address raised this event.
     *  \param epType - The data type, which then will be transmitted on this endpoint.
     *  \param epDir - The Direction of the stream. (EPDIR_IN == Data from the INIC to the EHC, EPDIR_OUT == Data from the EHC to the INIC)
     *  \param blockWidthMlb - The block width in bytes allocated on the MLB bus.
     *  \param mlbChannelAddress - The MLB channel address (even value).
     *  \param mlbSocketHandle - Unique handle for this socket. Use this for creating connections or closing this socket again.
     *  \param splitterHandle - Unique handle for this Splitter socket. Use this for creating connections or closing this socket again!
     *  \param tag - Any 32 bit value. This value was passed to the corresponding function call of the CNetworkDevice class.
     *  \note This is an INIC API Version 3 event. It is raised from e.g. OS81118
     */
    /*----------------------------------------------------------*/
    virtual void OnCreateSplittedMlbSocketV3( void *source, bool success, uint16_t nodeAddr, EPDataType_t epType,
        EPDirection_t epDir, uint16_t blockWidthMlb, uint8_t mlbChannelAddress, uint16_t mlbSocketHandle,
        uint16_t splitterHandle, uint32_t tag );


    /*----------------------------------------------------------*/
    /*! \brief Callback, when ever a MOST socket was created.
     *  \param source - Cast this pointer to (CNetworkDevice*), it points to server network device, which received this event.
     *  \param success - true, when the operation was successful.
     *  \param nodeAddr - The device with this MOST node address raised this event.
     *  \param epType - The data type, which then will be transmitted on this endpoint.
     *  \param epDir - The Direction of the stream. (EPDIR_IN == Data from the INIC to the EHC, EPDIR_OUT == Data from the EHC to the INIC)
     *  \param blockwidthMost - The block width in bytes allocated on the MOST bus.
     *  \param connectionLabel - The MOST connection label, which this socket will be bound.
     *  \param socketHandle - Unique handle for this socket. Use this for creating connections or destroying this socket again.
     *  \param tag - Any 32 bit value. This value was passed to the corresponding function call of the CNetworkDevice class.
     *  \note This is an INIC API Version 3 event. It is raised from e.g. OS81118
     */
    /*----------------------------------------------------------*/
    virtual void OnCreateMostSocketV3( void *source, bool success, uint16_t nodeAddr, EPDataType_t epType,
        EPDirection_t epDir, uint16_t blockwidthMost, uint16_t connectionLabel, uint16_t socketHandle, uint32_t tag );



    /*----------------------------------------------------------*/
    /*! \brief Callback, when ever two MOST sockets where connected.
     *  \param source - Cast this pointer to (CNetworkDevice*), it points to server network device, which received this event.
     *  \param success - true, when the operation was successful.
     *  \param nodeAddr - The device with this MOST node address raised this event.
     *  \param epType - The data type, which then will be transmitted on this connection.
     *  \param inSocketHandle - The user given IN-handle.
     *  \param outSocketHandle - The user given OUT-handle.
     *  \param connectionHandle - Unique handle for this connection. Use this for destroying this connection again.
     *  \param tag - Any 32 bit value. This value was passed to the corresponding function call of the CNetworkDevice class.
     *  \note This is an INIC API Version 3 event. It is raised from e.g. OS81118
     */
    /*----------------------------------------------------------*/
    virtual void OnConnectSocketsV3( void *source, bool success, uint16_t nodeAddr, EPDataType_t epType,
        uint16_t inSocketHandle, uint16_t outSocketHandle, uint16_t connectionHandle, uint32_t tag );



    /*----------------------------------------------------------*/
    /*! \brief Callback, when ever MOST resources where destroyed.
     *  \param source - Cast this pointer to (CNetworkDevice*), it points to server network device, which received this event.
     *  \param success - true, when the operation was successful.
     *  \param nodeAddr - The device with this MOST node address raised this event.
     *  \param amountOfHandles - The amount of resources to be destroyed. (value will be between 1 to 3).
     *  \param pHandle - Pointer to the resource list
     *  \param tag - Any 32 bit value. This value was passed to the corresponding function call of the CNetworkDevice class.
     *  \note This is an INIC API Version 3 event. It is raised from e.g. OS81118
     */
    /*----------------------------------------------------------*/
    virtual void OnResourceDestroyV3( void *source, bool success, uint16_t nodeAddr, uint8_t amountOfHandles,
        const uint16_t *pHandle, uint32_t tag );



    /*----------------------------------------------------------*/
    /*! \brief Callback, when ever a I2S port has been configured.
     *  \param source - Cast this pointer to (CNetworkDevice*), it points to server network device, which received this event.
     *  \param success - true, when the operation was successful.
     *  \param nodeAddr - The device with this MOST node address raised this event.
     *  \param portInstance - The I2S port instance. 0 = Port A, 1 = Port B.
     *  \param option - The I2S port option.
     *  \param mode - The I2S clock mode.
     *  \param delay - The I2S delay mode.
     *  \param tag - Any 32 bit value. This value was passed to the corresponding function call of the CNetworkDevice class.
     *  \note This is an INIC API Version 3 event. It is raised from e.g. OS81118
     */
    /*----------------------------------------------------------*/
    virtual void OnConfigureI2SPortV3( void *source, bool success, uint16_t nodeAddr, uint8_t portInstance,
        V3I2SPortOption_t option, V3I2SClockMode_t mode, V3I2SDelayMode_t delay, uint32_t tag );



    /*----------------------------------------------------------*/
    /*! \brief Callback, when ever a I2S port has been created.
     *  \param source - Cast this pointer to (CNetworkDevice*), it points to server network device, which received this event.
     *  \param success - true, when the operation was successful.
     *  \param nodeAddr - The device with this MOST node address raised this event.
     *  \param portInstance - The I2S port instance. 0 = Port A, 1 = Port B.
     *  \param clock - The I2S port speed in multiple of MOST base clock.
     *  \param align - The I2S data format alignment.
     *  \param tag - Any 32 bit value. This value was passed to the corresponding function call of the CNetworkDevice class.
     *  \note This is an INIC API Version 3 event. It is raised from e.g. OS81118
     */
    /*----------------------------------------------------------*/
    virtual void OnCreateI2SPortV3( void *source, bool success, uint16_t nodeAddr, uint8_t portInstance,
        V3I2SPortSpeed_t clock, V3I2SAlignment_t align, uint32_t tag );



    /*----------------------------------------------------------*/
    /*! \brief Callback, when ever a I2S socket has been created.
     *  \param source - Cast this pointer to (CNetworkDevice*), it points to server network device, which received this event.
     *  \param success - true, when the operation was successful.
     *  \param nodeAddr - The device with this MOST node address raised this event.
     *  \param portInstance - The I2S port instance. 0 = Port A, 1 = Port B.
     *  \param epDir - he Direction of the stream. (EPDIR_IN == Data from the INIC to the EHC, EPDIR_OUT == Data from the EHC to the INIC)
     *  \param blockWidthI2S - The block width in bytes allocated on the I2S bus.
     *  \param pin - The physical I2S data pin of the INIC.
     *  \param socketHandle - Unique handle for this socket. Use this for creating connections or destroying this socket again.
     *  \param tag - Any 32 bit value. This value was passed to the corresponding function call of the CNetworkDevice class.
     *  \note This is an INIC API Version 3 event. It is raised from e.g. OS81118
     */
    /*----------------------------------------------------------*/
    virtual void OnCreateI2SSocketV3( void *source, bool success, uint16_t nodeAddr, uint8_t portInstance,
        EPDirection_t epDir, uint16_t blockWidthI2S, V3I2SPin_t pin, uint16_t socketHandle, uint32_t tag );



    /*----------------------------------------------------------*/
    /*! \brief Callback, when ever a I2S socket has been created, which uses a Splitter or a Combiner.
     *  \param source - Cast this pointer to (CNetworkDevice*), it points to server network device, which received this event.
     *  \param success - true, when the operation was successful.
     *  \param nodeAddr - The device with this MOST node address raised this event.
     *  \param portInstance - The I2S port instance. 0 = Port A, 1 = Port B.
     *  \param epDir - The Direction of the stream. (EPDIR_IN == Data from the INIC to the EHC, EPDIR_OUT == Data from the EHC to the INIC)
     *  \param blockWidthI2S - The block width in bytes allocated on the I2S bus.
     *  \param pin - The physical I2S data pin of the INIC.
     *  \param i2sSocketHandle - Unique handle for this socket. Use this for creating connections or destroying this socket again.
     *  \param splitterHandle - Unique handle for this Splitter socket. Use this for creating connections or closing this socket again!
     *  \param tag - Any 32 bit value. This value was passed to the corresponding function call of the CNetworkDevice class.
     *  \note This is an INIC API Version 3 event. It is raised from e.g. OS81118
     */
    /*----------------------------------------------------------*/
    virtual void OnCreateSplittedI2SSocketV3( void *source, bool success, uint16_t nodeAddr, uint8_t portInstance,
        EPDirection_t epDir, uint16_t blockWidthI2S, V3I2SPin_t pin, uint16_t i2sSocketHandle, uint16_t splitterHandle,
        uint32_t tag );


    /*----------------------------------------------------------*/
    /*! \brief Callback, when a Ring Break Diagnosis Result was received.
     *  \param source - Cast this pointer to (CNetworkDevice*), it points to server network device, which received this event.
     *  \param nodeAddr - The device with this MOST node address raised this event.
     *  \praram result - The ring break diagnosis result 
     *  \param position - Relative position to the ring break.
     *  \param status - Gives information about the activity state.
     *  \param id - The RBD identifier as configured in config string.
     *  \note This is an INIC API Version 3 event. It is raised from e.g. OS81118
     */
    /*----------------------------------------------------------*/
    virtual void OnRbdResultV3( void *source, uint16_t nodeAddress, uint8_t result, uint8_t position, 
        uint8_t status, uint16_t id );


    /*----------------------------------------------------------*/
    /*! \brief Callback, when the MOST control channel receive thread has ended.
     *  \param source - Cast this pointer to (CNetworkDevice*), it points to server network device, which received this event.
     */
    /*----------------------------------------------------------*/
    virtual void OnControlChannelReadEnd( void *source );


    /*----------------------------------------------------------*/
    /*! \brief This callback method is called whenever a MOST control message was sent or received.
     *  \note In order to be informed about this event, derive this class, implement this method and register the class with AddListener method of CNetwork class.
     *  \note The user may interpret the data and sent a corresponding MOST control message via CNetwork::SendMostControlMessage.
     *  \param source - Cast this pointer to (CNetworkDevice*), it points to server network device, which received this event.
     *  \param sourceAddr - The MOST source address (may be not accurate)
     *  \param targetAddr - The MOST target address
     *  \param nFBlock - The MOST Function Block Identifier
     *  \param nInst - The MOST Function Block Instance Identifier
     *  \param nFunc - The MOST Function Block Function Identifier
     *  \param nPayloadLen - The amount of bytes stored in Payload
     *  \param Payload - The pointer to the payload byte array.
     * 
     */
    /*----------------------------------------------------------*/
    virtual void OnMostControlMessage( void *source, uint32_t sourceAddr, uint32_t targetAddr, uint32_t nFBlock,
        uint32_t nInst, uint32_t nFunc, uint8_t nOpType, uint32_t nPayloadLen, const uint8_t *Payload );
    
    /*----------------------------------------------------------*/
    /*! \brief This callback method is as result of the DeviceVersion query.
     *  \param source - Cast this pointer to (CNetworkDevice*), it points to server network device, which received this event.
     *  \param success - true, when the operation was successful.
     *  \param sourceAddr - The device with this MOST node address raised this event.
     *  \param productId - Unique identifier that represents the product name.
     *  \param fwVersion - INIC firmware version: Byte[3] Major, Byte[2] Minor, Byte[1] Release.
     *  \param buildVersion - Build version of the firmware.
     *  \param hwVersion - Chip revision number.
     *  \param diagnosisId - Unique diagnosis identifier of the device.
     *  \param tag - Any 32 bit value. This value was passed to the corresponding function call of the CNetworkDevice class.
     */
    /*----------------------------------------------------------*/
    virtual void OnDeviceVersion( void *source, bool success, uint32_t sourceAddr,
        uint32_t productId, uint32_t fwVersion, uint32_t buildVersion, uint8_t hwVersion, uint16_t diagnosisId, uint32_t tag );

};

#endif //_NETWORKDEVICELISTENER_H_
