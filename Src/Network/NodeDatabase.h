/*
 * Video On Demand Samples
 *
 * Copyright (C) 2015 Microchip Technology Germany II GmbH & Co. KG
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * You may also obtain this software under a propriety license from Microchip.
 * Please contact Microchip for further information.
 *
 */

/*----------------------------------------------------------*/
/*! \file
 *  \brief  This file contains the CNodeEntry and the CNodeConnectionEntry class.
 */
/*----------------------------------------------------------*/
#ifndef NODEDATABASE_H
#define NODEDATABASE_H

#include <stdint.h>
#include <stdbool.h>
#include "SafeVector.h"
#include "VodXml.h"
#include "MacAddr.h"

class CNodeEntry;
class CNodeConnectionEntry;

/*----------------------------------------------------------*/
/*! \brief Enumeration describing the state of a Node socket
 */
/*----------------------------------------------------------*/
typedef enum NodeConnectionState_tag
{
    NodeConnection_NotUsed,
    NodeConnection_Pending_Up,
    NodeConnection_Pending_Down,
    NodeConnection_Used
} NodeConnectionState_t;

/*----------------------------------------------------------*/
/*! \brief Storage class holding informations of a specific MOST connection (Ports)
 */
/*----------------------------------------------------------*/
class CNodeConnectionEntry
{
public:
    /*----------------------------------------------------------*/
    /*! \brief Pointer to the device, which this connection belongs to.
     */
    /*----------------------------------------------------------*/
    CNodeEntry *parentNode;


    /*----------------------------------------------------------*/
    /*! \brief Pointer to the XML generated configuration, holding the user specific part of this connection.
     */
    /*----------------------------------------------------------*/
    CChannelConfiguration *channelConfig;


    /*----------------------------------------------------------*/
    /*! \brief Identifier determinating the connection based on the XML configuration.
     */
    /*----------------------------------------------------------*/
    uint32_t channelId;


    /*----------------------------------------------------------*/
    /*! \brief In-Handle provided from INIC when the In-Socket was created
     */
    /*----------------------------------------------------------*/
    uint32_t inHandle;


    /*----------------------------------------------------------*/
    /*! \brief Out-Handle provided from INIC when the Out-Socket was created
     */
    /*----------------------------------------------------------*/
    uint32_t outHandle;


    /*----------------------------------------------------------*/
    /*! \brief Connection-Handle provided from INIC when the In- and the Out-Socket were connected.
     */
    /*----------------------------------------------------------*/
    uint32_t connectHandle;


    /*----------------------------------------------------------*/
    /*! \brief If this connection uses combiner or splitter, this handle points to the source / sink of the splitter / combiner.
    *  \note The idea of storing this handle, is only for cleanup purposes.
    *  \warning Do not use this handle for creating connections.
    */
    /*----------------------------------------------------------*/
    uint32_t splittedSourceHandle;


    /*----------------------------------------------------------*/
    /*! \brief Connection-Handle provided from INIC when the MOST-Socket was created.
     */
    /*----------------------------------------------------------*/
    uint32_t mostConnectionLabel;


    /*----------------------------------------------------------*/
    /*! \brief The state of the In-Socket
     */
    /*----------------------------------------------------------*/
    NodeConnectionState_t inSocketState;


    /*----------------------------------------------------------*/
    /*! \brief The state of the Out-Socket
     */
    /*----------------------------------------------------------*/
    NodeConnectionState_t outSocketState;


    /*----------------------------------------------------------*/
    /*! \brief The connection state between In-Socket and Out-Socket
     */
    /*----------------------------------------------------------*/
    NodeConnectionState_t connectedSocketState;

    /*----------------------------------------------------------*/
    /*! \brief The used buffer size for this connection.
     *  \note bufferSize is -1 in case, there were no buffers configured.
     */
    /*----------------------------------------------------------*/
    int32_t bufferSize;

    /*----------------------------------------------------------*/
    /*! \brief The Linux character device name, when this connection belongs to a local connection. May be strlen of 0.
     */
    /*----------------------------------------------------------*/
    char deviceName[64];


    /*----------------------------------------------------------*/
    /*! \brief Constructor of CNodeConnectionEntry.
     *  \param Pointer to the device this connection belongs to.
     */
    /*----------------------------------------------------------*/
    CNodeConnectionEntry( CNodeEntry *parentNode );


    /*----------------------------------------------------------*/
    /*! \brief Destructor of CNodeConnectionEntry.
     */
    /*----------------------------------------------------------*/
    ~CNodeConnectionEntry();
};

/*----------------------------------------------------------*/
/*! \brief Storage class holding information of a specific MOST device
 */
/*----------------------------------------------------------*/
class CNodeEntry
{
private:
    static CSafeVector<CNodeEntry *> allNodes;
    CSafeVector<CNodeConnectionEntry *> allConnections;

    CNodeEntry();
    static const char *GetConnectionStateString( NodeConnectionState_t state );
public:
    ~CNodeEntry();
    
    /*----------------------------------------------------------*/
    /*! \brief MOST instance. 0 for the first instance. If the server has multiple MOST devices, this value will be increased.
     */
    /*----------------------------------------------------------*/
    uint8_t deviceInstance;


    /*----------------------------------------------------------*/
    /*! \brief The unique address of the MOST device.
     */
    /*----------------------------------------------------------*/
    uint16_t nodeAddress;


    /*----------------------------------------------------------*/
    /*! \brief The type of the device (derived from the group address), as described in the XML configuration.
     */
    /*----------------------------------------------------------*/
    uint16_t deviceType;


    /*----------------------------------------------------------*/
    /*! \brief The unique MAC address of the device.
     */
    /*----------------------------------------------------------*/
    CMacAddr *macAddress;


    /*----------------------------------------------------------*/
    /*! \brief Determines if the TSI port is opened.
     *  \return true, if the port is opened. false
     */
    /*----------------------------------------------------------*/
    bool isTsiPortOpened;
    
    /*----------------------------------------------------------*/
    /*! \brief Determines if the MLB port is opened.
     *  \return true, if the port is opened. false
     */
    /*----------------------------------------------------------*/
    bool isMlbPortOpened;


    /*----------------------------------------------------------*/
    /*! \brief Determines if the USB port is opened.
     *  \return true, if the port is opened. false
     */
    /*----------------------------------------------------------*/
    bool isUsbPortOpened;


    /*----------------------------------------------------------*/
    /*! \brief Determines if the USB port A is opened.
     *  \return true, if the port is opened. false
     */
    /*----------------------------------------------------------*/
    bool isI2SPortOpened;



    /*----------------------------------------------------------*/
    /*! \brief Gets the amount of available MOST devices.
     *  \return The number of MOST devices.
     */
    /*----------------------------------------------------------*/
    static uint16_t GetAmountOfNodeEntries();


    /*----------------------------------------------------------*/
    /*! \brief Gets an instance of CNodeEntry by the given instance index
     *  \param index - starting at 0 for the first instance. "GetAmountOfNodeEntries() - 1" for the last instance.
     *  \return The requested instance, when successful. Otherwise NULL pointer.
     */
    /*----------------------------------------------------------*/
    static CNodeEntry *GetNodeEntry( uint16_t index );


    /*----------------------------------------------------------*/
    /*! \brief Gets an instance of CNodeEntry by the given MOST instance and the unique node address of the device.
     *  \param deviceInstance - MOST instance id, starting at 0 for the first MOST ring. When the server have multiple MOST devices,
     *                          This value may be above 0.
     *  \param nodeAddress - The MOST node address of the specific device (0x100 e.g.).
     *  \return The requested instance, when successful. Otherwise NULL pointer.
     */
    /*----------------------------------------------------------*/
    static CNodeEntry *GetNodeEntry( uint8_t deviceInstance, uint16_t nodeAddress );



    /*----------------------------------------------------------*/
    /*! \brief Gets an instance of CNodeEntry by the given unique MAC address of the device.
     *  \param macAddress - Unique MAC address of the device
     *  \return The requested instance, when successful. Otherwise NULL pointer.
     */
    /*----------------------------------------------------------*/
    static CNodeEntry *GetNodeEntry( CMacAddr *macAddress );

    /*----------------------------------------------------------*/
    /*! \brief Gets an instance of CNodeEntry by the given device type and terminal instance
     *  \param deviceInstance - MOST instance id, starting at 0 for the first MOST ring. When the server have multiple MOST devices,
     *                          This value may be above 0.
     *  \param deviceType - The type of the device (derived from the group address), as described in the XML configuration.
     *  \param terminalInstance - The instance of the terminal, starting at 0 for the first terminal.
     *
     *  \return The requested instance, when successful. Otherwise NULL pointer.
     */
    /*----------------------------------------------------------*/
    static CNodeEntry *GetNodeEntry( uint8_t deviceInstance, uint32_t deviceType, uint32_t terminalInstance );


    /*----------------------------------------------------------*/
    /*! \brief Gets the terminal instance number for the given device type.
     *  \param deviceInstance - MOST instance id, starting at 0 for the first MOST ring. When the server have multiple MOST devices,
     *                          This value may be above 0.
     *  \param nodeAddress - The MOST node address of the specific device (0x100 e.g.).
     *  \param deviceType - The type of the device (derived from the group address), as described in the XML configuration.
     *
     *  \return The requested instance, when successful. Otherwise NULL pointer.
     */
    /*----------------------------------------------------------*/
    static uint32_t GetTerminalInstance( uint8_t deviceInstance, uint16_t nodeAddress, uint32_t deviceType );

    /*----------------------------------------------------------*/
    /*! \brief Deleting all informations of any device.
     */
    /*----------------------------------------------------------*/
    static void DeleteAllNodeEntries();



    /*----------------------------------------------------------*/
    /*! \brief Deleting all informations of a specific device.
     *  \param index - starting at 0 for the first instance. "GetAmountOfNodeEntries() - 1" for the last instance.
     */
    /*----------------------------------------------------------*/
    static void DeleteNodeEntry( uint16_t index );



    /*----------------------------------------------------------*/
    /*! \brief Dumps out all information of all devices to stdout
     */
    /*----------------------------------------------------------*/
    static void PrintAllInformations();



    /*----------------------------------------------------------*/
    /*! \brief Gets the amount of connection for the current instance of MOST device.
     *  \return The amount of connections.
     */
    /*----------------------------------------------------------*/
    uint16_t GetAmountOfConnections();



    /*----------------------------------------------------------*/
    /*! \brief Gets an instance of CNodeConnectionEntry by the given instance index.
     *  \param index - starting at 0 for the first instance. "GetAmountOfConnections() - 1" for the last instance.
     *  \return The requested instance, when successful. Otherwise NULL pointer.
     */
    /*----------------------------------------------------------*/
    CNodeConnectionEntry *GetConnectionByIndex( uint16_t index );



    /*----------------------------------------------------------*/
    /*! \brief Gets an instance of CNodeConnectionEntry by the given channel identifier.
     *  \param channelId - The channel identifier as specified in the XML file.
     *  \param createNewIfUnknown - TRUE, if this function shall create a new entry, when there is no existing object.
     *  \return The requested instance, when successful. Otherwise NULL pointer.
     */
    /*----------------------------------------------------------*/
    CNodeConnectionEntry *GetConnectionByChannelId( uint32_t channelId, bool createNewIfUnknown );



    /*----------------------------------------------------------*/
    /*! \brief Retrieves a conflicting connection by the given parameters.#
     *        This method checks if the in and out configurations are unique for one device.
     *  \param channelId - The channel identifier as specified in the XML file.
     *  \param opposite - The other side to be connected to.
     *  \return The instance, which causes an conflict. If there is no conflict, a NULL pointer will be returned.
     */
    /*----------------------------------------------------------*/
    CNodeConnectionEntry *GetConflictingConnection( uint32_t channelId, CNodeEntry *opposite );



    /*----------------------------------------------------------*/
    /*! \brief Delets all informations of the given channel identifier.
     *  \param channelId - The channel identifier as specified in the XML file.
     */
    /*----------------------------------------------------------*/
    void DeleteConnection( uint32_t channelId );
};

#endif //NODEDATABASE_H
