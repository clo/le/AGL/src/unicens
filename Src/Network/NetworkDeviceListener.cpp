/*
 * Video On Demand Samples
 *
 * Copyright (C) 2015 Microchip Technology Germany II GmbH & Co. KG
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * You may also obtain this software under a propriety license from Microchip.
 * Please contact Microchip for further information.
 *
 */

#include <stdint.h>
#include <stdio.h>
#include <Board.h>
#include "Console.h"
#include "NetworkDeviceListener.h"

void CNetworkDeviceListener::OnSync( void *source, bool isSynced )
{
    ConsolePrintf( PRIO_LOW, "BASE->CNetworkDeviceListener::OnSync\n" );
}

void CNetworkDeviceListener::OnNetworkState( void *source, bool mpValChanged, bool systemNotOk,
    bool mostAvailable, uint8_t availableSubState, uint8_t availableTransition, uint16_t nodeAddress, uint8_t nodePos,
    uint8_t maxPos, uint16_t packetBW )
{
    ConsolePrintf( PRIO_LOW, "BASE->CNetworkDeviceListener::OnNetworkState\n" );
}

void CNetworkDeviceListener::OnNetworkStartupV3( void *source, bool success )
{
    ConsolePrintf( PRIO_LOW, "BASE->CNetworkDeviceListener::OnNetworkStartupV3, success=%s\n",
        ( success ? GREEN"yes"RESETCOLOR :  RED"no"RESETCOLOR ) );
}

void CNetworkDeviceListener::OnNetworkShutdownV3( void *source, bool success )
{
    ConsolePrintf( PRIO_LOW, "BASE->CNetworkDeviceListener::OnNetworkShutdownV2, success=%s\n",
        ( success ? GREEN"yes"RESETCOLOR :  RED"no"RESETCOLOR ) );
}

void CNetworkDeviceListener::OnMostMacAddress( void *source, bool success, uint16_t nodeAddress, uint8_t macAddress1,
    uint8_t macAddress2, uint8_t macAddress3, uint8_t macAddress4, uint8_t macAddress5, uint8_t macAddress6 )
{
    if( success )
    {
        ConsolePrintf( PRIO_LOW,
            "BASE->OnMostMacAddress:nodeAddress:0x%X, MAC-Address:%02X-%02X-%02X-%02X-%02X-%02X)\n", nodeAddress,
            macAddress1, macAddress2, macAddress3, macAddress4, macAddress5, macAddress6 );
    }
    else
    {
        ConsolePrintf( PRIO_ERROR,
            RED"BASE->OnMostMacAddress reports failure for nodeAddress:0x%X"RESETCOLOR"\n", nodeAddress );
    }
}

void CNetworkDeviceListener::OnMostDeviceType( void *source, bool success, uint16_t nodeAddress, uint16_t deviceType )
{
    ConsolePrintf( PRIO_LOW, "BASE->CNetworkDeviceListener::OnMostDeviceType, success=%s\n",
        ( success ? GREEN"yes"RESETCOLOR :  RED"no"RESETCOLOR ) );
}

void CNetworkDeviceListener::OnInitCompleteV1( void *source )
{
    ConsolePrintf( PRIO_LOW, "BASE->CNetworkDeviceListener::OnInitCompleteV1\n" );
}

void CNetworkDeviceListener::OnNetOnV1( void *source, bool isMaster )
{
    ConsolePrintf( PRIO_LOW, "BASE->CNetworkDeviceListener::OnNetOnV1, mode=%s\n", ( isMaster ? "master" : "slave" ) );
}

void CNetworkDeviceListener::OnShutDownV1( void *source )
{
    ConsolePrintf( PRIO_LOW, "BASE->CNetworkDeviceListener::OnShutDownV1\n" );
}

void CNetworkDeviceListener::OnNprV1( void *source, uint8_t npr )
{
    ConsolePrintf( PRIO_LOW, "BASE->CNetworkDeviceListener::OnNprV1, NPR=%d\n", npr );
}

void CNetworkDeviceListener::OnUnlockV1( void *source )
{
    ConsolePrintf( PRIO_LOW, "BASE->CNetworkDeviceListener::OnUnlockV1\n" );
}

void CNetworkDeviceListener::OnMprV1( void *source, uint8_t oldMpr, uint8_t newMpr )
{
    ConsolePrintf( PRIO_LOW, "BASE->CNetworkDeviceListener::OnMprV1, old MPR=%d, new MPR:%d\n", oldMpr, newMpr );
}

void CNetworkDeviceListener::OnOpenTsiV1( void *source, bool success, uint16_t nodeAddress )
{
    ConsolePrintf( PRIO_LOW, "BASE->CNetworkDeviceListener::OnOpenTsiV1, success=%s\n",
        ( success ? GREEN"yes"RESETCOLOR :  RED"no"RESETCOLOR ) );
}

void CNetworkDeviceListener::OnCreateTsiSocketV1( void *source, bool success, uint16_t nodeAddr, V1TsiPortInstance_t tsiPortInst,
    EPDataType_t epType, EPDirection_t epDir, uint16_t blockWidthTsi, uint16_t socketHandle, uint32_t tag )
{
    ConsolePrintf( PRIO_LOW, "BASE->CNetworkDeviceListener::OnCreateTsiSocketV1, success=%s\n",
        ( success ? GREEN"yes"RESETCOLOR :  RED"no"RESETCOLOR ) );
}

void CNetworkDeviceListener::OnOpenMlbV1( void *source, bool success, uint16_t nodeAddress )
{
    ConsolePrintf( PRIO_LOW, "BASE->CNetworkDeviceListener::OnOpenMlbV1, success=%s\n",
        ( success ? GREEN"yes"RESETCOLOR :  RED"no"RESETCOLOR ) );
}

void CNetworkDeviceListener::OnCreateMlbSocketV1( void *source, bool success, uint16_t nodeAddr, EPDataType_t epType,
    EPDirection_t epDir, uint16_t blockWidthMlb, uint8_t mlbChannelAddress, uint16_t socketHandle, uint32_t tag )
{
    ConsolePrintf( PRIO_LOW, "BASE->CNetworkDeviceListener::OnCreateMlbSocketV1, success=%s\n",
        ( success ? GREEN"yes"RESETCOLOR :  RED"no"RESETCOLOR ) );
}

void CNetworkDeviceListener::OnCreateMostSocketV1( void *source, bool success, uint16_t nodeAddr, EPDataType_t epType,
    EPDirection_t epDir, uint16_t blockwidthMost, uint16_t connectionLabel, uint16_t socketHandle, uint32_t tag )
{
    ConsolePrintf( PRIO_LOW, "BASE->CNetworkDeviceListener::OnCreateMostSocketV1, success=%s\n",
        ( success ? GREEN"yes"RESETCOLOR :  RED"no"RESETCOLOR ) );
}

void CNetworkDeviceListener::OnConnectSocketsV1( void *source, bool success, uint16_t nodeAddr,
    uint16_t inSocketHandle, uint16_t outSocketHandle, uint16_t connectionHandle, uint32_t tag )
{
    ConsolePrintf( PRIO_LOW, "BASE->CNetworkDeviceListener::OnConnectSocketsV1, success=%s\n",
        ( success ? GREEN"yes"RESETCOLOR :  RED"no"RESETCOLOR ) );
}

void CNetworkDeviceListener::OnDestroySocketV1( void *source, bool success, uint16_t nodeAddr, uint16_t handle,
    uint32_t tag )
{
    ConsolePrintf( PRIO_LOW, "BASE->CNetworkDeviceListener::OnDestroySocketV1, success=%s\n",
        ( success ? GREEN"yes"RESETCOLOR :  RED"no"RESETCOLOR ) );
}

void CNetworkDeviceListener::OnDisconnectSocketsV1( void *source, bool success, uint16_t nodeAddr, uint16_t handle,
    uint32_t tag )
{
    ConsolePrintf( PRIO_LOW, "BASE->CNetworkDeviceListener::OnDisconnectSocketsV1, success=%s\n",
        ( success ? GREEN"yes"RESETCOLOR :  RED"no"RESETCOLOR ) );
}

void CNetworkDeviceListener::OnOpenI2SPortV1( void *source, bool success, uint16_t nodeAddress,
    V1I2SPortClkDriveMode_t portMode, V1I2SStreamingDataFormat_t format, uint32_t tag )
{
    ConsolePrintf( PRIO_LOW, "BASE->CNetworkDeviceListener::OnOpenI2SPortV1, success=%s\n",
        ( success ? GREEN"yes"RESETCOLOR :  RED"no"RESETCOLOR ) );
}

void CNetworkDeviceListener::OnCreateI2SSocketV1( void *source, bool success, uint16_t nodeAddr, EPDirection_t epDir,
    uint16_t blockWidthI2S, V1I2SPin_t pin, uint16_t socketHandle, uint32_t tag )
{
    ConsolePrintf( PRIO_LOW, "BASE->CNetworkDeviceListener::OnCreateI2SSocketV1, success=%s\n",
        ( success ? GREEN"yes"RESETCOLOR :  RED"no"RESETCOLOR ) );
}

void CNetworkDeviceListener::OnOpenMlbV3( void *source, bool success, uint16_t nodeAddress, uint16_t mlbPortHandle )
{
    ConsolePrintf( PRIO_LOW, "BASE->CNetworkDeviceListener::OnOpenMlbV2, success=%s, Handle:%X\n",
        ( success ? GREEN"yes"RESETCOLOR :  RED"no"RESETCOLOR ), mlbPortHandle );
}

void CNetworkDeviceListener::OnCreateUsbSocketV3( void *source, bool success, uint16_t nodeAddr, EPDataType_t epType,
    EPDirection_t epDir, uint8_t endPointAddress, uint16_t socketHandle, uint32_t tag )
{
    ConsolePrintf( PRIO_LOW, "BASE->CNetworkDeviceListener::OnCreateUsbSocketV2, success=%s\n",
        ( success ? GREEN"yes"RESETCOLOR :  RED"no"RESETCOLOR ) );
}

void CNetworkDeviceListener::OnCreateSplittedUsbSocketV3( void *source, bool success, uint16_t nodeAddr,
    EPDataType_t epType, EPDirection_t epDir, uint8_t endPointAddress, uint16_t usbHandle, uint16_t splitterHandle,
    uint32_t tag )
{
    ConsolePrintf( PRIO_LOW, "BASE->CNetworkDeviceListener::OnCreateSplittedUsbSocketV2, success=%s\n",
        ( success ? GREEN"yes"RESETCOLOR :  RED"no"RESETCOLOR ) );
}

void CNetworkDeviceListener::OnCreateMlbSocketV3( void *source, bool success, uint16_t nodeAddr, EPDataType_t epType,
    EPDirection_t epDir, uint16_t blockWidthMlb, uint8_t mlbChannelAddress, uint16_t socketHandle, uint32_t tag )
{
    ConsolePrintf( PRIO_LOW, "BASE->CNetworkDeviceListener::OnCreateMlbSocketV2, success=%s\n",
        ( success ? GREEN"yes"RESETCOLOR :  RED"no"RESETCOLOR ) );
}

void CNetworkDeviceListener::OnCreateSplittedMlbSocketV3( void *source, bool success, uint16_t nodeAddr,
    EPDataType_t epType, EPDirection_t epDir, uint16_t blockWidthMlb, uint8_t mlbChannelAddress,
    uint16_t mlbSocketHandle, uint16_t splittertHandle, uint32_t tag )
{
    ConsolePrintf( PRIO_LOW, "BASE->CNetworkDeviceListener::OnCreateSplittedMlbSocketV2, success=%s\n",
        ( success ? GREEN"yes"RESETCOLOR :  RED"no"RESETCOLOR ) );
}

void CNetworkDeviceListener::OnCreateMostSocketV3( void *source, bool success, uint16_t nodeAddr, EPDataType_t epType,
    EPDirection_t epDir, uint16_t blockwidthMost, uint16_t connectionLabel, uint16_t socketHandle, uint32_t tag )
{
    ConsolePrintf( PRIO_LOW, "BASE->CNetworkDeviceListener::OnCreateMostSocketV2, success=%s\n",
        ( success ? GREEN"yes"RESETCOLOR :  RED"no"RESETCOLOR ) );
}

void CNetworkDeviceListener::OnConnectSocketsV3( void *source, bool success, uint16_t nodeAddr, EPDataType_t epType,
    uint16_t inSocketHandle, uint16_t outSocketHandle, uint16_t connectionHandle, uint32_t tag )
{
    ConsolePrintf( PRIO_LOW, "BASE->CNetworkDeviceListener::OnConnectSocketsV2, success=%s\n",
        ( success ? GREEN"yes"RESETCOLOR :  RED"no"RESETCOLOR ) );
}


void CNetworkDeviceListener::OnResourceDestroyV3( void *source, bool success, uint16_t nodeAddress,
    uint8_t amountOfHandles, const uint16_t *pHandle, uint32_t tag )
{
    ConsolePrintf( PRIO_LOW, "BASE->CNetworkDeviceListener::OnResourceDestroyV2, success=%s\n",
        ( success ? GREEN"yes"RESETCOLOR :  RED"no"RESETCOLOR ) );
}

void CNetworkDeviceListener::OnConfigureI2SPortV3( void *source, bool success, uint16_t nodeAddr, uint8_t portInstance,
    V3I2SPortOption_t option, V3I2SClockMode_t mode, V3I2SDelayMode_t delay, uint32_t tag )
{
    ConsolePrintf( PRIO_LOW, "BASE->CNetworkDeviceListener::OnConfigureI2SPortV2, success=%s\n",
        ( success ? GREEN"yes"RESETCOLOR :  RED"no"RESETCOLOR ) );
}

void CNetworkDeviceListener::OnCreateI2SPortV3( void *source, bool success, uint16_t nodeAddr, uint8_t portInstance,
    V3I2SPortSpeed_t clock, V3I2SAlignment_t align, uint32_t tag )
{
    ConsolePrintf( PRIO_LOW, "BASE->CNetworkDeviceListener::OnCreateI2SPortV2, success=%s\n",
        ( success ? GREEN"yes"RESETCOLOR :  RED"no"RESETCOLOR ) );
}

void CNetworkDeviceListener::OnCreateI2SSocketV3( void *source, bool success, uint16_t nodeAddr, uint8_t portInstance,
    EPDirection_t epDir, uint16_t blockWidthI2S, V3I2SPin_t pin, uint16_t socketHandle, uint32_t tag )
{
    ConsolePrintf( PRIO_LOW, "BASE->CNetworkDeviceListener::OnCreateI2SSocketV2, success=%s\n",
        ( success ? GREEN"yes"RESETCOLOR :  RED"no"RESETCOLOR ) );
}

void CNetworkDeviceListener::OnCreateSplittedI2SSocketV3( void *source, bool success, uint16_t nodeAddr,
    uint8_t portInstance, EPDirection_t epDir, uint16_t blockWidthI2S, V3I2SPin_t pin, uint16_t i2sSocketHandle,
    uint16_t splitterHandle, uint32_t tag )
{
    ConsolePrintf( PRIO_LOW, "BASE->CNetworkDeviceListener::OnCreateSplittedI2SSocketV2, success=%s\n",
        ( success ? GREEN"yes"RESETCOLOR :  RED"no"RESETCOLOR ) );
}

void CNetworkDeviceListener::OnRbdResultV3( void *source, uint16_t nodeAddress, uint8_t result, uint8_t position,
        uint8_t status, uint16_t id )
{
    ConsolePrintfStart( PRIO_HIGH, YELLOW"BASE->OnRbdResultV3: Diagnosis Result from node=0x%X, result=0x%X (",
        nodeAddress, result );
    switch( result )
    {
    case 0:
        ConsolePrintfContinue( "NoError" );
        break;
    case 1:
        ConsolePrintfContinue( "PosDetected" );
        break;
    case 2:
        ConsolePrintfContinue( "DiagFailed" );
        break;
    case 3:
        ConsolePrintfContinue( "Pos0WeakSig" );
        break;
    case 0xFF:
        ConsolePrintfContinue( "No result" );
        break;
    default:
        ConsolePrintfContinue( "UNKNOWN" );
        break;
    }
    ConsolePrintfExit( ") Position: %d"RESETCOLOR"\n", position );
}

void CNetworkDeviceListener::OnControlChannelReadEnd( void *source )
{
    ConsolePrintf( PRIO_LOW, "BASE->OnControlChannelReadEnd\n" );
}

void CNetworkDeviceListener::OnMostControlMessage( void *source, uint32_t sourceAddr, uint32_t targetAddr,
    uint32_t nFBlock, uint32_t nInst, uint32_t nFunc, uint8_t nOpType, uint32_t nPayloadLen, const uint8_t *Payload )
{
    ConsolePrintf( PRIO_LOW, "Base->OnMostControlMessage\n" );
}

void CNetworkDeviceListener::OnDeviceVersion( void *source, bool success, uint32_t sourceAddr, uint32_t productId,
        uint32_t fwVersion, uint32_t buildVersion, uint8_t hwVersion, uint16_t diagnosisId, uint32_t tag )
{
    ConsolePrintf( PRIO_LOW, "BASE->OnDeviceVersion::OnDeviceVersion, success=%s\n",
        ( success ? GREEN"yes"RESETCOLOR :  RED"no"RESETCOLOR ) );
}
