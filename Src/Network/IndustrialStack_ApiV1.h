/*
 * Video On Demand Samples
 *
 * Copyright (C) 2015 Microchip Technology Germany II GmbH & Co. KG
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * You may also obtain this software under a propriety license from Microchip.
 * Please contact Microchip for further information.
 *
 */

/*----------------------------------------------------------*/
/*! \file
 *  \brief  This file contains the CIndustrialStack class (API V1 commands).
 */
/*----------------------------------------------------------*/

#ifndef INDUSTRIAL_STACK_API_V1_H
#define INDUSTRIAL_STACK_API_V1_H

#define ISO_PACKET_SIZE 188

#include "IndustrialStack.h"
#include "Network.h"
#include "Types.h"

class CV1_OnMostRx : public CSInternalEvent
{
private:
    CNetworkDevice *device;
public:
    CV1_OnMostRx(CNetworkDevice *d) : device(d)    
    {
    }
    
    virtual ISReturn_t OnMostMessage(CIndustrialStack *iStack, CISMostMsg *r)
    {
        assert(NULL != device);
        assert(r->IsValid);
        if (0x0 == r->FBlock) //INIC Block
        {
            switch (r->Func)
            {
            case 0x30E: //Change EUI48
                if (CISOpType_STATUS == r->OpType && r->PayloadLen >= 6)
                {
                    for( uint32_t i = 0; NULL != device && i < device->GetAmountOfListners(); i++ )
                        device->GetListener( i )->OnMostMacAddress(device, true,
                        r->SourceAddress, r->Payload[0], r->Payload[1], 
                        r->Payload[2], r->Payload[3], r->Payload[4], r->Payload[5]);
                }
                else if (CISOpType_ERROR == r->OpType)
                {
                    for( uint32_t i = 0; NULL != device && i < device->GetAmountOfListners(); i++ )
                        device->GetListener( i )->OnMostMacAddress(device, false, 
                            r->SourceAddress, 0,0,0,0,0,0);
                }
                break;
            }
        }
        return ISReturn_NoChange;
    }
    
    virtual void OnControlReadEnd(CIndustrialStack *iStack)
    {
        assert(NULL != device);
        for( uint32_t i = 0; NULL != device && i < device->GetAmountOfListners(); i++ )
            device->GetListener( i )->OnControlChannelReadEnd(device);
    }
};

class CV1_ChangeEUI48 : public CISSendMostMsgElement, public IISElementCallback
{
private:
    CNetworkDevice *device;
public:
    CV1_ChangeEUI48(CNetworkDevice *d, uint16_t nodeAddress, 
        uint8_t macAddress1, uint8_t macAddress2, uint8_t macAddress3, 
        uint8_t macAddress4, uint8_t macAddress5, uint8_t macAddress6,
        bool persistent) : device(d)
    {
        ElementName = "CV1_ChangeEUI48";
        WaitForResponse = true;
        WaitForResponseOpType = CISOpType_RESULT;
        Callback = this;
        
        Request.IsValid = true;
        Request.SourceAddress = 0x2;
        Request.TargetAddress = nodeAddress;
        Request.FBlock = 0x0;
        Request.Func = 0x30E;
        Request.Inst = 0x00;
        Request.OpType = CISOpType_STARTRESULT;
        Request.PayloadLen = 7;
        Request.Payload[0] = macAddress1;
        Request.Payload[1] = macAddress2;
        Request.Payload[2] = macAddress3;
        Request.Payload[3] = macAddress4;
        Request.Payload[4] = macAddress5;
        Request.Payload[5] = macAddress6;
        Request.Payload[6] = persistent;
    }
    
    virtual void ElementProcessed(CIndustrialStack *iStack, ISReturn_t result, IISElement *element)
    {
        assert(this == element);
        bool success = (ISReturn_Success == result) && Response.PayloadLen >= 6;
        if (success)
            for( uint32_t i = 0; NULL != device && i < device->GetAmountOfListners(); i++ )
                device->GetListener( i )->OnMostMacAddress(device, true, 
                    Request.TargetAddress, Response.Payload[0],Response.Payload[1], 
                    Response.Payload[2], Response.Payload[3], Response.Payload[4],
                    Response.Payload[5]);
        else
            for( uint32_t i = 0; NULL != device && i < device->GetAmountOfListners(); i++ )
                device->GetListener( i )->OnMostMacAddress(device, false,
                    Request.TargetAddress, 0, 0, 0, 0, 0, 0);
    }
};

class CV1_NetworkStartup : public CISWaitElement
{
private:
    CNetworkDevice *device;
public:
    CV1_NetworkStartup(CNetworkDevice *d, bool isTimingMaster, uint16_t packetBW) : device(d)
    {
        ElementName = "CV1_NetworkStartup is not implemented!";
        timeout = 1;
    }
};

class CV1_NetworkShutdown : public CISWaitElement
{
private:
    CNetworkDevice *device;
public:
    CV1_NetworkShutdown(CNetworkDevice *d) : device(d)
    {
        ElementName = "CV1_NetworkShutdown is not implemented!";
        timeout = 1;
    }
};

class CV1_TsiPortCreate : public CISSendMostMsgElement, public IISElementCallback
{
private:
    CNetworkDevice *device;
public:
    CV1_TsiPortCreate(CNetworkDevice *d, uint16_t nodeAddress, V1TsiPortInstance_t tsiPort, V1TsiPortMode tsiMode) : device(d)
    {
        ElementName = "CV2_TsiPortCreate";
        WaitForResponse = true;
        WaitForResponseOpType = CISOpType_RESULT;
        Callback = this;
        
        Request.IsValid = true;
        Request.SourceAddress = 0x2;
        Request.TargetAddress = nodeAddress;
        Request.FBlock = 0x0;
        Request.Func = 0x400;
        Request.Inst = 0x00;
        Request.OpType = CISOpType_STARTRESULT;
        Request.PayloadLen = 2;
        Request.Payload[0] = tsiPort;
        Request.Payload[1] = tsiMode;
    }
    
    virtual void ElementProcessed(CIndustrialStack *iStack, ISReturn_t result, IISElement *element)
    {
        for( uint32_t i = 0; NULL != device && i < device->GetAmountOfListners(); i++ )
            device->GetListener( i )->OnOpenTsiV1(device, (ISReturn_Success == result), Request.TargetAddress);
    }
};

class CV1_TsiSocketCreate : public CISSendMostMsgElement, public IISElementCallback
{
private:
    CNetworkDevice *device;
    V1TsiPortInstance_t tsiPort;
    EPDataType_t epType;
    EPDirection_t epDir;
    uint16_t blockWidthTsi;
    uint32_t tag;
public:
    CV1_TsiSocketCreate(CNetworkDevice *d, uint16_t nodeAddress, V1TsiPortInstance_t tp, 
        EPDataType_t ept, EPDirection_t epd, uint16_t bw, uint32_t t) : 
        device(d), tsiPort(tp), epType(ept), epDir(epd), blockWidthTsi(bw), tag(t)
    {
        if ( EP_Isochron != epType )
        {
            ConsolePrintf( PRIO_ERROR, RED"CV1_TsiSocketCreate: Unsupported Data type"RESETCOLOR"\n" );
                return;
        }
        ElementName = "CV1_TsiSocketCreate";
        WaitForResponse = true;
        WaitForResponseOpType = CISOpType_RESULT;
        Callback = this;
        
        Request.IsValid = true;
        Request.SourceAddress = 0x2;
        Request.TargetAddress = nodeAddress;
        Request.FBlock = 0x0;
        Request.Func = 0x403;
        Request.Inst = 0x00;
        Request.OpType = CISOpType_STARTRESULT;
        Request.PayloadLen = 7;
        Request.Payload[0] = tsiPort;
        Request.Payload[1] = epDir;
        Request.Payload[2] = epType;
        Request.Payload[3] = blockWidthTsi / 256;
        Request.Payload[4] = blockWidthTsi % 256;
        Request.Payload[5] = ISO_PACKET_SIZE / 256;
        Request.Payload[6] = ISO_PACKET_SIZE % 256;
    }
    
    virtual void ElementProcessed(CIndustrialStack *iStack, ISReturn_t result, IISElement *element)
    {
        uint8_t tsiPortHandle = 0xFF;
        bool success = (ISReturn_Success == result) && Response.PayloadLen >= 1;
        if (success)
            tsiPortHandle =  Response.Payload[0];
        for( uint32_t i = 0; NULL != device && i < device->GetAmountOfListners(); i++ )
            device->GetListener( i )->OnCreateTsiSocketV1(device, success, Request.TargetAddress, tsiPort,
                epType, epDir, blockWidthTsi, tsiPortHandle, tag);
    }
};

class CV1_MlbPortCreate : public CISSendMostMsgElement, public IISElementCallback
{
private:
    CNetworkDevice *device;
public:
    CV1_MlbPortCreate(CNetworkDevice *d, uint16_t nodeAddress, MlbPortSpeed_t mlbSpeed) : device(d)
    {
        ElementName = "CV2_MlbPortCreate";
        WaitForResponse = true;
        WaitForResponseOpType = CISOpType_RESULT;
        Callback = this;
        
        Request.IsValid = true;
        Request.SourceAddress = 0x2;
        Request.TargetAddress = nodeAddress;
        Request.FBlock = 0x0;
        Request.Func = 0x400;
        Request.Inst = 0x00;
        Request.OpType = CISOpType_STARTRESULT;
        Request.PayloadLen = 4;
        Request.Payload[0] = 0x00; //SCM_PORT_ID_MEDIALB;
        Request.Payload[1] = 0xFF;
        Request.Payload[2] = 0x00; //SCM_PORT_CFG_MLB_MODE_CTL;
        Request.Payload[3] = mlbSpeed;
    }
    
    virtual void ElementProcessed(CIndustrialStack *iStack, ISReturn_t result, IISElement *element)
    {
        for( uint32_t i = 0; NULL != device && i < device->GetAmountOfListners(); i++ )
            device->GetListener( i )->OnOpenMlbV1(device, (ISReturn_Success == result), Request.TargetAddress);
    }
};

class CV1_MlbSocketCreate : public CISSendMostMsgElement, public IISElementCallback
{
private:
    CNetworkDevice *device;
    EPDataType_t epType;
    EPDirection_t epDir;
    uint16_t mlbChannelAddress;
    uint16_t blockWidthMlb;
    uint32_t tag;
public:
    CV1_MlbSocketCreate(CNetworkDevice *d, uint16_t nodeAddress, EPDataType_t ept, EPDirection_t epd,
        uint16_t mlb, uint16_t bw, uint32_t t) : device(d), epType(ept), epDir(epd),
        mlbChannelAddress(mlb), blockWidthMlb(bw), tag(t)
    {
        ElementName = "CV1_MlbSocketCreate";
        WaitForResponse = true;
        WaitForResponseOpType = CISOpType_RESULT;
        Callback = this;
        
        Request.IsValid = true;
        Request.SourceAddress = 0x2;
        Request.TargetAddress = nodeAddress;
        Request.FBlock = 0x0;
        Request.Func = 0x403;
        Request.Inst = 0x00;
        Request.OpType = CISOpType_STARTRESULT;
        Request.PayloadLen = 7;
        Request.Payload[0] = 0x00; //SCM_PORT_ID_MEDIALB
        Request.Payload[1] = epDir;
        Request.Payload[2] = epType;
        Request.Payload[3] = blockWidthMlb / 256;
        Request.Payload[4] = blockWidthMlb % 256;
        Request.Payload[5] = mlbChannelAddress / 256;
        Request.Payload[6] = mlbChannelAddress % 256;
        if ( EP_Isochron == epType )
        {
            Request.PayloadLen = 10;
            Request.Payload[7] = ISO_PACKET_SIZE / 256;
            Request.Payload[8] = ISO_PACKET_SIZE % 256;
            Request.Payload[9] = 0x1; //SCM_FLOW_CONTROL_ON
        }
    }
    
    virtual void ElementProcessed(CIndustrialStack *iStack, ISReturn_t result, IISElement *element)
    {
        uint8_t mlbPortHandle = 0xFF;
        bool success = (ISReturn_Success == result) && Response.PayloadLen >= 1;
        if (success)
            mlbPortHandle =  Response.Payload[0];
        for( uint32_t i = 0; NULL != device && i < device->GetAmountOfListners(); i++ )
            device->GetListener( i )->OnCreateMlbSocketV1(device, success, Request.TargetAddress, epType,
                epDir, blockWidthMlb, mlbChannelAddress, mlbPortHandle, tag);
    }
};

class CV1_SplittedMlbSocketCreate : public CISSendMostMsgElement, public IISElementCallback
{
private:
    CNetworkDevice *device;
    EPDataType_t epType;
    EPDirection_t epDir;
    uint16_t mlbChannelAddress;
    uint16_t blockWidthMlb;
    uint16_t splittedOffset;
    uint16_t blockWidthCombined;
    uint32_t tag;
public:
    CV1_SplittedMlbSocketCreate(CNetworkDevice *d, uint16_t nodeAddress, 
        EPDataType_t ept, EPDirection_t epd, uint16_t mlb, uint16_t bw, 
        uint16_t so, uint16_t bwc, uint32_t t) 
        : device(d), epType(ept), epDir(epd), mlbChannelAddress(mlb), 
        blockWidthMlb(bw), splittedOffset(so), blockWidthCombined(bwc), tag(t)
    {
        ElementName = "CV1_SplittedMlbSocketCreate";
        WaitForResponse = true;
        WaitForResponseOpType = CISOpType_RESULT;
        Callback = this;
        
        Request.IsValid = true;
        Request.SourceAddress = 0x2;
        Request.TargetAddress = nodeAddress;
        Request.FBlock = 0x0;
        Request.Func = 0x403;
        Request.Inst = 0x00;
        Request.OpType = CISOpType_STARTRESULT;
        Request.PayloadLen = 9;
        Request.Payload[0] = 0x00; //SCM_PORT_ID_MEDIALB
        Request.Payload[1] = epDir;
        Request.Payload[2] = epType;
        Request.Payload[3] = blockWidthMlb / 256;
        Request.Payload[4] = blockWidthMlb % 256;
        Request.Payload[5] = mlbChannelAddress / 256;
        Request.Payload[6] = mlbChannelAddress % 256;
        Request.Payload[7] = ( uint8_t )( splittedOffset & 0xFF );
        Request.Payload[8] = ( uint8_t )( blockWidthCombined & 0xFF );
    }
    
    virtual void ElementProcessed(CIndustrialStack *iStack, ISReturn_t result, IISElement *element)
    {
        uint8_t mlbPortHandle = 0xFF;
        bool success = (ISReturn_Success == result) && Response.PayloadLen >= 1;
        if (success)
            mlbPortHandle =  Response.Payload[0];
        for( uint32_t i = 0; NULL != device && i < device->GetAmountOfListners(); i++ )
            device->GetListener( i )->OnCreateMlbSocketV1(device, success, Request.TargetAddress, epType,
                epDir, blockWidthMlb, mlbChannelAddress, mlbPortHandle, tag);
    }
};

class CV1_MostSocketCreate : public CISSendMostMsgElement, public IISElementCallback
{
private:
    CNetworkDevice *device;
    EPDataType_t epType;
    EPDirection_t epDir;
    uint16_t connectionLabel;
    uint16_t blockWidthMost;
    uint32_t tag;
public:
    CV1_MostSocketCreate(CNetworkDevice *d, uint16_t nodeAddress, EPDataType_t ept, EPDirection_t epd,
        uint16_t cl, uint16_t bw, uint32_t t) : device(d), epType(ept), epDir(epd),
        connectionLabel(cl), blockWidthMost(bw), tag(t)
    {
        ElementName = "CV1_MostSocketCreate";
        WaitForResponse = true;
        WaitForResponseOpType = CISOpType_RESULT;
        Callback = this;
        
        Request.IsValid = true;
        Request.SourceAddress = 0x2;
        Request.TargetAddress = nodeAddress;
        Request.FBlock = 0x0;
        Request.Func = 0x403;
        Request.Inst = 0x00;
        Request.OpType = CISOpType_STARTRESULT;
        Request.Payload[0] = 0x02; //SCM_PORT_ID_MOST
        Request.Payload[1] = epDir;
        Request.Payload[2] = epType;
        Request.Payload[3] = blockWidthMost / 256;
        Request.Payload[4] = blockWidthMost % 256;
        switch( epType )
        {
        case EP_Isochron:
            Request.Payload[5] = ISO_PACKET_SIZE / 256;
            Request.Payload[6] = ISO_PACKET_SIZE % 256;
            if( SCM_IN == epDir )
            {
                Request.PayloadLen = 10;
                Request.Payload[7] = 0x1; //SCM_PA_CL
                Request.Payload[8] = connectionLabel / 256;
                Request.Payload[9] = connectionLabel % 256;
            }
            else
            {
                Request.PayloadLen = 8;
                Request.Payload[7] = 0x0; //SCM_NOT_PA
            }
            break;
        case EP_Synchron:
            if( SCM_IN == epDir )
            {
                Request.PayloadLen = 8;
                Request.Payload[5] = 0x1; //SCM_PA_CL
                Request.Payload[6] = connectionLabel / 256;
                Request.Payload[7] = connectionLabel % 256;
            }
            else
            {
                Request.PayloadLen = 6;
                Request.Payload[5] = 0x0; //SCM_NOT_PA
            }
            break;
        default:
            timeout = 1;
            ConsolePrintf( PRIO_ERROR, RED"CreateMostSocket: Unsupported Data type"RESETCOLOR"\n" );
            return;
        }
    }
    
    virtual void ElementProcessed(CIndustrialStack *iStack, ISReturn_t result, IISElement *element)
    {
        uint8_t usbSocketHandle = 0xFF;
        bool success = (ISReturn_Success == result) && Response.PayloadLen >= 1;
        if (success)
            usbSocketHandle =  Response.Payload[0];
        if( success && SCM_OUT == epDir && Response.PayloadLen >= 4)
            connectionLabel = ( Response.Payload[2] << 8) | Response.Payload[3];
        for( uint32_t i = 0; NULL != device && i < device->GetAmountOfListners(); i++ )
            device->GetListener( i )->OnCreateMostSocketV1(device, success, Request.TargetAddress, epType,
                epDir, blockWidthMost, connectionLabel, usbSocketHandle, tag);
    }
};

class CV1_ConnectSockets : public CISSendMostMsgElement, public IISElementCallback
{
private:
    CNetworkDevice *device;
    uint16_t nodeAddress;
    EPDataType_t epType;
    uint16_t inHandle;
    uint16_t outHandle;
    uint32_t tag;
public:
    CV1_ConnectSockets(CNetworkDevice *d, uint16_t nodeAddress, uint16_t iH, 
        uint16_t oH, uint32_t t) : device(d), inHandle(iH), outHandle(oH), tag(t)
    {
        ElementName = "CV1_ConnectSockets";
        WaitForResponse = true;
        WaitForResponseOpType = CISOpType_RESULT;
        Callback = this;
        
        Request.IsValid = true;
        Request.SourceAddress = 0x2;
        Request.TargetAddress = nodeAddress;
        Request.FBlock = 0x0;
        Request.Func = 0x405;
        Request.Inst = 0x00;
        Request.OpType = CISOpType_STARTRESULT;
        Request.PayloadLen = 2;
        Request.Payload[0] = inHandle;
        Request.Payload[1] = outHandle;
    }
    
    virtual void ElementProcessed(CIndustrialStack *iStack, ISReturn_t result, IISElement *element)
    {
        uint8_t conHandle = 0xFF;
        bool success = (ISReturn_Success == result) && Response.PayloadLen >= 1;
        if (success)
            conHandle = Response.Payload[0];
        
        for( uint32_t i = 0; NULL != device && i < device->GetAmountOfListners(); i++ )
            device->GetListener( i )->OnConnectSocketsV1(device, success, 
                Request.TargetAddress, inHandle, outHandle, conHandle, tag);
    }
};

class CV1_DestroySocket : public CISSendMostMsgElement, public IISElementCallback
{
private:
    CNetworkDevice *device;
    uint8_t handle;
    uint32_t tag;
public:
    CV1_DestroySocket(CNetworkDevice *d, int16_t nodeAddress, uint8_t h,
        uint32_t t) : device(d), handle(h), tag(t)
    {
        ElementName = "CV1_DestroySocket";
        WaitForResponse = true;
        WaitForResponseOpType = CISOpType_RESULT;
        Callback = this;
        
        Request.IsValid = true;
        Request.SourceAddress = 0x2;
        Request.TargetAddress = nodeAddress;
        Request.FBlock = 0x0;
        Request.Func = 0x404;
        Request.Inst = 0x00;
        Request.OpType = CISOpType_STARTRESULT;
        Request.PayloadLen = 1;
        Request.Payload[0] = handle;
    }
    
    virtual void ElementProcessed(CIndustrialStack *iStack, ISReturn_t result, IISElement *element)
    {
        for( uint32_t i = 0; NULL != device && i < device->GetAmountOfListners(); i++ )
            device->GetListener( i )->OnDestroySocketV1(device, (ISReturn_Success == result), 
                Request.TargetAddress, handle, tag);
    }
};

class CV1_DisconnectSockets : public CISSendMostMsgElement, public IISElementCallback
{
private:
    CNetworkDevice *device;
    uint8_t handle;
    uint32_t tag;
public:
    CV1_DisconnectSockets(CNetworkDevice *d, int16_t nodeAddress, uint8_t h,
        uint32_t t) : device(d), handle(h), tag(t)
    {
        ElementName = "CV1_DisconnectSockets";
        WaitForResponse = true;
        WaitForResponseOpType = CISOpType_RESULT;
        Callback = this;
        
        Request.IsValid = true;
        Request.SourceAddress = 0x2;
        Request.TargetAddress = nodeAddress;
        Request.FBlock = 0x0;
        Request.Func = 0x406;
        Request.Inst = 0x00;
        Request.OpType = CISOpType_STARTRESULT;
        Request.PayloadLen = 1;
        Request.Payload[0] = handle;
    }
    
    virtual void ElementProcessed(CIndustrialStack *iStack, ISReturn_t result, IISElement *element)
    {
        for( uint32_t i = 0; NULL != device && i < device->GetAmountOfListners(); i++ )
            device->GetListener( i )->OnDisconnectSocketsV1(device, (ISReturn_Success == result), 
                Request.TargetAddress, handle, tag);
    }
};

class CV1_StreamPortOpen : public CISSendMostMsgElement, public IISElementCallback
{
private:
    CNetworkDevice *device;
    V1I2SPortClkDriveMode_t portMode;
    V1I2SStreamingPortMode_t streamPortMode;
    V1I2SStreamingDataFormat_t format;
    uint32_t tag;
public:
    CV1_StreamPortOpen(CNetworkDevice *d, int16_t nodeAddress, 
        V1I2SPortClkDriveMode_t pm, V1I2SStreamingPortMode_t spm, 
        V1I2SStreamingDataFormat_t f, uint32_t t) : device(d), portMode(pm),
        streamPortMode(spm), format(f), tag(t)
    {
        ElementName = "CV1_StreamPortOpen";
        WaitForResponse = true;
        WaitForResponseOpType = CISOpType_RESULT;
        Callback = this;
        
        Request.IsValid = true;
        Request.SourceAddress = 0x2;
        Request.TargetAddress = nodeAddress;
        Request.FBlock = 0x0;
        Request.Func = 0x400;
        Request.Inst = 0x00;
        Request.OpType = CISOpType_STARTRESULT;
        Request.PayloadLen = 4;
        Request.Payload[0] = 0x03; //Streaming Port
        Request.Payload[1] = portMode;
        Request.Payload[2] = streamPortMode;
        Request.Payload[3] = format;
    }
    
    virtual void ElementProcessed(CIndustrialStack *iStack, ISReturn_t result, IISElement *element)
    {
        for( uint32_t i = 0; NULL != device && i < device->GetAmountOfListners(); i++ )
            device->GetListener( i )->OnOpenI2SPortV1(device, (ISReturn_Success == result), 
                Request.TargetAddress, portMode, format, tag);
    }
};

class CV1_StreamSocketCreate : public CISSendMostMsgElement, public IISElementCallback
{
private:
    CNetworkDevice *device;
    EPDirection_t epDir;
    uint16_t blockWidthI2S;
    V1I2SPin_t pin;
    uint32_t tag;
public:
    CV1_StreamSocketCreate(CNetworkDevice *d, uint16_t nodeAddress, EPDirection_t epd,
        uint16_t bw, V1I2SPin_t p, uint32_t t) : device(d), epDir(epd),
        blockWidthI2S(bw), pin(p), tag(t)
    {
        ElementName = "CV1_StreamSocketCreate";
        WaitForResponse = true;
        WaitForResponseOpType = CISOpType_RESULT;
        Callback = this;
        
        Request.IsValid = true;
        Request.SourceAddress = 0x2;
        Request.TargetAddress = nodeAddress;
        Request.FBlock = 0x0;
        Request.Func = 0x403;
        Request.Inst = 0x00;
        Request.OpType = CISOpType_STARTRESULT;
        Request.PayloadLen = 6;
        Request.Payload[0] = 0x03; //Streaming Port
        Request.Payload[1] = epDir;
        Request.Payload[2] = 0x0; //SyncData
        Request.Payload[3] = blockWidthI2S / 256;
        Request.Payload[4] = blockWidthI2S % 256;
        Request.Payload[5] = (uint8_t)pin;
    }
    
    virtual void ElementProcessed(CIndustrialStack *iStack, ISReturn_t result, IISElement *element)
    {
        uint8_t i2sSocketHandle = 0xFF;
        bool success = (ISReturn_Success == result) && Response.PayloadLen >= 1;
        if (success)
            i2sSocketHandle =  Response.Payload[0];
        for( uint32_t i = 0; NULL != device && i < device->GetAmountOfListners(); i++ )
            device->GetListener( i )->OnCreateI2SSocketV1(device, success,
                Request.TargetAddress, epDir, blockWidthI2S, pin, i2sSocketHandle, tag);
    }
};

class CV1_SplittedStreamSocketCreate : public CISSendMostMsgElement, public IISElementCallback
{
private:
    CNetworkDevice *device;
    EPDirection_t epDir;
    uint16_t blockWidthI2S;
    V1I2SPin_t pin;
    uint32_t tag;
public:
    CV1_SplittedStreamSocketCreate(CNetworkDevice *d, uint16_t nodeAddress, EPDirection_t epd,
        uint16_t bw, uint16_t splittedOffset, uint16_t blockWidthCombined, V1I2SPin_t p,
        uint32_t t) : device(d), epDir(epd), blockWidthI2S(bw), pin(p), tag(t)
    {
        ElementName = "CV1_SplittedStreamSocketCreate";
        WaitForResponse = true;
        WaitForResponseOpType = CISOpType_RESULT;
        Callback = this;
        
        Request.IsValid = true;
        Request.SourceAddress = 0x2;
        Request.TargetAddress = nodeAddress;
        Request.FBlock = 0x0;
        Request.Func = 0x403;
        Request.Inst = 0x00;
        Request.OpType = CISOpType_STARTRESULT;
        Request.PayloadLen = 7;
        Request.Payload[0] = 0x03; //Streaming Port
        Request.Payload[1] = epDir;
        Request.Payload[2] = 0x0; //SyncData
        Request.Payload[3] = blockWidthI2S / 256;
        Request.Payload[4] = blockWidthI2S % 256;
        Request.Payload[5] = (uint8_t)pin;
        Request.Payload[6] = ( uint8_t )( splittedOffset & 0xFF );
    }
    
    virtual void ElementProcessed(CIndustrialStack *iStack, ISReturn_t result, IISElement *element)
    {
        uint8_t i2sSocketHandle = 0xFF;
        bool success = (ISReturn_Success == result) && Response.PayloadLen >= 1;
        if (success)
            i2sSocketHandle =  Response.Payload[0];
        for( uint32_t i = 0; NULL != device && i < device->GetAmountOfListners(); i++ )
            device->GetListener( i )->OnCreateI2SSocketV1(device, success,
                Request.TargetAddress, epDir, blockWidthI2S, pin, i2sSocketHandle, tag);
    }
};

#endif //INDUSTRIAL_STACK_API_V1_H