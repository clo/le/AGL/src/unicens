/*
 * Video On Demand Samples
 *
 * Copyright (C) 2015 Microchip Technology Germany II GmbH & Co. KG
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * You may also obtain this software under a propriety license from Microchip.
 * Please contact Microchip for further information.
 *
 */

/*----------------------------------------------------------*/
/*! \file
 *  \brief This component implements the MOST low level driver.
 */
/*----------------------------------------------------------*/
#ifndef INDUSTRIAL_STACK_LLD_H
#define INDUSTRIAL_STACK_LLD_H

#include <stdint.h>
#include <stdbool.h>
#include <semaphore.h>
#include "IndustrialStack_Types.h"

#define BOARD_PMS_RX_QUEUE	256
#define BOARD_PMS_RX_SIZE	64
#define BOARD_PMS_TX_SIZE	64

typedef struct
{
    int16_t payloadLen;
    uint8_t buffer[BOARD_PMS_RX_SIZE];
} QueueEntry_t;

#define QUEUE_TESTPATTERN (0x34547382)
typedef struct
{
    uint32_t testPattern;
    QueueEntry_t dataQueue[BOARD_PMS_RX_QUEUE];
    QueueEntry_t *pRx;
    QueueEntry_t *pTx;
    volatile uint32_t rxPos;
    volatile uint32_t txPos;
} Queue_t;

class CIndustrialStackLld
{
private:
    pthread_t txThread;
    pthread_t rxThread;
public:
    ///Do not use directly! (Called internally from thread context)
    Queue_t txQueue;
    ///Do not use directly! (Called internally from thread context)
    Queue_t rxQueue;
    ///Do not use directly! (Called internally from thread context)
    int hControlRx;
    ///Do not use directly! (Called internally from thread context)
    int hControlTx;
    ///Do not use directly! (Called internally from thread context)
    bool allowThreadRun;
    ///Do not use directly! (Called internally from thread context)
    sem_t txSem;
    
    CIndustrialStackLldCB *listener;
    
    /*----------------------------------------------------------*/
    /*! \brief initializes LLD
     *
     * \param controlRxHandle File handle for the RX character device
     * \param controlRxHandle File handle for the TX character device
     * \return nothing.
     */
    /*----------------------------------------------------------*/
     CIndustrialStackLld(  int controlRxHandle, int controlTxHandle );



    /*----------------------------------------------------------*/
    /*! \brief deinitializes LLD
     */
    /*----------------------------------------------------------*/
    ~CIndustrialStackLld();


    /*----------------------------------------------------------*/
    /*! \brief determins if INIC has something to read
     *
     * \return The amount of data, which will be delivered by the next call of Read
     */
    /*----------------------------------------------------------*/
    uint16_t DataAvailable();


    /*----------------------------------------------------------*/
    /*! \brief receive a control message via USB.
     *
     * \param pData - message data
     *
     * \return The amount of bytes read.
     */
    /*----------------------------------------------------------*/
    uint16_t Read( uint8_t *pData, uint32_t bufferLen );



    /*----------------------------------------------------------*/
    /*! \brief Clearing RX and TX queues.
     * 
     * \return nothing
     */
    /*----------------------------------------------------------*/
    void ClearQueues();



    /*----------------------------------------------------------*/
    /*! \brief send a control message via USB.
     *
     * \param wLen - length of message in bytes
     * \param pData - message data
     *
     * \return True if no error.
     */
    /*----------------------------------------------------------*/
    bool Write( uint16_t wLen, uint8_t *pData );
    
};

class CIndustrialStackLldCB
{
public:
    virtual void OnReadThreadEnd(CIndustrialStackLld *lld) = 0;
};

#endif // INDUSTRIAL_STACK_LLD_H
