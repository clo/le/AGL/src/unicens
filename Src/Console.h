/*
 * Video On Demand Samples
 *
 * Copyright (C) 2015 Microchip Technology Germany II GmbH & Co. KG
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * You may also obtain this software under a propriety license from Microchip.
 * Please contact Microchip for further information.
 *
 */

/*----------------------------------------------------------*/
/*! \file
 *  \brief This file contains C-functions starting with "Console" to provide
 *         process and thread safe access to the console output.
 */
/*----------------------------------------------------------*/
#ifndef _CONSOLE_H_
#define _CONSOLE_H_

#define RESETCOLOR "\033[0m"
#define GREEN      "\033[0;32m"
#define RED        "\033[0;31m"
#define YELLOW     "\033[1;33m"
#define BLUE       "\033[0;34m"

#include <stdbool.h>

#ifdef __cplusplus
extern "C"
{
#endif

    typedef enum
    {
        PRIO_LOW = 0,
        PRIO_MEDIUM = 1,
        PRIO_HIGH = 2,
        PRIO_ERROR = 0xFF
    } ConsolePrio_t;

    /*----------------------------------------------------------*/
    /*! \brief Initializes the resources needed to synchronize between processes and threads.
     *  \note This function must be called before any other function of this component.
     *  \param synchronizeProcesses - If set to true, multiple processes using this component will synchronize their output.
     *
     */
    /*----------------------------------------------------------*/
    void ConsoleInit( bool synchronizeProcesses );

    /*----------------------------------------------------------*/
    /*! \brief Destroys the resources needed to synchronize between processes and threads.
     *  \note After this function, any other function (except ConsoleInit) must not be called.
     *
     */
    /*----------------------------------------------------------*/
    void ConsoleDeinit( void );


    /*----------------------------------------------------------*/
    /*! \brief Sets the minimum priority to be displayed. Lower priority messages are discarded
     *  \param prio - The minimum priority to display
     */
    /*----------------------------------------------------------*/
    void ConsoleSetPrio( ConsolePrio_t prio );

    /*----------------------------------------------------------*/
    /*! \brief Uses the board specific PRINT mechanism and provides thread and process safety.
     *
     */
    /*----------------------------------------------------------*/
    void ConsolePrintf( ConsolePrio_t prio, const char *statement, ... );


    /*----------------------------------------------------------*/
    /*! \brief Starts to print and stay blocked after exit of this function
     *
     */
    /*----------------------------------------------------------*/
    void ConsolePrintfStart( ConsolePrio_t prio, const char *statement, ... );

    /*----------------------------------------------------------*/
    /*! \brief Continue to print and stay blocked after exit of this function
     *  \note ConsolePrintfStart must be called before and when finished ConsolePrintfExit must be called.
     *  \note This function may be called multiple times.
     */
    /*----------------------------------------------------------*/
    void ConsolePrintfContinue( const char *statement, ... );

    /*----------------------------------------------------------*/
    /*! \brief Continue to print and unblock after finishing.
     *  \note ConsolePrintfStart must be called before. ConsolePrintfContinue may have been called before multiple times.
     */
    /*----------------------------------------------------------*/
    void ConsolePrintfExit( const char *statement, ... );

#ifdef __cplusplus
}
#endif

#endif //_CONSOLE_H_
