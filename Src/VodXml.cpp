/*
 * Video On Demand Samples
 *
 * Copyright (C) 2015 Microchip Technology Germany II GmbH & Co. KG
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * You may also obtain this software under a propriety license from Microchip.
 * Please contact Microchip for further information.
 *
 */

#include "SafeVector.h"
#include "Console.h"
#include "VodXml.h"

#define TAG_ADDRESS             ((xmlChar *)"channel_addr")
#define TAG_ADDRESS_HEX         ((xmlChar *)"channel_addr_hex")
#define TAG_ASYNC_BANDWIDTH     ((xmlChar *)"async_bandwidth")
#define TAG_BLOCKWIDTH          ((xmlChar *)"blockwidth")
#define TAG_OFFSET              ((xmlChar *)"offset")
#define TAG_BUFFER_SIZE         ((xmlChar *)"buffer_size")
#define TAG_CHANNEL             ((xmlChar *)"channel")
#define TAG_CHANNEL_ID          ((xmlChar *)"channel_id")
#define TAG_DATA_TYPE           ((xmlChar *)"data_type")
#define TAG_AIM_TYPE            ((xmlChar *)"aim_type")
#define TAG_DEVICE              ((xmlChar *)"device")
#define TAG_DEVICE_API_VERSION  ((xmlChar *)"device_api_ver")
#define TAG_DEVICE_MLB_SPEED    ((xmlChar *)"open_mlb_port_speed")
#define TAG_DEVICE_TYPE         ((xmlChar *)"device_type")
#define TAG_DEVICE_TYPE_HEX     ((xmlChar *)"device_type_hex")
#define TAG_DIR                 ((xmlChar *)"dir")
#define TAG_ENDPOINT            ((xmlChar *)"endpoint")
#define TAG_TSI_PORT_ID         ((xmlChar *)"tsi_port_id")
#define TAG_TSI_PORT_MODE       ((xmlChar *)"tsi_port_mode")
#define TAG_I2S_V1_CLKDRIVEMODE ((xmlChar *)"i2s_v1_clock_drive_mode")
#define TAG_I2S_V1_OPTION       ((xmlChar *)"i2s_v1_option")
#define TAG_I2S_V1_DATAFORMAT   ((xmlChar *)"i2s_v1_data_format")
#define TAG_I2S_V1_PIN          ((xmlChar *)"i2s_v1_pin")
#define TAG_I2S_OPTION          ((xmlChar *)"i2s_option")
#define TAG_I2S_CLOCK_DIR       ((xmlChar *)"i2s_clock_dir")
#define TAG_I2S_CLOCK_DELAY     ((xmlChar *)"i2s_clock_delay")
#define TAG_I2S_FS_SPEED        ((xmlChar *)"i2s_fs_speed")
#define TAG_I2S_ALIGN           ((xmlChar *)"i2s_align")
#define TAG_I2S_PIN             ((xmlChar *)"i2s_pin")
#define TAG_INSTANCE            ((xmlChar *)"instance")
#define TAG_LOCAL_CONNECTION    ((xmlChar *)"local_connection")
#define TAG_NETWORK_CONFIG      ((xmlChar *)"network_config")
#define TAG_NETWORK_MANAGER     ((xmlChar *)"network_manager")
#define TAG_NUMBER_OF_BUFFERS   ((xmlChar *)"number_of_buffers")
#define TAG_PACKETS_XACT        ((xmlChar *)"packets_per_xact")
#define TAG_PACKETS_XACT_HEX    ((xmlChar *)"packets_per_xact_hex")
#define TAG_PORT_ID             ((xmlChar *)"port_id")
#define TAG_ROUTE               ((xmlChar *)"route")
#define TAG_SCRIPT              ((xmlChar *)"script")
#define TAG_SOCKET              ((xmlChar *)"socket")
#define TAG_SUBBUFFER_SIZE      ((xmlChar *)"subbuffer_size")
#define TAG_TERMINAL            ((xmlChar *)"terminal")
#define TAG_TIMING_MASTER       ((xmlChar *)"timing_master")

#define VAL_AIM_TYPE_CDEV       ((char*)"CDEV")
#define VAL_AIM_TYPE_AUDIO      ((char*)"AUDIO")
#define VAL_AIM_TYPE_V4L        ((char*)"V4L")

#define VAL_PORT_MLB			((char*)"MLB")
#define VAL_PORT_TSI			((char*)"TSI")
#define VAL_PORT_I2S			((char*)"I2S")
#define VAL_PORT_I2C			((char*)"I2C")
#define VAL_PORT_USB			((char*)"USB")
#define VAL_PORT_PCIE			((char*)"PCIE")
#define VAL_PORT_MOST			((char*)"MOST")

#define VAL_DATATYPE_SYNC		((char*)"SYNC")
#define VAL_DATATYPE_ISOC		((char*)"ISOC")
#define VAL_DATATYPE_ASYNC		((char*)"ASYNC")
#define VAL_DATATYPE_CTRL		((char*)"CTRL")

#define VAL_DIR_IN				((char*)"IN")
#define VAL_DIR_OUT				((char*)"OUT")

#define VAL_TSI_V1_PORT_0       ((char*)"TSI_PORT0")
#define VAL_TSI_V1_PORT_1       ((char*)"TSI_PORT1")

#define VAL_TSI_V1_SLAVE        ((char*)"TSI_SLAVE")
#define VAL_TSI_V1_MASTER       ((char*)"TSI_MASTER")

#define VAL_I2S_V1_CLK_OUTPUT   ((char*)"Output")
#define VAL_I2S_V1_CLK_INPUT    ((char*)"Input")
#define VAL_I2S_V1_CLK_ISOCOUT  ((char*)"IsocOutput")
#define VAL_I2S_V1_CLK_ISOCIN   ((char*)"IsocInput")

#define VAL_I2S_V1_OPTION_INOUT    ((char*)"InOut")
#define VAL_I2S_V1_OPTION_DUALIN   ((char*)"DualIn")
#define VAL_I2S_V1_OPTION_DUALOUT  ((char*)"DualOut")
#define VAL_I2S_V1_OPTION_FULL     ((char*)"FullStream")
#define VAL_I2S_V1_OPTION_WILDCARD ((char*)"Wildcard")

#define VAL_I2S_V1_FMT_D64Fs16  ((char*)"Delay64Fs16Bit")
#define VAL_I2S_V1_FMT_D64Fs24  ((char*)"Delay64Fs24Bit")
#define VAL_I2S_V1_FMT_D64FsSeq ((char*)"Delay64FsSeq")
#define VAL_I2S_V1_FMT_D128FsSeq ((char*)"Delay128FsSeq")
#define VAL_I2S_V1_FMT_D256FsSeq ((char*)"Delay256FsSeq")
#define VAL_I2S_V1_FMT_R64Fs16  ((char*)"Right64Fs16Bit")
#define VAL_I2S_V1_FMT_R64Fs24  ((char*)"Right64Fs24Bit")
#define VAL_I2S_V1_FMT_Seq64Fs  ((char*)"Seq64Fs")
#define VAL_I2S_V1_FMT_Seq128Fs ((char*)"Seq128Fs")
#define VAL_I2S_V1_FMT_Seq256Fs ((char*)"Seq256Fs")
#define VAL_I2S_V1_FMT_L64Fs16  ((char*)"Left64Fs16Bit")
#define VAL_I2S_V1_FMT_L64Fs24  ((char*)"Left64Fs24Bit")
#define VAL_I2S_V1_FMT_D512FsSeq ((char*)"Delay512FsSeq")
#define VAL_I2S_V1_FMT_Seq512Fs ((char*)"Seq512Fs")

#define VAL_I2S_V1_PIN_SR0      ((char*)"SR0Pin")
#define VAL_I2S_V1_PIN_SX0      ((char*)"SX0Pin")
#define VAL_I2S_V1_PIN_SR1      ((char*)"SR1Pin")
#define VAL_I2S_V1_PIN_SX1      ((char*)"SX1Pin")
#define VAL_I2S_V1_PIN_SR2      ((char*)"SR2Pin")
#define VAL_I2S_V1_PIN_SX2      ((char*)"SX2Pin")
#define VAL_I2S_V1_PIN_SR3      ((char*)"SR3Pin")
#define VAL_I2S_V1_PIN_SX3      ((char*)"SX3Pin")
#define VAL_I2S_V1_PIN_FSY_SCK  ((char*)"FSY_SCK_Pins")

#define VAL_I2S_OPTION_INOUT	((char*)"InOut")
#define VAL_I2S_OPTION_DUALIN	((char*)"DualIn")
#define VAL_I2S_OPTION_DUALOUT	((char*)"DualOut")
#define VAL_I2S_OPTION_WILDCARD	((char*)"Wildcard")

#define VAL_I2S_CLOCK_OUTPUT	((char*)"Output")
#define VAL_I2S_CLOCK_INPUT		((char*)"Input")
#define VAL_I2S_CLOCK_WILDCARD	((char*)"Wildcard")

#define VAL_I2S_ALIGNMENT_L16	((char*)"Left16")
#define VAL_I2S_ALIGNMENT_L24	((char*)"Left24")
#define VAL_I2S_ALIGNMENT_R16	((char*)"Right16")
#define VAL_I2S_ALIGNMENT_R24	((char*)"Right24")
#define VAL_I2S_SEQUENTIAL		((char*)"Seq")

#define VAL_I2S_PIN_SRXA0		((char*)"SRXA0")
#define VAL_I2S_PIN_SRXA1		((char*)"SRXA1")
#define VAL_I2S_PIN_SRXB0		((char*)"SRXB0")
#define VAL_I2S_PIN_SRXB1		((char*)"SRXB1")

#define UNKNOWN_ENDPOINT_ADDR	0xFF

#define XML_STRCMP(x,y) 		(strcmp(reinterpret_cast< const char* >(x), reinterpret_cast< const char* >(y)) == 0)

CVodXml::CVodXml( const char *szFile ) : CXml( szFile )
{

}

bool CVodXml::GetLocalInicConfigurations( uint8_t *rxEndpoint, uint8_t *txEndpoint )
{
    if( NULL == rxEndpoint || NULL == txEndpoint )
        return false;
    *rxEndpoint = UNKNOWN_ENDPOINT_ADDR;
    *txEndpoint = UNKNOWN_ENDPOINT_ADDR;
    CXml rootXml( *( ( CXml * )this ) );
    bool success = rootXml.SetToTopNode();
    if( success && rootXml.FindNode( TAG_NETWORK_MANAGER ) )
    {
        CXml xmlNWM( rootXml );
        success = xmlNWM.FindFirstChildNode();
        while( success && xmlNWM.FindNode( TAG_ENDPOINT ) )
        {
            ConsolePrintf( PRIO_LOW, "NETWORK_MANAGER\r\n" );
            xmlChar interface[XML_DEFAULT_VALUE_LEN];
            if( xmlNWM.GetChildValue( TAG_PORT_ID, interface ) )
            {
                if( XML_STRCMP(interface, VAL_PORT_I2C) )
                {
                    ConsolePrintf( PRIO_LOW,
                        "GetLocalInicConfigurations found I2C, returning without channel addresses\r\n" );
                    return true;
                }
            }
            xmlChar dataType[XML_DEFAULT_VALUE_LEN];
            xmlChar direction[XML_DEFAULT_VALUE_LEN];
            dataType[0] = '\0';
            direction[0] = '\0';
            int epAddress;
            success &= xmlNWM.GetChildValue( TAG_DATA_TYPE, dataType );
            ConsolePrintf( PRIO_LOW, "	Type:%s\r\n", dataType );
            success &= xmlNWM.GetChildValue( TAG_DIR, direction );
            ConsolePrintf( PRIO_LOW, "	Direction:%s\r\n", direction );
            if( !xmlNWM.GetChildValueInt( TAG_ADDRESS, epAddress ) )
            {
                xmlChar szTmp[XML_DEFAULT_VALUE_LEN];
                if( xmlNWM.GetChildValue( TAG_ADDRESS_HEX, szTmp, XML_DEFAULT_VALUE_LEN ) )
                    epAddress = ConvertToInt( szTmp );
            }
            ConsolePrintf( PRIO_LOW, "	Address:%d\r\n", epAddress );

            if( success )
            {
                if( XML_STRCMP(dataType, VAL_DATATYPE_CTRL) )
                {
                    if( XML_STRCMP(direction, VAL_DIR_IN) )
                    {
                        *rxEndpoint = epAddress;
                    }
                    else if( XML_STRCMP(direction, VAL_DIR_OUT) )
                    {
                        *txEndpoint = epAddress;
                    }
                }
            }
            else
            {
                ConsolePrintf( PRIO_ERROR, "Failed to retrieve Endpoint config\n" );
            }
            xmlNWM.FindNextNode();
        }
    }
    return success && ( 0xFF != *rxEndpoint ) && ( 0xFF != *txEndpoint );
}

bool CVodXml::GetMostParameters(bool *isTimingMaster, uint32_t *asyncBandwidth)
{
    if( NULL == isTimingMaster || NULL == asyncBandwidth )
    {
        ConsolePrintf( PRIO_ERROR, RED"GetMostParameters was"\
            " called with wrong parameters"RESETCOLOR"\n" );
        return false;
    }
    uint8_t serverNum = 0;
    CXml xmlNWM( *( ( CXml * )this ) );
    bool success = xmlNWM.SetToTopNode();
    if( success && xmlNWM.FindNode( TAG_NETWORK_MANAGER ) )
    {
        success = xmlNWM.FindFirstChildNode();
        while( success && xmlNWM.FindNode( TAG_LOCAL_CONNECTION ) )
        {
            serverNum +=  1;
            xmlNWM.FindNextNode();
        }
    }
    //Be compatible to old XML API
    if( !success || 0 == serverNum )
    {
        success = GetNetworkConfiguration( isTimingMaster, asyncBandwidth );
        return success;
    }

    if( success )
        success = xmlNWM.SetToTopNode();
    if( success )
        success = xmlNWM.FindNode( TAG_NETWORK_MANAGER );
    if( success )
        success = xmlNWM.FindFirstChildNode();

    if( success && xmlNWM.FindNode( TAG_LOCAL_CONNECTION ) )
    {
        CXml xmlLC( xmlNWM );
        int bw;
        int tm = true;
        success &= xmlNWM.GetChildValueInt( TAG_TIMING_MASTER, tm );
        if (success)
        {
            ConsolePrintf( PRIO_LOW, " Timing Master:%d\r\n", tm );
            success &= xmlNWM.GetChildValueInt( TAG_ASYNC_BANDWIDTH, bw );
            if (success)
            {
                *isTimingMaster = tm;
                *asyncBandwidth = bw;
                ConsolePrintf( PRIO_LOW, "Asynchronous Bandwidth:%d\r\n", bw );
            }
        }
    }
    return success;
}

bool CVodXml::GetLocalInicConfigurations( uint8_t *rxEndpoint[], uint8_t *txEndpoint[], uint32_t *apiVersion[],
    SocketPort_t *hwPort[], bool *isTimingMaster[], uint32_t *asyncBandwidth[], uint8_t *serverNum )
{
    if( NULL == rxEndpoint || NULL == txEndpoint || NULL == serverNum ||
        NULL == apiVersion || NULL == hwPort || NULL == isTimingMaster ||
        NULL == asyncBandwidth )
    {
        ConsolePrintf( PRIO_ERROR, RED"GetLocalInicConfigurations was"\
            " called with wrong parameters"RESETCOLOR"\n" );
        return false;
    }
    *rxEndpoint = NULL;
    *txEndpoint = NULL;
    *apiVersion = NULL;
    *hwPort = NULL;
    *isTimingMaster = NULL;
    *asyncBandwidth = NULL;
    *serverNum = 0;
    CXml xmlNWM( *( ( CXml * )this ) );
    bool success = xmlNWM.SetToTopNode();
    if( success && xmlNWM.FindNode( TAG_NETWORK_MANAGER ) )
    {
        success = xmlNWM.FindFirstChildNode();
        while( success && xmlNWM.FindNode( TAG_LOCAL_CONNECTION ) )
        {
            *serverNum = *serverNum + 1;
            xmlNWM.FindNextNode();
        }
    }
    //Be compatible to old XML API
    if( !success || 0 == *serverNum )
    {
        uint8_t rxEP;
        uint8_t txEP;
        bool tm;
        uint32_t bw;
        success = GetLocalInicConfigurations( &rxEP, &txEP );
        if( success )
            success = GetNetworkConfiguration( &tm, &bw );

        if( success )
        {
            *rxEndpoint = ( uint8_t * )calloc( 1, sizeof( uint8_t ) );
            *txEndpoint = ( uint8_t * )calloc( 1, sizeof( uint8_t ) );
            *apiVersion = ( uint32_t * )calloc( 1, sizeof( uint32_t ) );
            *hwPort = ( SocketPort_t * )calloc( 1, sizeof( SocketPort_t ) );
            *isTimingMaster = ( bool * )calloc( 1, sizeof( bool ) );
            *asyncBandwidth = ( uint32_t * )calloc( 1, sizeof( uint32_t ) );
            ( *rxEndpoint )[0] = rxEP;
            ( *txEndpoint )[0] = txEP;
            ( *apiVersion )[0] = GetServerDeviceApi();
            ( *hwPort )[0] = GetServerHardwareInterface();
            ( *isTimingMaster )[0] = tm;
            ( *asyncBandwidth )[0] = bw;
            *serverNum = 1;
        }
        return success;
    }
    //New method to get multiple instances of local configurations
    *rxEndpoint = ( uint8_t * )calloc( *serverNum, sizeof( uint8_t ) );
    *txEndpoint = ( uint8_t * )calloc( *serverNum, sizeof( uint8_t ) );
    *apiVersion = ( uint32_t * )calloc( *serverNum, sizeof( uint32_t ) );
    *hwPort = ( SocketPort_t * )calloc( *serverNum, sizeof( SocketPort_t ) );
    *isTimingMaster = ( bool * )calloc( *serverNum, sizeof( bool ) );
    *asyncBandwidth = ( uint32_t * )calloc( *serverNum, sizeof( uint32_t ) );

    if( success )
        success = xmlNWM.SetToTopNode();
    if( success )
        success = xmlNWM.FindNode( TAG_NETWORK_MANAGER );
    if( success )
        success = xmlNWM.FindFirstChildNode();

    uint8_t currentCon = 0;
    while( success && xmlNWM.FindNode( TAG_LOCAL_CONNECTION ) && currentCon < *serverNum )
    {
        int apiVer = -1;
        CXml xmlLC( xmlNWM );
        if( xmlLC.GetChildValueInt( TAG_DEVICE_API_VERSION, apiVer ) )
        {
            ( *apiVersion )[currentCon] = ( uint32_t )apiVer;
            ConsolePrintf( PRIO_LOW, "[Con:%d] API Version: %d\r\n", currentCon, apiVer );
        }
        ( *hwPort )[currentCon] = PORT_UNKNOWN;
        xmlChar interface[XML_DEFAULT_VALUE_LEN];
        if( xmlLC.GetChildValue( TAG_PORT_ID, interface ) )
        {
            ConsolePrintf( PRIO_LOW, "[Con:%d] HW Type: %s\r\n", currentCon, interface );

            if( XML_STRCMP(interface, VAL_PORT_MLB) )
                ( *hwPort )[currentCon] = PORT_MLB;
            else if( XML_STRCMP(interface, VAL_PORT_TSI) )
                ( *hwPort )[currentCon] = PORT_TSI;        
            else if( XML_STRCMP(interface, VAL_PORT_USB) )
                ( *hwPort )[currentCon] = PORT_USB;
            else if( XML_STRCMP(interface, VAL_PORT_I2C) )
                ( *hwPort )[currentCon] = PORT_I2C;        
        }
        int isMaster = true;
        int bw = 40;
        success &= xmlNWM.GetChildValueInt( TAG_TIMING_MASTER, isMaster );
        ConsolePrintf( PRIO_LOW, "[Con:%d]	Timing Master:%d\r\n", currentCon, isMaster );
        success &= xmlNWM.GetChildValueInt( TAG_ASYNC_BANDWIDTH, bw );
        ConsolePrintf( PRIO_LOW, "[Con:%d]	Asynchronous Bandwidth:%d\r\n", currentCon, bw );
        ( *isTimingMaster )[currentCon] = isMaster;
        ( *asyncBandwidth )[currentCon] = bw;
        success = xmlLC.FindFirstChildNode();
        while( success && xmlLC.FindNode( TAG_ENDPOINT ) )
        {
            xmlChar dataType[XML_DEFAULT_VALUE_LEN];
            xmlChar direction[XML_DEFAULT_VALUE_LEN];
            dataType[0] = '\0';
            direction[0] = '\0';
            int epAddress;
            success &= xmlLC.GetChildValue( TAG_DATA_TYPE, dataType );
            ConsolePrintf( PRIO_LOW, "[Con:%d]	Type:%s\r\n", currentCon, dataType );
            success &= xmlLC.GetChildValue( TAG_DIR, direction );
            ConsolePrintf( PRIO_LOW, "[Con:%d]	Direction:%s\r\n", currentCon, direction );
            if( !xmlLC.GetChildValueInt( TAG_ADDRESS, epAddress ) )
            {
                xmlChar szTmp[XML_DEFAULT_VALUE_LEN];
                if( xmlLC.GetChildValue( TAG_ADDRESS_HEX, szTmp, XML_DEFAULT_VALUE_LEN ) )
                    epAddress = ConvertToInt( szTmp );
            }
            ConsolePrintf( PRIO_LOW, "[Con:%d]	Address:%d\r\n", currentCon, epAddress );

            if( success )
            {
                if( XML_STRCMP(dataType, VAL_DATATYPE_CTRL) )
                {
                    if( XML_STRCMP(direction, VAL_DIR_IN) )
                    {
                        ( *rxEndpoint )[currentCon] = epAddress;
                    }
                    else if( XML_STRCMP(direction, VAL_DIR_OUT) )
                    {
                        ( *txEndpoint )[currentCon] = epAddress;
                    }
                }
            }
            else
            {
                ConsolePrintf( PRIO_ERROR, "Failed to retrieve Endpoint config\n" );
            }
            xmlLC.FindNextNode();
        }
        currentCon++;
        xmlNWM.FindNextNode();
    }
    return success;
}

SocketPort_t CVodXml::GetServerHardwareInterface()
{
    //Be compatible to old configurations, which had no interface declaration
    SocketPort_t hwInterface = PORT_USB;
    CXml rootXml( *( ( CXml * )this ) );
    bool success = rootXml.SetToTopNode();
    if( success && rootXml.FindNode( TAG_NETWORK_MANAGER ) )
    {
        CXml xmlNWM( rootXml );
        success = xmlNWM.FindFirstChildNode();
        if( success && xmlNWM.FindNode( TAG_ENDPOINT ) )
        {
            xmlChar interface[XML_DEFAULT_VALUE_LEN];

            if( xmlNWM.GetChildValue( TAG_PORT_ID, interface ) )
            {
                ConsolePrintf( PRIO_LOW, "        - Port ID   : %s\r\n", interface );
                if( XML_STRCMP(interface, VAL_PORT_MLB) )
                    hwInterface = PORT_MLB;
                else if( XML_STRCMP(interface, VAL_PORT_USB) )
                    hwInterface = PORT_USB;
                else if( XML_STRCMP(interface, VAL_PORT_I2C) )
                    hwInterface = PORT_I2C;
            }
        }
    }
    return hwInterface;
}

uint32_t CVodXml::GetServerDeviceApi()
{
    //Be compatible to old configurations, which had no API declaration
    uint32_t deviceApi = 2;
    CXml rootXml( *( ( CXml * )this ) );
    bool success = rootXml.SetToTopNode();
    if( success && rootXml.FindNode( TAG_NETWORK_MANAGER ) )
    {
        CXml xmlNWM( rootXml );
        success = xmlNWM.FindFirstChildNode();
        if( success && xmlNWM.FindNode( TAG_ENDPOINT ) )
        {
            ConsolePrintf( PRIO_LOW, "NETWORK_MANAGER\r\n" );
            int apiVer;
            if( xmlNWM.GetChildValueInt( TAG_DEVICE_API_VERSION, apiVer ) )
            {
                deviceApi = apiVer;
                ConsolePrintf( PRIO_LOW, "        - Device API Version: %d\r\n", apiVer );
            }
        }
    }
    return deviceApi;
}

uint32_t CVodXml::GetDeviceApi( uint32_t deviceType )
{
    uint32_t deviceApi = 0xFFFFFFFF;
    bool success = true;
    CXml Xml( *( ( CXml * )this ) );
    success = Xml.SetToTopNode();
    success &= Xml.FindNode( TAG_NETWORK_MANAGER );
    success &= Xml.FindFirstChildNode();

    while( success && Xml.FindNode( TAG_DEVICE ) )
    {
        int xmlDeviceType = -1;
        int xmlDeviceApiVer = -1;
        if( !Xml.GetChildValueInt( TAG_DEVICE_TYPE, xmlDeviceType ) )
        {
            xmlChar szTmp[XML_DEFAULT_VALUE_LEN];
            if( Xml.GetChildValue( TAG_DEVICE_TYPE_HEX, szTmp, XML_DEFAULT_VALUE_LEN ) )
                xmlDeviceType = ConvertToInt( szTmp );
        }
        if( ( uint32_t )xmlDeviceType == deviceType )
        {
            success &= Xml.GetChildValueInt( TAG_DEVICE_API_VERSION, xmlDeviceApiVer );
            if( success )
            {
                ConsolePrintf( PRIO_LOW, "Device API Version %d for device type: 0x%X\r\n", xmlDeviceApiVer,
                    xmlDeviceType );
                deviceApi = xmlDeviceApiVer;
                break;
            }
        }
        Xml.FindNextNode();
    }
    return deviceApi;
}

MlbPortSpeed_t CVodXml::GetDeviceMlbPortSpeed( uint32_t deviceType )
{
    MlbPortSpeed_t mlbSpeed = MlbSpeedNotSet;
    bool success = true;
    CXml Xml( *( ( CXml * )this ) );
    success = Xml.SetToTopNode();
    success &= Xml.FindNode( TAG_NETWORK_MANAGER );
    success &= Xml.FindFirstChildNode();

    while( success && Xml.FindNode( TAG_DEVICE ) )
    {
        int xmlDeviceType;
        int xmlDeviceMlbPortSpeed = -1;
        if( !Xml.GetChildValueInt( TAG_DEVICE_TYPE, xmlDeviceType ) )
        {
            xmlChar szTmp[XML_DEFAULT_VALUE_LEN];
            if( Xml.GetChildValue( TAG_DEVICE_TYPE_HEX, szTmp, XML_DEFAULT_VALUE_LEN ) )
                xmlDeviceType = ConvertToInt( szTmp );
        }
        ConsolePrintf( PRIO_LOW, "DeviceType: %d\r\n", xmlDeviceType );
        if( ( uint32_t )xmlDeviceType == deviceType )
        {
            success &= Xml.GetChildValueInt( TAG_DEVICE_MLB_SPEED, xmlDeviceMlbPortSpeed );
            if( success )
            {
                ConsolePrintf( PRIO_LOW, "Device MLB speed %d for device type: 0x%X\r\n", xmlDeviceMlbPortSpeed,
                    xmlDeviceType );
                switch( xmlDeviceMlbPortSpeed )
                {
                case 256:
                    mlbSpeed = MlbSpeed256FS;
                    break;
                case 512:
                    mlbSpeed = MlbSpeed512FS;
                    break;
                case 1024:
                    mlbSpeed = MlbSpeed1024FS;
                    break;
                case 2048:
                    mlbSpeed = MlbSpeed2048FS;
                    break;
                case 3072:
                    mlbSpeed = MlbSpeed3072FS;
                    break;
                case 4096:
                    mlbSpeed = MlbSpeed4096FS;
                    break;
                case 6144:
                    mlbSpeed = MlbSpeed6144FS;
                    break;
                case 8192:
                    mlbSpeed = MlbSpeed8192FS;
                    break;
                default:
                    mlbSpeed = MlbSpeedNotSet;
                    break;
                }
                break;
            }
        }
        Xml.FindNextNode();
    }
    return mlbSpeed;
}

bool CVodXml::GetNetworkConfiguration( bool *isTimingMaster, uint32_t *asyncBandwidth )
{
    if( NULL == isTimingMaster || NULL == asyncBandwidth )
        return false;
    *isTimingMaster = true;
    *asyncBandwidth = 40;
    CXml rootXml( *( ( CXml * )this ) );
    bool success = rootXml.SetToTopNode();
    if( success && rootXml.FindNode( TAG_NETWORK_MANAGER ) )
    {
        CXml xmlNWM( rootXml );
        success = xmlNWM.FindFirstChildNode();
        while( success && xmlNWM.FindNode( TAG_NETWORK_CONFIG ) )
        {
            ConsolePrintf( PRIO_LOW, "NETWORK_MANAGER\r\n" );
            int isMaster;
            int bw;
            success &= xmlNWM.GetChildValueInt( TAG_TIMING_MASTER, isMaster );
            ConsolePrintf( PRIO_LOW, "	Is Timing Master:%d\r\n", isMaster );
            success &= xmlNWM.GetChildValueInt( TAG_ASYNC_BANDWIDTH, bw );
            ConsolePrintf( PRIO_LOW, "	Asynchronous Bandwidth:%d\r\n", bw );
            if( success )
            {
                *isTimingMaster = isMaster;
                *asyncBandwidth = bw;
                break;
            }
            else
            {
                ConsolePrintf( PRIO_ERROR, "Failed to retrieve Network config\n" );
            }
            xmlNWM.FindNextNode();
        }
    }
    return success;
}


bool CVodXml::GetChannelConfigurations( uint32_t deviceType, CSafeVector<CChannelConfiguration *> &allChannels )
{
    bool success = false;
    bool found = false;
    CXml Xml( *( ( CXml * )this ) );
    success = Xml.SetToTopNode();
    success &= Xml.FindNode( TAG_NETWORK_MANAGER );
    success &= Xml.FindFirstChildNode();

    while( !found && Xml.FindNode( TAG_DEVICE ) )
    {
        int xmlDeviceType = -1;
        char *externalScript = NULL;
        xmlChar szTmp[XML_DEFAULT_VALUE_LEN];
        if( !Xml.GetChildValueInt( TAG_DEVICE_TYPE, xmlDeviceType ) )
        {
            xmlChar szTmp[XML_DEFAULT_VALUE_LEN];
            if( Xml.GetChildValue( TAG_DEVICE_TYPE_HEX, szTmp, XML_DEFAULT_VALUE_LEN ) )
                xmlDeviceType = ConvertToInt( szTmp );
        }
        if( ( uint32_t )xmlDeviceType != deviceType )
        {
            Xml.FindNextNode();
            continue;
        }
        ConsolePrintf( PRIO_LOW, "DeviceType: %d (0x%X)\r\n", xmlDeviceType, xmlDeviceType );
        szTmp[0] = '\0';
        if( Xml.GetChildValue( TAG_SCRIPT, szTmp ) )
        {
            int scriptLen = xmlStrlen( szTmp );
            externalScript = ( char * )malloc( scriptLen + 1 );
            if( NULL != externalScript )
            {
                memcpy( externalScript, szTmp, scriptLen );
                externalScript[scriptLen] = '\0';
            }
            ConsolePrintf( PRIO_LOW, "External script:'%s'\r\n", externalScript );
        }
        found = true;
        CXml Channel( Xml );
        success = Channel.FindFirstChildNode();

        while( success && Channel.FindNode( TAG_CHANNEL ) )
        {
            int channelId;
            success &= Channel.GetChildValueInt( TAG_CHANNEL_ID, channelId );
            if( success )
            {
                ConsolePrintf( PRIO_LOW, "    Channel: %u\r\n", channelId );
                CChannelConfiguration *channelConfig = new CChannelConfiguration();
                allChannels.PushBack( channelConfig );
                channelConfig->deviceType = xmlDeviceType;
                channelConfig->channelId = channelId;
                channelConfig->externalScipt = externalScript;
                CXml Socket( Channel );
                success = Socket.FindFirstChildNode();
                while( Socket.FindNode( TAG_SOCKET ) )
                {
                    ConsolePrintf( PRIO_LOW, "        Socket:\r\n" );
                    int nTmp;
                    CSocketConfiguration *sockConfig = NULL;
                    szTmp[0] = '\0';
                    if( Socket.GetChildValue( TAG_DIR, szTmp ) )
                    {
                        ConsolePrintf( PRIO_LOW, "        - Direction : %s\r\n", szTmp );
                        if( XML_STRCMP(szTmp, VAL_DIR_IN) )
                            sockConfig = &channelConfig->inSocket;
                        else if( XML_STRCMP(szTmp, VAL_DIR_OUT) )
                            sockConfig = &channelConfig->outSocket;
                    }
                    if( NULL == sockConfig )
                    {
                        ConsolePrintf( PRIO_ERROR, RED"VodXml can not detect socket direction"RESETCOLOR"\n" );
                        return false; //unknown target socket
                    }

                    szTmp[0] = '\0';
                    if( Socket.GetChildValue( TAG_PORT_ID, szTmp ) )
                    {
                        ConsolePrintf( PRIO_LOW, "        - Port ID   : %s\r\n", szTmp );
                        if( XML_STRCMP(szTmp, VAL_PORT_MLB) )
                            sockConfig->port = PORT_MLB;
                        else if( XML_STRCMP(szTmp, VAL_PORT_TSI) )
                            sockConfig->port = PORT_TSI;
                        else if( XML_STRCMP(szTmp, VAL_PORT_I2S) )
                            sockConfig->port = PORT_I2S;
                        else if( XML_STRCMP(szTmp, VAL_PORT_USB) )
                            sockConfig->port = PORT_USB;
                        else if( XML_STRCMP(szTmp, VAL_PORT_PCIE) )
                            sockConfig->port = PORT_PCIE;
                        else if( XML_STRCMP(szTmp, VAL_PORT_MOST) )
                            sockConfig->port = PORT_MOST;
                        else
                            sockConfig->port = PORT_UNKNOWN;
                    }

                    szTmp[0] = '\0';
                    if( Socket.GetChildValue( TAG_DATA_TYPE, szTmp ) )
                    {
                        ConsolePrintf( PRIO_LOW, "        - Data Type   : %s\r\n", szTmp );
                        if( XML_STRCMP(szTmp, VAL_DATATYPE_SYNC) )
                            sockConfig->type = EP_Synchron;
                        else if( XML_STRCMP(szTmp, VAL_DATATYPE_ISOC) )
                            sockConfig->type = EP_Isochron;
                        else if( XML_STRCMP(szTmp, VAL_DATATYPE_ASYNC) )
                            sockConfig->type = EP_Asynchron;
                        else if( XML_STRCMP(szTmp, VAL_DATATYPE_CTRL) )
                            sockConfig->type = EP_Control;
                        else
                            sockConfig->type = EP_Unknown;
                    }

                    if( Socket.GetChildValueInt( TAG_OFFSET, nTmp ) )
                    {
                        ConsolePrintf( PRIO_LOW, "        - Splitted Offset: %d\r\n", nTmp );
                        sockConfig->splittedOffset = nTmp;
                    }

                    if( Socket.GetChildValueInt( TAG_BLOCKWIDTH, nTmp ) )
                    {
                        ConsolePrintf( PRIO_LOW, "        - Blockwidth: %d\r\n", nTmp );
                        sockConfig->blockWidth = nTmp;
                    }

                    if( Socket.GetChildValueInt( TAG_ADDRESS, nTmp ) )
                    {
                        ConsolePrintf( PRIO_LOW, "        - Channel Address: 0x%X\r\n", nTmp );
                        sockConfig->address = nTmp;
                    }
                    else if( Socket.GetChildValue( TAG_ADDRESS_HEX, szTmp, XML_DEFAULT_VALUE_LEN ) )
                    {
                        ConsolePrintf( PRIO_LOW, "        - Channel Address (hex): 0x%X\r\n", nTmp );
                        sockConfig->address = ConvertToInt( szTmp );
                    }

                    if( Socket.GetChildValueInt( TAG_NUMBER_OF_BUFFERS, nTmp ) )
                    {
                        ConsolePrintf( PRIO_LOW, "        - Number of Buffers: %d\r\n", nTmp );
                        sockConfig->amountOfBuffers = nTmp;
                    }

                    if( Socket.GetChildValueInt( TAG_BUFFER_SIZE, nTmp ) )
                    {
                        ConsolePrintf( PRIO_LOW, "        - Buffer Size: %d\r\n", nTmp );
                        sockConfig->bufferSize = nTmp;
                    }

                    if( Socket.GetChildValueInt( TAG_PACKETS_XACT, nTmp ) )
                    {
                        ConsolePrintf( PRIO_LOW, "        - Packets per Transaction: %d\r\n", nTmp );
                        sockConfig->packetsPerTransaction = nTmp;
                    }
                    else
                    {
                        if( Socket.GetChildValue( TAG_PACKETS_XACT_HEX, szTmp, XML_DEFAULT_VALUE_LEN ) )
                        {
                            sockConfig->packetsPerTransaction = ConvertToInt( szTmp );
                            ConsolePrintf( PRIO_LOW, "        - Packets per Transaction: 0x%X\r\n", sockConfig->packetsPerTransaction );
                        }
                    }

                    if( Socket.GetChildValueInt( TAG_SUBBUFFER_SIZE, nTmp ) )
                    {
                        ConsolePrintf( PRIO_LOW, "        - Subbuffer Size: %d\r\n", nTmp );
                        sockConfig->subbufferSize = nTmp;
                    }

                    if( Socket.GetChildValue( TAG_AIM_TYPE, szTmp ) )
                    {
                        ConsolePrintf( PRIO_LOW, "        - TAG_AIM_TYPE   : %s\r\n", szTmp );
                        if( XML_STRCMP(szTmp, VAL_AIM_TYPE_CDEV) )
                            sockConfig->aimType = AIM_CDEV;
                        else if( XML_STRCMP(szTmp, VAL_AIM_TYPE_AUDIO) )
                            sockConfig->aimType = AIM_AUDIO;
                        else if( XML_STRCMP(szTmp, VAL_AIM_TYPE_V4L) )
                            sockConfig->aimType = AIM_V4L;
                        else
                            sockConfig->aimType = AIM_UNKNOWN;
                    }

                    if( PORT_TSI == sockConfig->port )
                    {
                        if( Socket.GetChildValue( TAG_TSI_PORT_ID, szTmp ) )
                        {
                            ConsolePrintf( PRIO_LOW, "        - TSI Port ID   : %s\r\n", szTmp );
                            if( XML_STRCMP(szTmp, VAL_TSI_V1_PORT_0) )
                                sockConfig->tsiConfig.tsiPortInstance = V1TsiPortInstance0;
                            else if( XML_STRCMP(szTmp, VAL_TSI_V1_PORT_1) )
                                sockConfig->tsiConfig.tsiPortInstance = V1TsiPortInstance1;
                        }
                        if( Socket.GetChildValue( TAG_TSI_PORT_MODE, szTmp ) )
                        {
                            ConsolePrintf( PRIO_LOW, "        - TSI Port Mode   : %s\r\n", szTmp );
                            if( XML_STRCMP(szTmp, VAL_TSI_V1_SLAVE) )
                                sockConfig->tsiConfig.tsiPortMode = V1TsiPortModeSlave;
                            else if( XML_STRCMP(szTmp, VAL_TSI_V1_MASTER) )
                                sockConfig->tsiConfig.tsiPortMode = V1TsiPortModeMaster;
                        }
                    }
                    else if( PORT_I2S == sockConfig->port )
                    {
                        sockConfig->i2sConfig.portClkDriveModeV1 = V1I2sClockModeUnknown;
                        sockConfig->i2sConfig.portOptionV1 = V1I2sStreamingPortWildcard; //Be backward compatible
                        sockConfig->i2sConfig.streamingDataFormatV1 = V1I2sDataFormatUnknown;
                        sockConfig->i2sConfig.pinV1 = V1I2sInvalidPin;
                        sockConfig->i2sConfig.portOption = V3I2SOptionUnknown;
                        sockConfig->i2sConfig.clockMode = V3I2SClockModeUnknown;
                        sockConfig->i2sConfig.delayMode = V3I2SDelayUnknown;
                        sockConfig->i2sConfig.portSpeed = V3I2SSpeedUnknown;
                        sockConfig->i2sConfig.alignment = V3I2SAlignUnknown;
                        sockConfig->i2sConfig.pin = V3I2SInvalidPin;
                        szTmp[0] = '\0';
                        if( Socket.GetChildValue( TAG_I2S_V1_CLKDRIVEMODE, szTmp ) )
                        {
                            ConsolePrintf( PRIO_LOW, "        - I2S V1 clock drive mode   : %s\r\n", szTmp );
                            if( XML_STRCMP(szTmp, VAL_I2S_V1_CLK_OUTPUT) )
                                sockConfig->i2sConfig.portClkDriveModeV1 = V1I2sClockModeOutput;
                            else if( XML_STRCMP(szTmp, VAL_I2S_V1_CLK_INPUT) )
                                sockConfig->i2sConfig.portClkDriveModeV1 = V1I2sClockModeInput;
                            else if( XML_STRCMP(szTmp, VAL_I2S_V1_CLK_ISOCOUT) )
                                sockConfig->i2sConfig.portClkDriveModeV1 = V1I2sClockModeIsocOutput;
                            else if( XML_STRCMP(szTmp, VAL_I2S_V1_CLK_ISOCIN) )
                                sockConfig->i2sConfig.portClkDriveModeV1 = V1I2sClockModeIsocInput;
                        }

                        if( Socket.GetChildValue( TAG_I2S_V1_OPTION, szTmp ) )
                        {
                            ConsolePrintf( PRIO_LOW, "        - I2S V1 port option   : %s\r\n", szTmp );
                            if( XML_STRCMP(szTmp, VAL_I2S_V1_OPTION_INOUT ) )
                                sockConfig->i2sConfig.portOptionV1 = V1I2sStreamingPortInOut;
                            else if( XML_STRCMP(szTmp, VAL_I2S_V1_OPTION_DUALIN ) )
                                sockConfig->i2sConfig.portOptionV1 = V1I2sStreamingPortDualIn;
                            else if( XML_STRCMP(szTmp, VAL_I2S_V1_OPTION_DUALOUT ) )
                                sockConfig->i2sConfig.portOptionV1 = V1I2sStreamingPortDualOut;
                            else if( XML_STRCMP(szTmp, VAL_I2S_V1_OPTION_FULL ) )
                                sockConfig->i2sConfig.portOptionV1 = V1I2sStreamingPortFullStream;
                            else if( XML_STRCMP(szTmp, VAL_I2S_V1_OPTION_WILDCARD ) )
                                sockConfig->i2sConfig.portOptionV1 = V1I2sStreamingPortWildcard;
                        }

                        szTmp[0] = '\0';
                        if( Socket.GetChildValue( TAG_I2S_V1_DATAFORMAT, szTmp ) )
                        {
                            ConsolePrintf( PRIO_LOW, "        - I2S V1 data format   : %s\r\n", szTmp );
                            if( XML_STRCMP(szTmp, VAL_I2S_V1_FMT_D64Fs16) )
                                sockConfig->i2sConfig.streamingDataFormatV1 = V1I2sDelay64Fs16Bit;
                            else if( XML_STRCMP(szTmp, VAL_I2S_V1_FMT_D64Fs24) )
                                sockConfig->i2sConfig.streamingDataFormatV1 = V1I2sDelay64Fs24Bit;
                            else if( XML_STRCMP(szTmp, VAL_I2S_V1_FMT_D64FsSeq) )
                                sockConfig->i2sConfig.streamingDataFormatV1 = V1I2sDelay64FsSeq;
                            else if( XML_STRCMP(szTmp, VAL_I2S_V1_FMT_D128FsSeq) )
                                sockConfig->i2sConfig.streamingDataFormatV1 = V1I2sDelay128FsSeq;
                            else if( XML_STRCMP(szTmp, VAL_I2S_V1_FMT_D256FsSeq) )
                                sockConfig->i2sConfig.streamingDataFormatV1 = V1I2sDelay256FsSeq;
                            else if( XML_STRCMP(szTmp, VAL_I2S_V1_FMT_R64Fs16) )
                                sockConfig->i2sConfig.streamingDataFormatV1 = V1I2sRight64Fs16Bit;
                            else if( XML_STRCMP(szTmp, VAL_I2S_V1_FMT_R64Fs24) )
                                sockConfig->i2sConfig.streamingDataFormatV1 = V1I2sRight64Fs24Bit;
                            else if( XML_STRCMP(szTmp, VAL_I2S_V1_FMT_Seq64Fs) )
                                sockConfig->i2sConfig.streamingDataFormatV1 = V1I2sSeq64Fs;
                            else if( XML_STRCMP(szTmp, VAL_I2S_V1_FMT_Seq128Fs) )
                                sockConfig->i2sConfig.streamingDataFormatV1 = V1I2sSeq128Fs;
                            else if( XML_STRCMP(szTmp, VAL_I2S_V1_FMT_Seq256Fs) )
                                sockConfig->i2sConfig.streamingDataFormatV1 = V1I2sSeq256Fs;
                            else if( XML_STRCMP(szTmp, VAL_I2S_V1_FMT_L64Fs16) )
                                sockConfig->i2sConfig.streamingDataFormatV1 = V1I2sLeft64Fs16Bit;
                            else if( XML_STRCMP(szTmp, VAL_I2S_V1_FMT_L64Fs24) )
                                sockConfig->i2sConfig.streamingDataFormatV1 = V1I2sLeft64Fs24Bit;
                            else if( XML_STRCMP(szTmp, VAL_I2S_V1_FMT_D512FsSeq) )
                                sockConfig->i2sConfig.streamingDataFormatV1 = V1I2sDelay512FsSeq;
                            else if( XML_STRCMP(szTmp, VAL_I2S_V1_FMT_Seq512Fs) )
                                sockConfig->i2sConfig.streamingDataFormatV1 = V1I2sSeq512Fs;
                        }

                        szTmp[0] = '\0';
                        if( Socket.GetChildValue( TAG_I2S_V1_PIN, szTmp ) )
                        {
                            ConsolePrintf( PRIO_LOW, "        - I2S V1 pin   : %s\r\n", szTmp );
                            if( XML_STRCMP(szTmp, VAL_I2S_V1_PIN_SR0) )
                                sockConfig->i2sConfig.pinV1 = V1I2sSR0Pin;
                            else if( XML_STRCMP(szTmp, VAL_I2S_V1_PIN_SX0) )
                                sockConfig->i2sConfig.pinV1 = V1I2sSX0Pin;
                            else if( XML_STRCMP(szTmp, VAL_I2S_V1_PIN_SR1) )
                                sockConfig->i2sConfig.pinV1 = V1I2sSR1Pin;
                            else if( XML_STRCMP(szTmp, VAL_I2S_V1_PIN_SX1) )
                                sockConfig->i2sConfig.pinV1 = V1I2sSX1Pin;
                            else if( XML_STRCMP(szTmp, VAL_I2S_V1_PIN_SR2) )
                                sockConfig->i2sConfig.pinV1 = V1I2sSR2Pin;
                            else if( XML_STRCMP(szTmp, VAL_I2S_V1_PIN_SX2) )
                                sockConfig->i2sConfig.pinV1 = V1I2sSX2Pin;
                            else if( XML_STRCMP(szTmp, VAL_I2S_V1_PIN_SR3) )
                                sockConfig->i2sConfig.pinV1 = V1I2sSR3Pin;
                            else if( XML_STRCMP(szTmp, VAL_I2S_V1_PIN_SX3) )
                                sockConfig->i2sConfig.pinV1 = V1I2sSX3Pin;
                            else if( XML_STRCMP(szTmp, VAL_I2S_V1_PIN_FSY_SCK) )
                                sockConfig->i2sConfig.pinV1 = V1I2sFSYSCKPins;
                        }


                        szTmp[0] = '\0';
                        if( Socket.GetChildValue( TAG_I2S_OPTION, szTmp ) )
                        {
                            ConsolePrintf( PRIO_LOW, "        - I2S OPTION   : %s\r\n", szTmp );
                            if( XML_STRCMP(szTmp, VAL_I2S_OPTION_INOUT) )
                                sockConfig->i2sConfig.portOption = V3I2SOptionInOut;
                            else if( XML_STRCMP(szTmp, VAL_I2S_OPTION_DUALIN) )
                                sockConfig->i2sConfig.portOption = V3I2SOptionDualIn;
                            else if( XML_STRCMP(szTmp, VAL_I2S_OPTION_DUALOUT) )
                                sockConfig->i2sConfig.portOption = V3I2SOptionDualOut;
                        }

                        szTmp[0] = '\0';
                        if( Socket.GetChildValue( TAG_I2S_CLOCK_DIR, szTmp ) )
                        {
                            ConsolePrintf( PRIO_LOW, "        - I2S clock direction   : %s\r\n", szTmp );
                            if( XML_STRCMP(szTmp, VAL_I2S_CLOCK_OUTPUT) )
                                sockConfig->i2sConfig.clockMode = V3I2SClockModeOutput;
                            else if( XML_STRCMP(szTmp, VAL_I2S_CLOCK_INPUT) )
                                sockConfig->i2sConfig.clockMode = V3I2SClockModeInput;
                            else if( XML_STRCMP(szTmp, VAL_I2S_OPTION_WILDCARD) )
                                sockConfig->i2sConfig.clockMode = V3I2SClockModeWildcard;
                        }

                        if( Socket.GetChildValueInt( TAG_I2S_CLOCK_DELAY, nTmp ) )
                        {
                            ConsolePrintf( PRIO_LOW, "        - I2S clock delay: %d\r\n", nTmp );
                            if( 0 == nTmp )
                                sockConfig->i2sConfig.delayMode = V3I2SDelayOff;
                            else if( 1 == nTmp )
                                sockConfig->i2sConfig.delayMode = V3I2SDelayEnabled;
                            else if( 2 == nTmp )
                                sockConfig->i2sConfig.delayMode = V3I2SDelayWildcard;
                        }

                        if( Socket.GetChildValueInt( TAG_I2S_FS_SPEED, nTmp ) )
                        {
                            ConsolePrintf( PRIO_LOW, "        - I2S FS speed: %d\r\n", nTmp );
                            switch( nTmp )
                            {
                            case 64:
                                sockConfig->i2sConfig.portSpeed = V3I2SSpeed64FS;
                                break;
                            case 128:
                                sockConfig->i2sConfig.portSpeed = V3I2SSpeed128FS;
                                break;
                            case 256:
                                sockConfig->i2sConfig.portSpeed = V3I2SSpeed256FS;
                                break;
                            case 512:
                                sockConfig->i2sConfig.portSpeed = V3I2SSpeed512FS;
                                break;
                            case 0:
                                sockConfig->i2sConfig.portSpeed = V3I2SSpeedWildcard;
                                break;
                            }
                        }

                        szTmp[0] = '\0';
                        if( Socket.GetChildValue( TAG_I2S_ALIGN, szTmp ) )
                        {
                            ConsolePrintf( PRIO_LOW, "        - I2S alignment   : %s\r\n", szTmp );
                            if( XML_STRCMP(szTmp, VAL_I2S_ALIGNMENT_L16) )
                                sockConfig->i2sConfig.alignment = V3I2SAlignLeft16;
                            else if( XML_STRCMP(szTmp, VAL_I2S_ALIGNMENT_L24) )
                                sockConfig->i2sConfig.alignment = V3I2SAlignLeft24;
                            else if( XML_STRCMP(szTmp, VAL_I2S_ALIGNMENT_R16) )
                                sockConfig->i2sConfig.alignment = V3I2SAlignRight16;
                            else if( XML_STRCMP(szTmp, VAL_I2S_ALIGNMENT_R24) )
                                sockConfig->i2sConfig.alignment = V3I2SAlignRight24;
                            else if( XML_STRCMP(szTmp, VAL_I2S_SEQUENTIAL) )
                                sockConfig->i2sConfig.alignment = V3I2SAlignSeq;
                        }

                        szTmp[0] = '\0';
                        if( Socket.GetChildValue( TAG_I2S_PIN, szTmp ) )
                        {
                            ConsolePrintf( PRIO_LOW, "        - I2S Pin   : %s\r\n", szTmp );
                            if( XML_STRCMP(szTmp, VAL_I2S_PIN_SRXA0) )
                                sockConfig->i2sConfig.pin = V3I2SSRXA0;
                            else if( XML_STRCMP(szTmp, VAL_I2S_PIN_SRXA1) )
                                sockConfig->i2sConfig.pin = V3I2SSRXA1;
                            else if( XML_STRCMP(szTmp, VAL_I2S_PIN_SRXB0) )
                                sockConfig->i2sConfig.pin = V3I2SSRXB0;
                            else if( XML_STRCMP(szTmp, VAL_I2S_PIN_SRXB1) )
                                sockConfig->i2sConfig.pin = V3I2SSRXB1;
                        }
                    }

                    Socket.FindNextNode();
                }
            }
            Channel.FindNextNode();
        }
        Xml.FindNextNode();
    }
    return success && found;
}

bool CVodXml::GetRouteTerminals( CRouteTerminal *routeTerminal, CSafeVector<CRouteTerminal *> &allRouteTerminals )
{
    bool success = false;
    bool found = false;
    CXml Xml( *( ( CXml * )this ) );
    success = Xml.SetToTopNode();
    success &= Xml.FindNode( TAG_NETWORK_MANAGER );
    success &= Xml.FindFirstChildNode();

    while( success && Xml.FindNode( TAG_ROUTE ) )
    {
        CXml t1( Xml );
        t1.FindFirstChildNode();

        int deviceType = -1;
        int instance = -1;
        int channelId = -1;

        while( t1.FindNode( TAG_TERMINAL ) &&
            t1.GetChildValueInt( TAG_CHANNEL_ID, channelId ) &&
            t1.GetChildValueInt( TAG_INSTANCE, instance ) )
        {
            if( !t1.GetChildValueInt( TAG_DEVICE_TYPE, deviceType ) )
            {
                xmlChar szTmp[XML_DEFAULT_VALUE_LEN];
                if( t1.GetChildValue( TAG_DEVICE_TYPE_HEX, szTmp, XML_DEFAULT_VALUE_LEN ) )
                    deviceType = ConvertToInt( szTmp );
            }
            if( routeTerminal->deviceType != ( uint32_t )deviceType ||
                routeTerminal->channelId != ( uint32_t )channelId ||
                routeTerminal->instance != ( uint32_t )instance )
            {
                t1.FindNextNode();
                continue;
            }

            CXml t2( Xml );
            t2.FindFirstChildNode();

            while( t2.FindNode( TAG_TERMINAL ) )
            {
                deviceType = -1;
                if( !t2.GetChildValueInt( TAG_DEVICE_TYPE, deviceType ) )
                {
                    xmlChar szTmp[XML_DEFAULT_VALUE_LEN];
                    if( t2.GetChildValue( TAG_DEVICE_TYPE_HEX, szTmp, XML_DEFAULT_VALUE_LEN ) )
                        deviceType = ConvertToInt( szTmp );
                }
                if( !t2.GetChildValueInt( TAG_CHANNEL_ID, channelId ) ||
                    !t2.GetChildValueInt( TAG_INSTANCE, instance ) ||
                    ( routeTerminal->deviceType == ( uint32_t )deviceType &&
                    routeTerminal->channelId == ( uint32_t )channelId &&
                    routeTerminal->instance == ( uint32_t )instance ) )
                {
                    t2.FindNextNode();
                    continue;
                }
                bool alreadyStored = false;
                for( uint32_t i = 0; found && i < allRouteTerminals.Size(); i++ )
                {
                    if( allRouteTerminals[i]->deviceType == ( uint32_t )deviceType &&
                        allRouteTerminals[i]->channelId == ( uint32_t )channelId &&
                        allRouteTerminals[i]->instance == ( uint32_t )instance )
                    {
                        alreadyStored = true;
                        break;
                    }
                }
                if( !alreadyStored )
                {
                    CRouteTerminal *terminal = new CRouteTerminal();
                    if( NULL != terminal )
                    {
                        terminal->deviceType = deviceType;
                        terminal->channelId = channelId;
                        terminal->instance = instance;
                        allRouteTerminals.PushBack( terminal );
                        found = true;
                    }
                }
                t2.FindNextNode();
            }
            t1.FindNextNode();
        }
        Xml.FindNextNode();
    }
    return success && found;
}
