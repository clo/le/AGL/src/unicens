/*
 * Video On Demand Samples
 *
 * Copyright (C) 2015 Microchip Technology Germany II GmbH & Co. KG
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * You may also obtain this software under a propriety license from Microchip.
 * Please contact Microchip for further information.
 *
 */

/*----------------------------------------------------------*/
/*! \file
 *  \brief  This file contains the CMostIpc class.
 */
/*----------------------------------------------------------*/
#ifndef IPC_H
#define IPC_H

#include <stdbool.h>
#include <pthread.h>
#include <semaphore.h>
#include "SafeVector.h"
#include "MsgAddr.h"
#include "MostMsgTx.h"
#include "MsgAddr.h"
#include "MsgFilter.h"

class CMostIpc;

/*! \cond PRIVATE */
typedef struct
{
    pthread_t workerThread;
    CMostIpc *ipc;
    int clientSock;
    char clientIP[46];
    uint32_t clientPort;
} TcpClientInfo_t;
/*! \endcond */

/*----------------------------------------------------------*/
/*!
 * \brief  Inter Process Communication
 *         1. sends messages using MOST protocol over UDP
 *         2. receives messages and forwards them to their
 *            handlers
 */
/*----------------------------------------------------------*/
class CMostIpc
{
private:
    bool isServer;
    bool allowThreadRun;
    bool udpRxThreadIsRunning;
    bool txThreadIsRunning;
    bool acceptThreadIsRunning;
    CSafeVector<CMostMsgTx *> m_MsgTxQueue;
    CSafeVector<CMsgFilter *> m_MsgFilterQueue;
    CSafeVector<TcpClientInfo_t *> m_TcpClientConnections;
    pthread_mutex_t critical_mutex_rx;
    pthread_mutex_t critical_mutex_tcp;
    sem_t semTx;
    int udpSock;
    int tcpSock;

    /*----------------------------------------------------------*/
    /*! \brief TCP accept thread
     *  \note Never call this method directly!
     */
    /*----------------------------------------------------------*/
    static void *TcpAcceptThread( void *pInst );

    /*----------------------------------------------------------*/
    /*! \brief TCP read / write thread
     *  \note Never call this method directly!
     */
    /*----------------------------------------------------------*/
    static void *TcpWorkerThread( void *pInst );


    /*----------------------------------------------------------*/
    /*! \brief UDP receiving thread
     *  \note Never call this method directly!
     */
    /*----------------------------------------------------------*/
    static void *UdpWorkerRXThread( void *pInst );
    
    /*----------------------------------------------------------*/
    /*! \brief Sending thread
     *  \note Never call this method directly!
     */
    /*----------------------------------------------------------*/
    static void *TXThread( void *pInst );

    /*----------------------------------------------------------*/
    /*! \brief handle a received message
     */
    /*----------------------------------------------------------*/
    void OnMsgReceived( CMsgAddr *Addr, CMostMsg *msg );

    bool SendUdp( const char *ipAddress, uint32_t port, const uint8_t *buffer, uint32_t bufferLen );
    bool SendTcp( const char *ipAddress, uint32_t port, const uint8_t *buffer, uint32_t bufferLen );

public:


    /*----------------------------------------------------------*/
    /*! \brief constructs Inter Process Communication, which acts as Server
     *  \param port - If set to server by isServer=true, this value is the server port which the UDP and TCP sockets are listening.
     *                If set to client by isServer=false, this is the client socket port. If set to 0 or below, a random port will be chosen.
     *                Otherwise the given port will be used as source port of the connection. The server always can handle random and fixed client port connections.
     *  \param isServer - true, if this component shall act as "the" server. false, if this component shall act as one of many clients.
     */
    /*----------------------------------------------------------*/
    CMostIpc( int port, bool isServer );

    /*----------------------------------------------------------*/
    /*! \brief Deletes every resources used by Inter Process Communication
     */
    /*----------------------------------------------------------*/
    ~CMostIpc();


    /*----------------------------------------------------------*/
    /*! \brief send a MOST message
     */
    /*----------------------------------------------------------*/
    void SendMsg( CMostMsgTx *Msg );




    /*----------------------------------------------------------*/
    /*! \brief adds a message handler
     */
    /*----------------------------------------------------------*/
    void RegisterMessageHandler( CMsgFilter *Filter );




    /*----------------------------------------------------------*/
    /*! \brief removes a message handler
     */
    /*----------------------------------------------------------*/
    void UnregisterMessageHandler( CMsgFilter *Filter );

};

#endif //IPC_H

