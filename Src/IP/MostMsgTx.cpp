/*
 * Video On Demand Samples
 *
 * Copyright (C) 2015 Microchip Technology Germany II GmbH & Co. KG
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * You may also obtain this software under a propriety license from Microchip.
 * Please contact Microchip for further information.
 *
 */

#include "MostMsgTx.h"

CMostMsgTx::CMostMsgTx( CMsgAddr *Addr, uint32_t nFBlock, uint32_t nInst, uint32_t nFunc, uint8_t nOpType,
    uint32_t nPayloadLen, const uint8_t *Payload, uint32_t nTimeoutMs, OnMessageSent_t MessageSent,
    void *UserContext ) : CMostMsg( nFBlock, nInst, nFunc, nOpType, nPayloadLen, Payload )
{
    if( NULL != Addr )
    {
        Addr->AddReference();
        m_Addr = Addr;
    }
    m_nTimeoutMs = nTimeoutMs;
    m_MessageSent = MessageSent;
    m_UserContext = UserContext;
}

CMostMsgTx::~CMostMsgTx()
{
    if( NULL != m_Addr )
    {
        m_Addr->RemoveReference();
        m_Addr = NULL;
    }
}



/*----------------------------------------------------------*/
/*! \brief calls message handler with the send result
*/

/*----------------------------------------------------------*/
void CMostMsgTx::MsgSent( bool bSuccess )
{
    if( NULL != m_MessageSent )
        m_MessageSent( bSuccess, this );
}




/*----------------------------------------------------------*/
/*! \brief get target address
*/

/*----------------------------------------------------------*/
CMsgAddr *CMostMsgTx::GetAddr()
{
    return m_Addr;
}




/*----------------------------------------------------------*/
/*! \brief get user context
*/

/*----------------------------------------------------------*/
void *CMostMsgTx::GetUserContext()
{
    return m_UserContext;
}
