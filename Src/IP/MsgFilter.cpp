/*
 * Video On Demand Samples
 *
 * Copyright (C) 2015 Microchip Technology Germany II GmbH & Co. KG
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * You may also obtain this software under a propriety license from Microchip.
 * Please contact Microchip for further information.
 *
 */

#include "Console.h"
#include "MsgFilter.h"

CMsgFilter::CMsgFilter( uint32_t nFBlock, uint32_t nFunc, uint8_t nOpType, OnMsg_t MsgHandler )
{
    m_nFBlock = nFBlock;
    m_nFunc = nFunc;
    m_nOpType = nOpType;
    m_MsgHandler = MsgHandler;
}

void CMsgFilter::Filter( CMsgAddr *Addr, CMostMsg *Msg )
{
    if( NULL == Addr || NULL == Msg )
    {
        ConsolePrintf(
            PRIO_ERROR, RED"CMsgFilter::Filter was called with invalid parameters"RESETCOLOR"\n" );
        return;
    }
    if( NULL != m_MsgHandler )
        if( Msg->GetFBlock() == m_nFBlock && Msg->GetFunc() == m_nFunc
            && Msg->GetOpType() == m_nOpType )
            m_MsgHandler( Addr, Msg );
}
