/*
 * Video On Demand Samples
 *
 * Copyright (C) 2015 Microchip Technology Germany II GmbH & Co. KG
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * You may also obtain this software under a propriety license from Microchip.
 * Please contact Microchip for further information.
 *
 */

/*----------------------------------------------------------*/
/*! \file
 *  \brief  This file contains the CMsgFilter class.
 */
/*----------------------------------------------------------*/
#ifndef MSGFILTER_H
#define MSGFILTER_H

#include <stdint.h>
#include "MsgAddr.h"
#include "MostMsg.h"

typedef void ( *OnMsg_t )( CMsgAddr *Addr, CMostMsg *Msg );

/*----------------------------------------------------------*/
/*!
 * \brief  filters messages and dispatches result to a
 *         handler
 */

/*----------------------------------------------------------*/
class CMsgFilter
{
    uint32_t m_nFBlock;
    uint32_t m_nFunc;
    uint8_t m_nOpType;
    OnMsg_t m_MsgHandler;

public:


    /*----------------------------------------------------------*/
    /*! \brief construct a message filter
     */
    /*----------------------------------------------------------*/
    CMsgFilter( uint32_t nFBlock, uint32_t nFunc, uint8_t nOpType, OnMsg_t MsgHandler );




    /*----------------------------------------------------------*/
    /*! \brief filter an incoming message
     */
    /*----------------------------------------------------------*/
    void Filter( CMsgAddr *Addr, CMostMsg *Msg );
};


#endif //MSGFILTER_H
