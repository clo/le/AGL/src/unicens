/*
 * Video On Demand Samples
 *
 * Copyright (C) 2015 Microchip Technology Germany II GmbH & Co. KG
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * You may also obtain this software under a propriety license from Microchip.
 * Please contact Microchip for further information.
 *
 */

/*----------------------------------------------------------*/
/*! \file
 *  \brief  This file contains the CMsgAddr class.
 */
/*----------------------------------------------------------*/
#ifndef MSGADDR_H
#define MSGADDR_H
#include <stdint.h>

typedef enum
{
    IpcUdp_V2_0 = 1,
    IpcTcp_V2_0 = 2
} IpcProtocol_t;

/*----------------------------------------------------------*/
/*! \brief Storage class, which holds IP address, IP port and MAC address.
*/
/*----------------------------------------------------------*/
class CMsgAddr
{
private:
    int32_t m_nRefCount;
    char m_IpAddr[32];
    uint32_t m_nPort;
    IpcProtocol_t m_protocol;

public:
    CMsgAddr( const char *Addr, uint32_t nPort );
    CMsgAddr( const char *Addr, uint32_t nPort, IpcProtocol_t protocol );
    ~CMsgAddr();

    /*----------------------------------------------------------*/
    /*! \brief Increments the reference counter
     */
    /*----------------------------------------------------------*/
    void AddReference();

    /*----------------------------------------------------------*/
    /*! \brief Decrements the reference counter, if the value reaches 0, this object destroys it self.
     */
    /*----------------------------------------------------------*/
    void RemoveReference();

    char *GetInetAddress();
    uint32_t GetPort();
    IpcProtocol_t GetProtocol();
};

#endif //MSGADDR_H
