/*
 * Video On Demand Samples
 *
 * Copyright (C) 2015 Microchip Technology Germany II GmbH & Co. KG
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * You may also obtain this software under a propriety license from Microchip.
 * Please contact Microchip for further information.
 *
 */

#include <string.h>
#include <stdio.h>
#include "MsgAddr.h"
#include "Console.h"

CMsgAddr::CMsgAddr( const char *Addr, uint32_t nPort ) : m_nRefCount( 1 ), m_nPort( nPort ),
    m_protocol( IpcUdp_V2_0 )
{
    if( NULL != Addr )
        strncpy( m_IpAddr, Addr, sizeof( m_IpAddr ) );
}

CMsgAddr::CMsgAddr( const char *Addr, uint32_t nPort, IpcProtocol_t protocol ) : m_nRefCount( 1 ), m_nPort( nPort ),
    m_protocol( protocol )
{
    if( NULL != Addr )
        strncpy( m_IpAddr, Addr, sizeof( m_IpAddr ) );
}

CMsgAddr::~CMsgAddr()
{
}

void CMsgAddr::AddReference()
{
    ++m_nRefCount;
}

void CMsgAddr::RemoveReference()
{
    if( 0 >= --m_nRefCount )
        delete this;
}

char *CMsgAddr::GetInetAddress()
{
    return ( char * )m_IpAddr;
}

uint32_t CMsgAddr::GetPort()
{
    return m_nPort;
}

IpcProtocol_t CMsgAddr::GetProtocol()
{
    return m_protocol;
}
