/*
 * Video On Demand Samples
 *
 * Copyright (C) 2015 Microchip Technology Germany II GmbH & Co. KG
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * You may also obtain this software under a propriety license from Microchip.
 * Please contact Microchip for further information.
 *
 */

#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include "MacAddr.h"




CMacAddr::CMacAddr()
{
    memset( m_Mac, 0, sizeof( m_Mac ) );
}



CMacAddr::CMacAddr( const uint8_t *pAddress )
{
    if( NULL == pAddress )
    {
        memset( m_Mac, 0, sizeof( m_Mac ) );
        return;
    }
    m_Mac[0] = pAddress[0];
    m_Mac[1] = pAddress[1];
    m_Mac[2] = pAddress[2];
    m_Mac[3] = pAddress[3];
    m_Mac[4] = pAddress[4];
    m_Mac[5] = pAddress[5];
}



CMacAddr::CMacAddr( uint8_t b0, uint8_t b1, uint8_t b2, uint8_t b3, uint8_t b4, uint8_t b5 )
{
    m_Mac[0] = b0;
    m_Mac[1] = b1;
    m_Mac[2] = b2;
    m_Mac[3] = b3;
    m_Mac[4] = b4;
    m_Mac[5] = b5;
}



CMacAddr::CMacAddr( CMacAddr *copySouce )
{
    if( NULL != copySouce )
    {
        memcpy( m_Mac, copySouce->m_Mac, sizeof( m_Mac ) );
    }
}


const char *CMacAddr::ToString()
{
    snprintf( stringRep, sizeof( stringRep ), "%02X-%02X-%02X-%02X-%02X-%02X", m_Mac[0], m_Mac[1], m_Mac[2], m_Mac[3],
        m_Mac[4], m_Mac[5] );
    return stringRep;
}


const uint8_t *CMacAddr::GetBytes()
{
    return m_Mac;
}



void CMacAddr::CopyValuesFromByteArray( const uint8_t *pAddress )
{
    if( NULL == pAddress )
        return;
    m_Mac[0] = pAddress[0];
    m_Mac[1] = pAddress[1];
    m_Mac[2] = pAddress[2];
    m_Mac[3] = pAddress[3];
    m_Mac[4] = pAddress[4];
    m_Mac[5] = pAddress[5];
}



void CMacAddr::CopyValuesFromString( const char *pAddress )
{
    if( NULL == pAddress )
        return;
    uint8_t pos = 0;
    char buf[64];
    char *tok;
    char *tkPtr;
    strncpy( buf, pAddress, sizeof( buf ) );
    tok = strtok_r( buf, "-: ,.", &tkPtr );
    while( NULL != tok && pos < sizeof( m_Mac ) )
    {
        uint32_t val;
        sscanf( tok, "%X", &val );
        m_Mac[pos++] = val;
        tok = strtok_r( NULL, "-: ,.", &tkPtr );
    }
}



CMacAddr::CMacAddr( uint8_t deviceInstance, uint16_t nodeAddress )
{
    if( 0x01 == nodeAddress )
        nodeAddress = 0x100;

    m_Mac[0] =
        0x02;  //Local defined MAC address flag. Has to be unique in the local network.
    m_Mac[1] = 0;
    m_Mac[2] = deviceInstance;
    m_Mac[3] = ( uint8_t )( nodeAddress & 0xFF );
    m_Mac[4] = ( uint8_t )( ( nodeAddress >> 8 ) & 0xFF );
    m_Mac[5] = ( uint8_t )( nodeAddress & 0xFF );
}




CMacAddr::~CMacAddr()
{
}







uint8_t CMacAddr::operator[]( int nByte )
{
    return ( 5 < nByte ) ? 0 : m_Mac[nByte];
}




bool CMacAddr::operator==( CMacAddr const &rhs )
{
    return ( 0 == memcmp( m_Mac, rhs.m_Mac, sizeof( m_Mac ) ) );
}
