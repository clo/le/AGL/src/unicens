/*
 * Video On Demand Samples
 *
 * Copyright (C) 2015 Microchip Technology Germany II GmbH & Co. KG
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * You may also obtain this software under a propriety license from Microchip.
 * Please contact Microchip for further information.
 *
 */

#include "Thread.h"
#include <string.h>
#include "Console.h"

CThread::CThread( const char *threadName, bool maxPrio ) : m_hThread( THREAD_STATE_DONE )
{
    strncpy( m_threadName, threadName, sizeof( m_threadName ) );
    m_maxPrio = maxPrio;
#ifdef THREAD_DEBUG
    ConsolePrintf(PRIO_LOW, "===== Thread '%s' has been created =====\n", m_threadName);
#endif
}

CThread::~CThread()
{
    Stop();
    usleep( 10000 );
    while( IsThreadRunning() )
    {
        ConsolePrintf( PRIO_LOW, "===== Thread '%s' is still running =====\n", m_threadName );
        usleep( 1000000 );
    }
}

void *CThread::RunInst( void *pInst )
{
    CThread *thread = ( CThread * )pInst;
    if( NULL == thread )
        return 0;

    /* set thread priority: */
    if( thread->m_maxPrio )
    {
        int ret;
        pthread_t this_thread = pthread_self();
        struct sched_param params;
        params.sched_priority = sched_get_priority_max( SCHED_FIFO );
        ret = pthread_setschedparam( this_thread, SCHED_FIFO, &params );
        if( ret != 0 )
        {
            ConsolePrintf( PRIO_ERROR, "Thread %s: pthread_setschedparam(SCHED_FIFO) failed\n", thread->m_threadName );
        }
        // Now verify the change in thread priority
        int policy = 0;
        ret = pthread_getschedparam( this_thread, &policy, &params );
        if( ret != 0 )
        {
            ConsolePrintf( PRIO_ERROR, "Thread %s: pthread_getschedparam() failed\n", thread->m_threadName );
        }
        if( policy != SCHED_FIFO )
        {
            ConsolePrintf( PRIO_ERROR, "Thread %s: Scheduling is NOT SCHED_FIFO!\n", thread->m_threadName );
        }
        else
        {
            ConsolePrintf( PRIO_LOW, "Thread %s: SCHED_FIFO OK\n", thread->m_threadName );
        }
        ConsolePrintf( PRIO_LOW, "Thread %s: Thread priority is %d\n", thread->m_threadName, params.sched_priority );
    }

    while( THREAD_STATE_STOPPING !=
        thread->m_hThread )
    { // until thread is marked for termination
#ifdef THREAD_DEBUG
        ConsolePrintf(PRIO_LOW, "+++++ Thread '%s' run before Run +++++\n", thread->m_threadName);
#endif
        ( ( CThread * )pInst )->Run();
#ifdef THREAD_DEBUG
        ConsolePrintf(PRIO_LOW, "----- Thread '%s' run after Run -----\n", thread->m_threadName);
#endif
    }

    ( ( CThread * )pInst )->m_hThread = THREAD_STATE_DONE; // mark thread as done

    return 0;
}

void CThread::Start()
{
    if( THREAD_STATE_DONE != m_hThread )
        return;

    struct sched_param param;
    pthread_attr_t tattr;
    pthread_attr_init( &tattr );
    pthread_attr_getschedparam( &tattr, &param );
    pthread_attr_setschedpolicy( &tattr, SCHED_RR );
    if( m_maxPrio )
    {
        param.sched_priority = sched_get_priority_max( SCHED_RR );
        pthread_attr_setschedparam( &tattr, &param );
        // note: this doesn't seem to work, so we're setting prio in RunInst() now
    }
    pthread_create( &m_hThread, &tattr, CThread::RunInst, ( void * )this );
}

void CThread::Stop()
{
    if( m_hThread )
        m_hThread = THREAD_STATE_STOPPING;
}

bool CThread::IsThreadRunning()
{
    return ( THREAD_STATE_DONE != m_hThread );
}
