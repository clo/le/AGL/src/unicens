/*
 * Video On Demand Samples
 *
 * Copyright (C) 2015 Microchip Technology Germany II GmbH & Co. KG
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * You may also obtain this software under a propriety license from Microchip.
 * Please contact Microchip for further information.
 *
 */

/*----------------------------------------------------------*/
/*! \file
 *  \brief  This file contains the CXml class.
 */
/*----------------------------------------------------------*/
#ifndef _XML_H_
#define _XML_H_

#include "Types.h"
#include <libxml/tree.h>
#include <libxml/parser.h>


#define XML_DEFAULT_VALUE_LEN   255

/*----------------------------------------------------------*/
/*! \brief Generic class to read XML files. It does not implement a concrete use case.
 */
/*----------------------------------------------------------*/
class CXml
{
    xmlDoc *m_Doc;
    xmlNode *m_Node;
    bool m_bDocOpened;
protected:
    static int ConvertToInt( xmlChar *xString );

public:
    /*----------------------------------------------------------*/
    /*! \brief construct a XML document from a file name
     */
    /*----------------------------------------------------------*/
    CXml( const char *szFile );


    /*----------------------------------------------------------*/
    /*! \brief construct a XML document from a zero terminated string in the memory.
     */
    /*----------------------------------------------------------*/
    CXml( const char *szBuffer, uint32_t nBufferLen );


    /*----------------------------------------------------------*/
    /*! \brief construct a XML document from a given node
     */
    /*----------------------------------------------------------*/
    CXml( CXml &Node );



    /*----------------------------------------------------------*/
    /*! \brief construct a XML document from a file name
     */
    /*----------------------------------------------------------*/
    ~CXml();



    /*----------------------------------------------------------*/
    /*! \brief gets top node inside root of XML document
     *
     * \return true, if successful
     */
    /*----------------------------------------------------------*/
    bool SetToTopNode();



    /*----------------------------------------------------------*/
    /*! \brief find first child node
     *
     * \return true, if child node was found
     */
    /*----------------------------------------------------------*/
    bool FindFirstChildNode();




    /*----------------------------------------------------------*/
    /*! \brief find next node on same level
     *
     * \return true, if node was found
     */
    /*----------------------------------------------------------*/
    bool FindNextNode();




    /*----------------------------------------------------------*/
    /*! \brief find a XML node with a given tag
     *
     * \param szTag - tag looking for
     *
     * \return XML node
     */
    /*----------------------------------------------------------*/
    bool FindNode( const xmlChar *szTag );





    /*----------------------------------------------------------*/
    /*! \brief searches a node, which has a child node with a
     *         given value
     *
     * \param szNodeTag - tag of nodes which are included in search
     * \param szValueTag - tag of value inside node
     * \param szValue - value looked for
     *
     * \return XML true if found
     */
    /*----------------------------------------------------------*/
    bool FindNodeWithValue( const xmlChar *szNodeTag, const xmlChar *szValueTag, const xmlChar *szValue );



    /*----------------------------------------------------------*/
    /*! \brief searches node, which has a child node with a
     *         given integer value
     *
     * \param szNodeTag - tag of nodes which are included in search
     * \param szValueTag - tag of value inside node
     * \param szValue - value looked for
     *
     * \return true if found
     */
    /*----------------------------------------------------------*/
    bool FindNodeWithValueInt( const xmlChar *szNodeTag, const xmlChar *szValueTag, int nValue );

    /*----------------------------------------------------------*/
    /*! \brief get node's child value as string
     *
     * \param szTag - tag of the value inside the node
     *
     * \return value as string if found, otherwise NULL
     */
    /*----------------------------------------------------------*/
    bool GetChildValue( const xmlChar *szTag, xmlChar *szValue, int nMaxLen = XML_DEFAULT_VALUE_LEN );


    /*----------------------------------------------------------*/
    /*! \brief get node's child value as int
     *
     * \param szTag - tag of the value inside the node
     * \param nErr - value returned in case of an error
     *
     * \return value as int
     */
    /*----------------------------------------------------------*/
    bool GetChildValueInt( const xmlChar *szTag, int &nValue );
};

#endif
