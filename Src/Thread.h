/*
 * Video On Demand Samples
 *
 * Copyright (C) 2015 Microchip Technology Germany II GmbH & Co. KG
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * You may also obtain this software under a propriety license from Microchip.
 * Please contact Microchip for further information.
 *
 */

/*----------------------------------------------------------*/
/*! \file
 *  \brief  This file contains the CThread class.
 */
/*----------------------------------------------------------*/
#ifndef THREAD_H
#define	THREAD_H

#include <pthread.h>
#include <unistd.h>
#include <stdint.h>
#include <stdio.h>

//#define THREAD_DEBUG
#define THREAD_STATE_DONE       0
#define THREAD_STATE_STOPPING   (pthread_t)-1

/*----------------------------------------------------------*/
/*! \brief Helper class to deal with Threads.
 */
/*----------------------------------------------------------*/
class CThread
{
    pthread_t m_hThread;
    char m_threadName[32];
    bool m_maxPrio;

public:
    CThread( const char *threadName, bool maxPrio );
    virtual ~CThread();

    /*----------------------------------------------------------*/
    /*! \brief Starts the thread execution.
     */
    /*----------------------------------------------------------*/
    void Start();

    /*----------------------------------------------------------*/
    /*! \brief Stops the thread execution.
     */
    /*----------------------------------------------------------*/
    void Stop();

    /*----------------------------------------------------------*/
    /*! \brief Determines if the thread is running.
     *  \return true, if the thread is running. false, if the thread is stopped.
     */
    /*----------------------------------------------------------*/
    bool IsThreadRunning();

    /*----------------------------------------------------------*/
    /*! \brief This method must be overridden the deriving class.
     *         This method will then run in an own thread.
     * 		   This method will be infinitely called, as long as the CThread object is marked to run.
     */
    /*----------------------------------------------------------*/
    virtual void Run() = 0;

private:
    static void *RunInst( void *pInst );

};

#endif

