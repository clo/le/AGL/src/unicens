/*
 * Video On Demand Samples
 *
 * Copyright (C) 2015 Microchip Technology Germany II GmbH & Co. KG
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * You may also obtain this software under a propriety license from Microchip.
 * Please contact Microchip for further information.
 *
 */

/*----------------------------------------------------------*/
/*! \file
 *  \brief  This file generates the start page of the document you are currently reading.
 */
/*----------------------------------------------------------*/
#ifndef DOXYGEN_START_PAGE_H
#define DOXYGEN_START_PAGE_H


/*----------------------------------------------------------*/
/*! \mainpage
 *
 * \section intro_sec Introduction
 *
 * This package holds the sample application and the Linux driver to realize a certain use cases like MOST based Video on Demand Service, Slim-Audio Applications and Slim-Camera Applications. \n
 * All components where written for and tested for the on various Linux distributions on FreeScale i.MX6q, Ubuntu 14.4 LTS 64Bit on PC, Raspbian on Raspbery Pi 2. \n
 *
 * \section Package Contents
 *
 * \subsection examples Folder: "./Examples"
 * 		- All the examples
 * \subsubsection networkmanager Folder: "./Examples/NetworkManager"
 *      - The main application, which starts up MOST and configure all connections. \n
 * \subsubsection videoondemand Folder: "./Examples/VideoOnDemand"
 *      - The Video On Demand Streaming Server and client examples \n
 *        <a href="../../../VideoOnDemand/Server/html/index.html">Click here</a> to see the documentation of the VideoOnDemand Example.
 * \subsubsection audiomixer Folder: "./Examples/AudioMixer"
 *      - The synchronous Audio Mixer example\n
 *        <a href="../../../AudioMixer/doc/html/index.html">Click here</a> to see the documentation of the AudioMixer Example.
 * \subsubsection PatternCheck Folder: "./Examples/PatternCheck"
 *      - Pattern Generator and Pattern Checker example\n
 * \subsubsection SlimCamera Folder: "./Examples/SlimCamera"
 *      - SlimCamera example, with helper tool to stream from MOST isochronous channel to Ethernet UDP.\n
 * \subsection linuxdriver Folder: "./LinuxDriver"
 *		- The Linux driver modules as source code. \n
 *        <a href="../../../../LinuxDriver/html/index.html">Click here</a> to see the documentation of the MOST Linux Driver.
 * \subsection resources Folder:"./Resources"
 *      - Shell scripts to convert media typess.
 * 		- INIC Explorer Windows tool.
 * 		- INIC firmware.
 * 		- ConfigString sample setups.
 *      - Shell scripts to convert Audio and Video.
 *
 * \section install_sec Setup
 * \note General hint: Do not use white spaces in the path to this package, as some of the commands
 * 		 listed here need to be adapted otherwise.
 *
 * \subsection step1 Step 1: Unpack the delivered file
 * 		- Enter:
 * \code tar xvfz NM-Vx.x.x.tar.gz \endcode
 * to unpack the folder. Replace the 'x' characters with you version.
 *
 * \subsection step2 Step 2: Edit the cross compiling environment variables and load them
 * Use your favorite text editor to edit "crosscompile.sh".:
 * 		- Example:
 * \code vi crosscompile.sh \endcode
 * 		- Or:
 * \code gedit crosscompile.sh \endcode
 * 		- Set your cross-compiler-toolchain path to the "CROSS_COMPILE" variable.
 * 		- Set the target Linux kernel sources path to the "KDIR" variable.
 *      - Set the compiler flags (such as platform specific variables or setting up the path where the libraries shall be searched) to the "PROJECT_C_FLAGS".
 *      - Set the path to search for header files to the INCLUDE_PATH variable.
 *      - Set variables needed by the linker to LIBRARIES_PATH.
 *      
 *      - Example configuration made for a Yocto image on iMX6:
 * \code
 * #!/bin/sh
 * source /opt/poky/1.6.2/environment-setup-cortexa9hf-vfp-neon-poky-linux-gnueabi 
 * export ARCH=arm
 * export CC=${CROSS_COMPILE}gcc
 * export KDIR=/home/x/fsl-community-bsp/build/tmp/work/imx6qsabreauto-poky-linux-gnueabi/linux-imx/3.10.53-r0/git
 * export PROJECT_C_FLAGS="-static -L /home/x/fsl-community-bsp/build/tmp/sysroots/imx6qsabreauto/usr/lib -lm -lz -lcrypt -pthread"
 * export INCLUDE_PATH="-I/home/x/fsl-community-bsp/build/tmp/sysroots/imx6qsabreauto/usr/include/libxml2"
 * export LIBRARIES_FLAGS="-lm -lz -lcrypt"
 * \endcode
 * <br/>
 *      - Example configuration made for a LTIB image on iMX6:
 * \code
 * #!/bin/sh
 * export CROSS_COMPILE=/opt/freescale/usr/local/gcc-4.6.2-glibc-2.13-linaro-multilib-2011.12/fsl-linaro-toolchain/bin/arm-none-linux-gnueabi-
 * export ARCH=arm
 * export CC=${CROSS_COMPILE}gcc
 * export KDIR=/home/x/imx6/sdk/ltib/rpm/BUILD/linux
 * export PROJECT_C_FLAGS="-L /home/x/imx6/sdk/ltib/rootfs/usr/lib -lm -lz -lcrypt -pthread"
 * export INCLUDE_PATH="-I/home/x/imx6/sdk/ltib/rootfs/usr/include/libxml2"
 * export LIBRARIES_FLAGS="-lm -lz -lcrypt"
 * \endcode
 * <br/>
 * 
 * Next load the variables.
 * - Enter:
 * \code source crosscompile.sh \endcode
 * - To check if the variables are set, at any time for a certain terminal, enter:
 * \code echo $ARCH \endcode
 * 		When "arm" is printed, the variables are loaded.
 * \note The variables are only loaded in the current shell. If you open a new shell or a new tab, the variables have then loaded again.
 * \note If you want to run the binaries on the same host and you are on a Debian based system, you may skip this step. If you encounter problems anyway, you may have to set the PROJECT_C_FLAGS, INCLUDE_PATH and LIBRARIES_PATH also.
 *
 * \subsection step3 Step 3: Compile driver and examples
 * The VoD server sample application needs the libxml2 library, this may cause problems while compiling and linking. In this case, you have to find the library and the headers for your target. Maybe your platform builder (LTIB, Yocto) can create the needed library.\n
 * On a Debian based system you can easily install these packages by typing:
 * \code sudo apt-get install build-essential libxml2-dev \endcode
 *
 * Compiling all the examples and the MOST Linux Driver is done by calling make in the root folder of the package.
 * \code make \endcode
 * 
 * It may be necessary to patch the Linux Driver source code, depending on the Kernel version you use.
 * For Kernel Version 3.16.x, enter:
 * \code 
 * make patch-kernel-3.16
 * make
 * \endcode
 * For Kernel Version 3.18.x, enter:
 * \code 
 * make patch-kernel-3.18
 * make
 * \endcode
 * If you are facing problems with a different kernel version, please let us know: support-ais-de@microchip.com
 *
 * Compiling just the examples, without the MOST Linux Driver, enter:
 * \code make examples\endcode
 * 
 * If you want to build also the driver for DIM2-IP, enter:
 * \code make dim2\endcode
 * 
 * If you want to clean up the folder, enter:
 * \code make clean \endcode
 * \subsection step4 Step 4: Setup Streaming parameters and deploy configuration file
 * Use your favorite text editor to edit "config.xml". \n
 * Example:
 * \code vi config.xml \endcode
 * or:
 * \code gedit config.xml \endcode
 *
 * The given file is ready to work, but it may be changed to reflect the real use cases.
 *
 * The parameters that may be changed by the user are:
 * 	 - \code network_manager.network_config \endcode Must be instanced only once.
 *
 * 	 - \code network_manager.network_config.timing_master \endcode
 *              - 1 = VoD Server will act as timing master on all connected INICs.
 *              - 0 = VoD Server will act as timing slave on all connected INICs.
 *
 * 	 - \code network_manager.network_config.async_bandwidth \endcode = The amount of bytes reserved for Ethernet data. \n
 *              - 0 = no Ethernet traffic possible, max isochronous Bandwidth. \n
 *              - 372 = Max Ethernet Bandwidth, no isochronous traffic possible. \n
 *
 * 	 - \code network_manager.device \endcode May be instanced multiple times.
 *
 * 	 - \code network_manager.device.device_type \endcode Identifier for a specific types of hardware. \n
 *       The implementation uses the INIC group address to hold this value. See Step 5. \n
 *       So make sure to set the value correctly in the ConfigString with INIC Explorer tool. \n
 *
 *       Here some examples: \n
 *         - 784 (0x310): Server with OS81118 \n
 *         - 810 (0x320): Seat with OS81110 \n
 *         - 816 (0x330): Seat with OS81118 \n
 *         - 832 (0x340): Tuner with OS81110 \n
 *       \note This value must be specified in decimal way. \n Setting hex value with leading 0x will not work. \n
 *
 * 	 - \code network_manager.device.device_api_ver \endcode The Port Message Protocol Version number. \n
 *            - OS81110 uses API version 1, so set value to 1. \n
 *            - OS81118 uses API version 2, so set value to 2. \n
 *
 * 	 - \code network_manager.device.channel \endcode May be instanced multiple times.
 *
 * 	 - \code network_manager.device.channel.channel_id \endcode Unique identifier for this channel.
 *          May be incremented with every new instance inside "network_manager.device".
 *
 * 	 - \code network_manager.device.channel.socket \endcode" Must be instanced two times for all data types except of asynchronous channel. \n
 *            One instance is the input socket and the other is the output socket. \n
 *            \note Asynchronous channel must be one channel, specify the bus to the EHC (e.g. MLB). \n
 *            The asynchronous MOST channel will be automatically created.
 *
 * 	 - \code network_manager.device.channel.socket.dir \endcode Specifies the direction of the current socket. \n
 *          Possible values are: \n
 *        - "IN" - The current socket will act as source.
 *        - "OUT" - The current socket will act as sink.
 *
 * 	 - \code network_manager.device.channel.socket.port_id \endcode Enumeration declaring the type of physical port to use. \n
 *             Possible values are currently: \n
 *             - "MLB" - Media Local Bus, OS81110 and OS81118 \n
 *             - "USB" - Universal Serial Bus, OS81118 \n
 *             - "I2S" - Streaming port, OS81110 and OS81118 \n
 *             - "MOST"
 *
 * 	 - \code network_manager.device.channel.socket.data_type \endcode Enumeration declaring the type of data type transmitted. \n
 *               Possible value currently only: \n
 *               - "ISOC" - Isochronous data. \n
 *               - "ASYNC" - Asynchronous data.
 *               - "SYNC" - Synchronous data.
 *
 * 	 - \code network_manager.device.channel.socket.channel_addr \endcode Is only valid for non MOST physical ports. \n
 *                 - In case of MLB this is the MLB-channel address. \n
 *                 - In case of USB this is the USB endpoint address. \n
 *                 \note This value must be specified in decimal way. \n Setting hex value with leading 0x will not work. \n
 *
 * 	 - \code network_manager.device.channel.socket.blockwidth \endcode Is only valid for the MOST and MLB physical ports.
 *                  The value is given as amount of bytes, reserved specific for this socket.
 *
 * 	 - \code network_manager.device.channel.socket.number_of_buffers \endcode Is only valid for local attached INICs.
 *                  The value is given as amount of buffers used in the driver.
 * 	                \note This property is optional. If not set, the application will try to use default values.
 *
 * 	 - \code network_manager.device.channel.socket.buffer_size \endcode Is only valid for local attached INICs.
 *                  The value is given as amount of bytes of a single buffer used in the driver.
 * 	                \note This property is optional. If not set, the application will try to use default values.
 *
 * 	 - \code network_manager.device.channel.socket.packets_per_xact \endcode Is only valid for local attached INICs.
 *                  The value is given as amount packets transmitted in one USB frame which is max. 512 byte long.
 *                  For datatype ISOC and SYNC stuffing will be used,to fill up the unused bytes of the USB frame.
 * 	                \note This property is optional. If not set, the application will try to use default values.
 *
 * 	 - \code network_manager.device.channel.socket.subbuffer_size \endcode
 *                  The value is given as amount of bytes. It measures the width of a subbuffer.
 *                  For datatype ISOC this may be for example 188 or 196 byte.
 * 	                \note This property is optional. If not set, the application will try to use default values.
 *
 * 		Copy the configuration to the target: \n
 * 		Example:  \n
 * 		\code scp config.xml root@192.168.1.100:/root \endcode
 * 		\note If you do not cross compile, you can skip the copy process. Otherwise you need to adapt the IP address according to your targets address. The folder is freely choosable, here: "/root".
 *
 * \subsection step5 Step 5: Setup INICs on all platforms
 * \note InicExplorer V1 and Target Manager (InicExplorer V2) will run under Microsoft Windows operating systems only.
 *
 * The latest version of InicExplorer V1 can be downloaded from this URL: \n
 * http://www.k2l.de/products/analysis-and-verification-tools/inic-medialb-tools/inic-explorer/inic-explorer-downloads
 *
 * The Target Manager (InicExplorer V2) installer is included in this package in the sub-folder "Resources".
 *
 * - The configuration of OS81110 is done with the InicExplorer V1 tool.
 * - The configuration of OS81118 is done with the Target Manager (InicExplorer V2) tool.
 *
 * \note <b>Important hint:</b> The <b>MOST group address</b> set in the ConfigString is interpreted as DeviceType! \n
 *                This means certain types of devices share the same group address. \n
 *                Here some examples, configured by the config.xml shipped along with this package: \n
 *                - 784 (0x310): Server with OS81118
 *                - 810 (0x320): Seat with OS81110
 *                - 816 (0x330): Seat with OS81118
 *                - 832 (0x340): Tuner with OS81110
 *
 *                Also there are example configuration files, for the Target Manager (InicExplorer V2) (FileEnding.jscs). \n
 * \n
 *                Currently the VoD server application expects the OS8118 USB endpoints to be configured as following: \n
 *                Receive endpoints:
 *                    - EP-81 = RX CONTROL
 *                    - EP-82 = RX ASYNC
 *                    - EP-83 = RX
 *                    - EP-84 = RX
 *                    - EP-85 = RX
 *                    - EP-86 = RX
 *                    - EP-87 = RX
 *
 *                Transmit endpoints:
 *                    - EP-08 = TX CONTROL
 *                    - EP-09 = TX ASYNC
 *                    - EP-0A = TX
 *                    - EP-0B = TX
 *                    - EP-0C = TX
 *                    - EP-0D = TX
 *                    - EP-0E = TX
 *                    - EP-0F = TX
 *
 * For the OS81118 INICs attached to the server device do following steps:
 * - Attach InicExplorer hardware to UART and connect it to the INIC connector.
 * - Open Target Manager (InicExplorer V2)
 * - Press the "Detect"-button. (If not found, try different COM instance)
 * - Flash INIC firmware, provided by this package, by pressing "Flash"-button.
 * - Select "OS81118_Vx_x_x_x_PV.mchpkg" and press the "Open"-button. Replace the 'x' characters with you version.
 * - Make sure that in the next window there is "configstring" and "firmware"-check-box is checked. Then press "Ok"-Button.
 * - Flashing takes some minutes.
 * - After successful flashing, press "Explore"-button.
 * - Press "Ok"-Button in the upcoming Package Selector dialogue.
 * - Press "Load"-Button in the Configuration String Editor Window.
 * - Select "inic-config-ga310.jscs", and press "Open"-Button.
 * - Verify the settings, if they match to your needs.
 * - Press "Write"-Button to write the configuration into the INIC.
 *
 * \subsection step6 Step 6: Load Linux driver on target
 * Login into your target. This step will be necessary both for server and clients. \n
 * Switch to the directory, where you have copied all outputs of the previous steps. \n
 *
 * In order to load all drivers and set them up in one step.\n
 * Enter: \n
 * \code sudo ./loadDriver.sh &\endcode
 * \note The trailing "&" will run the script as daemon. It will run in background and set the driver up, whenever a new INIC is detected. \n
 *
 * \note If the device does not appear in the /dev folder, check if the USB device was registered to the system by typing: \code lsusb \endcode
 *
 * In order to unload the driver and the background daemon.\n
 * Enter: \n
 * \code
 * sudo killall loadDriver.sh
 * sudo ./unloadDriver.sh
 * \endcode
 *
 *
 * \subsection step7 Step 7: Enable Ethernet communication on MOST
 *
 * The "loadDriver.sh" shell script, started in the previous script, already started the MOST Ethernet device.\n
 * But in order to get a correct network setup, you have to assign unique IP-addresses to each of your target.
 *
 * Login to the client target device. \n
 * Switch to the directory, where you have copied all outputs of the previous steps. \n
 * \n
 * Check if the meth0 device is created. \n
 * Enter:\n
 * \code ifconfig\endcode
 * \n
 * Display Output:\n
 * \code
 * meth0     Link encap:Ethernet  HWaddr 02:00:00:00:00:01
 *           inet addr:10.0.0.1  Mask:255.255.255.0
 *           BROADCAST MULTICAST  MTU:1500  Metric:1
 *           RX packets:0 errors:0 dropped:0 overruns:0 frame:0
 *           TX packets:0 errors:0 dropped:0 overruns:0 carrier:0
 *           collisions:0 txqueuelen:1000
 *           RX bytes:0 (0.0 B)  TX bytes:0 (0.0 B)
 * \endcode
 * \n
 * The IP address 10.0.0.1 is already set in the shell script mentioned above.\n
 * \n
  * Use your favorite text editor to edit "loadDriver.sh".:
 * 		- Example:
 * \code vi loadDriver.sh \endcode
 * 		- Or:
 * \code gedit loadDriver.sh \endcode
 *
 * Scroll down to the StartEthernetUsb function, you will find the ifconfig statement which sets the
 * IP address to 10.0.0.1. \n
 *
 * You have to adapt the address in this way, so it is unique in the whole network. This may be done
 * by incrementing the last number with every new target (10.0.$i.<b>1</b>).
 *
 * \code
 * #ETHERNET
 *		SetEthChannel $i ep82 rx 2048
 *		SetEthChannel $i ep09 tx 2048 10.0.$i.1
 * \endcode
 * \n
 * \note Do this step for any client.\n
 *
 * \subsection Appendix Appendix:
 * Timing sequence of NetworkManager startup:
 * <img src="NMSequence.png" alt="NetworkManager startup sequence" width="80%"/>\n
 */
/*----------------------------------------------------------*/



#endif //DOXYGEN_START_PAGE_H
