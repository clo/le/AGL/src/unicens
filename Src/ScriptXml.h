/*
 * Video On Demand Samples
 *
 * Copyright (C) 2015 Microchip Technology Germany II GmbH & Co. KG
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * You may also obtain this software under a propriety license from Microchip.
 * Please contact Microchip for further information.
 *
 */

/*----------------------------------------------------------*/
/*! \file
 *  \brief  This file contains the CSciptXml class.
 */
/*----------------------------------------------------------*/
#ifndef _SCRIPTXML_H_
#define _SCRIPTXML_H_

#include <stdint.h>
#include <stdbool.h>
#include <Types.h>
#include "Xml.h"

#define MAX_SCRIPT_NAME_LENGTH   64
#define MAX_PAYLOAD_LENGTH       64

typedef enum ScriptType_tag
{
    SCRIPT_UNKNOWN,
    SCRIPT_MCM_SEND,
    SCRIPT_PAUSE
} ScriptType_t;

/*----------------------------------------------------------*/
/*!
 * \brief Storage class to store the scripting informations. This class will be returned by
 *        the CSciptXml class.
 */
/*----------------------------------------------------------*/
class CScript
{
public:
    ScriptType_t scriptType;
    char scriptName[MAX_SCRIPT_NAME_LENGTH];
    uint8_t fblockId;
    uint16_t functionId;
    uint8_t opTypeRequest;
    uint8_t opTypeResponse;
    uint8_t payload[MAX_PAYLOAD_LENGTH];
    uint32_t payloadLength;
    uint32_t pauseInMillis;

    CScript() : scriptType( SCRIPT_UNKNOWN ), fblockId( 0xFF ), functionId( 0xFFFF ), opTypeRequest( 0xFF ),
                opTypeResponse( 0xFF ), payloadLength( 0 ), pauseInMillis( 0 )
    {
        scriptName[0] = '\0';
    }
};

/*----------------------------------------------------------*/
/*!
 *  \brief Class to read XML for the NetworkManager scripting use case.
 */
/*----------------------------------------------------------*/
class CSciptXml : public CXml
{
private:
    void StorePayload( xmlChar *xString, CScript *script );
public:
    CSciptXml( const char *szFile );
    CSciptXml( const char *szBuffer, uint32_t nBufferLen );
    bool GetScripts( CSafeVector<CScript *> &allChannels );
};

#endif //_SCRIPTXML_H_
