#
# Generated Makefile - do not edit!
#
# Edit the Makefile in the project folder instead (../Makefile). Each target
# has a -pre and a -post target defined where you can add customized code.
#
# This makefile implements configuration specific macros and targets.


# Environment
MKDIR=mkdir
CP=cp
GREP=grep
NM=nm
CCADMIN=CCadmin
RANLIB=ranlib
CC=${CROSS_COMPILE}gcc
CCC=${CROSS_COMPILE}g++
CXX=${CROSS_COMPILE}g++
FC=${CROSS_COMPILE}gfortran
AS=${CROSS_COMPILE}as

# Macros
CND_PLATFORM=GNU-Linux-x86
CND_DLIB_EXT=so
CND_CONF=Release
CND_DISTDIR=dist
CND_BUILDDIR=build

# Include project Makefile
include Makefile

# Object Directory
OBJECTDIR=${CND_BUILDDIR}/${CND_CONF}/${CND_PLATFORM}

# Object Files
OBJECTFILES= \
    ${OBJECTDIR}/Src/ConnectionInfo.o \
    ${OBJECTDIR}/Src/Console.o \
	${OBJECTDIR}/Src/IP/MostIpc.o \
	${OBJECTDIR}/Src/IP/MostMsg.o \
	${OBJECTDIR}/Src/IP/MostMsgTx.o \
	${OBJECTDIR}/Src/IP/MsgAddr.o \
	${OBJECTDIR}/Src/IP/MsgFilter.o \
	${OBJECTDIR}/Src/MacAddr.o \
	${OBJECTDIR}/Src/Main.o \
	${OBJECTDIR}/Src/Network/Network.o \
	${OBJECTDIR}/Src/Network/Network_CB.o \
	${OBJECTDIR}/Src/Network/Network_Private.o \
	${OBJECTDIR}/Src/Network/NetworkDevice.o \
	${OBJECTDIR}/Src/Network/NetworkDeviceListener.o \
	${OBJECTDIR}/Src/Network/NodeDatabase.o \
	${OBJECTDIR}/Src/Network/IndustrialStack.o \
	${OBJECTDIR}/Src/Network/IndustrialStack_LLD.o \
	${OBJECTDIR}/Src/Network/IndustrialStack_MNS.o \
	${OBJECTDIR}/Src/Network/base/Board.o \
	${OBJECTDIR}/Src/Network/base/DriverConfiguration.o \
	${OBJECTDIR}/Src/Thread.o \
	${OBJECTDIR}/Src/ScriptXml.o \
	${OBJECTDIR}/Src/VodXml.o \
	${OBJECTDIR}/Src/Xml.o \
	${OBJECTDIR}/mnsl/mns_alm.o \
	${OBJECTDIR}/mnsl/mns_ams.o \
	${OBJECTDIR}/mnsl/mns_amsmessage.o \
	${OBJECTDIR}/mnsl/mns_amspool.o \
	${OBJECTDIR}/mnsl/mns_base.o \
	${OBJECTDIR}/mnsl/mns_dl.o \
	${OBJECTDIR}/mnsl/mns_eh.o \
	${OBJECTDIR}/mnsl/mns_encoder.o \
	${OBJECTDIR}/mnsl/mns_lldpool.o \
	${OBJECTDIR}/mnsl/mns_message.o \
	${OBJECTDIR}/mnsl/mns_misc.o \
	${OBJECTDIR}/mnsl/mns_obs.o \
	${OBJECTDIR}/mnsl/mns_pmchannel.o \
	${OBJECTDIR}/mnsl/mns_pmfifo.o \
	${OBJECTDIR}/mnsl/mns_pmp.o \
	${OBJECTDIR}/mnsl/mns_pool.o \
	${OBJECTDIR}/mnsl/mns_scheduler.o \
	${OBJECTDIR}/mnsl/mns_segmentation.o \
	${OBJECTDIR}/mnsl/mns_timer.o \
	${OBJECTDIR}/mnsl/mns_telqueue.o \
	${OBJECTDIR}/mnsl/mns_transceiver.o \
    ${OBJECTDIR}/mnsl/mns_pmcmd.o \
    ${OBJECTDIR}/mnsl/mns_pmfifos.o \
	${OBJECTDIR}/mnsl/mnsl.o


# Include Path
C_INCLUDE=-Imnsl -ISrc -ISrc/IP -ISrc/Network -ISrc/Network/base -I/usr/include/libxml2 ${INCLUDE_PATH}

# C Compiler Flags
CFLAGS=-c -Wall -O2 -MMD -MP -DNDEBUG ${PROJECT_C_FLAGS}

# CC Compiler Flags
CCFLAGS=-Wall -O2 -MMD -MP -DNDEBUG ${PROJECT_C_FLAGS}
CXXFLAGS=${CCFLAGS}

# Fortran Compiler Flags
FFLAGS=

# Assembler Flags
ASFLAGS=

# Link Libraries and Options
LDLIBSOPTIONS=-lpthread -lxml2 -lrt ${LIBRARIES_FLAGS}

# Build Targets
.build-conf: ${BUILD_SUBPROJECTS}
	"${MAKE}"  -f buildX86/Makefile-${CND_CONF}.mk NetworkManager

NetworkManager: ${OBJECTFILES}
	${MKDIR} -p ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}
	${LINK.cc} -o NetworkManager ${OBJECTFILES} ${LDLIBSOPTIONS}

${OBJECTDIR}/Src/ConnectionInfo.o: Src/ConnectionInfo.cpp 
	${MKDIR} -p ${OBJECTDIR}/Src
	${RM} "$@.d"
	${CXX} ${CFLAGS} ${C_INCLUDE} -MF "$@.d" -o ${OBJECTDIR}/Src/ConnectionInfo.o Src/ConnectionInfo.cpp
	
${OBJECTDIR}/Src/Console.o: Src/Console.c 
	${MKDIR} -p ${OBJECTDIR}/Src
	${RM} "$@.d"
	${CC} ${CFLAGS} ${C_INCLUDE} -MF "$@.d" -o ${OBJECTDIR}/Src/Console.o Src/Console.c

${OBJECTDIR}/Src/IP/MostIpc.o: Src/IP/MostIpc.cpp
	${MKDIR} -p ${OBJECTDIR}/Src/IP
	${RM} "$@.d"
	${CXX} ${CFLAGS} ${C_INCLUDE} -MF "$@.d" -o ${OBJECTDIR}/Src/IP/MostIpc.o Src/IP/MostIpc.cpp

${OBJECTDIR}/Src/IP/MostMsg.o: Src/IP/MostMsg.cpp
	${MKDIR} -p ${OBJECTDIR}/Src/IP
	${RM} "$@.d"
	${CXX} ${CFLAGS} ${C_INCLUDE} -MF "$@.d" -o ${OBJECTDIR}/Src/IP/MostMsg.o Src/IP/MostMsg.cpp

${OBJECTDIR}/Src/IP/MostMsgTx.o: Src/IP/MostMsgTx.cpp
	${MKDIR} -p ${OBJECTDIR}/Src/IP
	${RM} "$@.d"
	${CXX} ${CFLAGS} ${C_INCLUDE} -MF "$@.d" -o ${OBJECTDIR}/Src/IP/MostMsgTx.o Src/IP/MostMsgTx.cpp

${OBJECTDIR}/Src/IP/MsgAddr.o: Src/IP/MsgAddr.cpp
	${MKDIR} -p ${OBJECTDIR}/Src/IP
	${RM} "$@.d"
	${CXX} ${CFLAGS} ${C_INCLUDE} -MF "$@.d" -o ${OBJECTDIR}/Src/IP/MsgAddr.o Src/IP/MsgAddr.cpp

${OBJECTDIR}/Src/IP/MsgFilter.o: Src/IP/MsgFilter.cpp
	${MKDIR} -p ${OBJECTDIR}/Src/IP
	${RM} "$@.d"
	${CXX} ${CFLAGS} ${C_INCLUDE} -MF "$@.d" -o ${OBJECTDIR}/Src/IP/MsgFilter.o Src/IP/MsgFilter.cpp

${OBJECTDIR}/Src/MacAddr.o: Src/MacAddr.cpp 
	${MKDIR} -p ${OBJECTDIR}/Src
	${RM} "$@.d"
	${CXX} ${CFLAGS} ${C_INCLUDE} -MF "$@.d" -o ${OBJECTDIR}/Src/MacAddr.o Src/MacAddr.cpp

${OBJECTDIR}/Src/Main.o: Src/Main.cpp 
	${MKDIR} -p ${OBJECTDIR}/Src
	${RM} "$@.d"
	${CXX} ${CFLAGS} ${C_INCLUDE} -MF "$@.d" -o ${OBJECTDIR}/Src/Main.o Src/Main.cpp

${OBJECTDIR}/Src/Network/Network.o: Src/Network/Network.cpp
	${MKDIR} -p ${OBJECTDIR}/Src/Network
	${RM} "$@.d"
	${CXX} ${CFLAGS} ${C_INCLUDE} -MF "$@.d" -o ${OBJECTDIR}/Src/Network/Network.o Src/Network/Network.cpp
	
${OBJECTDIR}/Src/Network/Network_CB.o: Src/Network/Network_CB.cpp
	${MKDIR} -p ${OBJECTDIR}/Src/Network
	${RM} "$@.d"
	${CXX} ${CFLAGS} ${C_INCLUDE} -MF "$@.d" -o ${OBJECTDIR}/Src/Network/Network_CB.o Src/Network/Network_CB.cpp
	
${OBJECTDIR}/Src/Network/Network_Private.o: Src/Network/Network_Private.cpp
	${MKDIR} -p ${OBJECTDIR}/Src/Network
	${RM} "$@.d"
	${CXX} ${CFLAGS} ${C_INCLUDE} -MF "$@.d" -o ${OBJECTDIR}/Src/Network/Network_Private.o Src/Network/Network_Private.cpp	

${OBJECTDIR}/Src/Network/NetworkDevice.o: Src/Network/NetworkDevice.cpp 
	${MKDIR} -p ${OBJECTDIR}/Src/Network
	${RM} "$@.d"
	${CXX} ${CFLAGS} ${C_INCLUDE} -MF "$@.d" -o ${OBJECTDIR}/Src/Network/NetworkDevice.o Src/Network/NetworkDevice.cpp

${OBJECTDIR}/Src/Network/NetworkDeviceListener.o: Src/Network/NetworkDeviceListener.cpp 
	${MKDIR} -p ${OBJECTDIR}/Src/Network
	${RM} "$@.d"
	${CXX} ${CFLAGS} ${C_INCLUDE} -MF "$@.d" -o ${OBJECTDIR}/Src/Network/NetworkDeviceListener.o Src/Network/NetworkDeviceListener.cpp

${OBJECTDIR}/Src/Network/NodeDatabase.o: Src/Network/NodeDatabase.cpp 
	${MKDIR} -p ${OBJECTDIR}/Src/Network
	${RM} "$@.d"
	${CXX} ${CFLAGS} ${C_INCLUDE} -MF "$@.d" -o ${OBJECTDIR}/Src/Network/NodeDatabase.o Src/Network/NodeDatabase.cpp
	
${OBJECTDIR}/Src/Network/IndustrialStack.o: Src/Network/IndustrialStack.cpp 
	${MKDIR} -p ${OBJECTDIR}/Src/Network
	${RM} "$@.d"
	${CXX} ${CFLAGS} ${C_INCLUDE} -MF "$@.d" -o ${OBJECTDIR}/Src/Network/IndustrialStack.o Src/Network/IndustrialStack.cpp

${OBJECTDIR}/Src/Network/IndustrialStack_LLD.o: Src/Network/IndustrialStack_LLD.cpp 
	${MKDIR} -p ${OBJECTDIR}/Src/Network
	${RM} "$@.d"
	${CXX} ${CFLAGS} ${C_INCLUDE} -MF "$@.d" -o ${OBJECTDIR}/Src/Network/IndustrialStack_LLD.o Src/Network/IndustrialStack_LLD.cpp
	
${OBJECTDIR}/Src/Network/IndustrialStack_MNS.o: Src/Network/IndustrialStack_MNS.cpp 
	${MKDIR} -p ${OBJECTDIR}/Src/Network
	${RM} "$@.d"
	${CXX} ${CFLAGS} ${C_INCLUDE} -MF "$@.d" -o ${OBJECTDIR}/Src/Network/IndustrialStack_MNS.o Src/Network/IndustrialStack_MNS.cpp

${OBJECTDIR}/Src/Network/base/Board.o: Src/Network/base/Board.c 
	${MKDIR} -p ${OBJECTDIR}/Src/Network/base
	${RM} "$@.d"
	${CC} ${CFLAGS} ${C_INCLUDE} -MF "$@.d" -o ${OBJECTDIR}/Src/Network/base/Board.o Src/Network/base/Board.c

${OBJECTDIR}/Src/Network/base/DriverConfiguration.o: Src/Network/base/DriverConfiguration.c 
	${MKDIR} -p ${OBJECTDIR}/Src/Network/base
	${RM} "$@.d"
	${CC} ${CFLAGS} ${C_INCLUDE} -MF "$@.d" -o ${OBJECTDIR}/Src/Network/base/DriverConfiguration.o Src/Network/base/DriverConfiguration.c

${OBJECTDIR}/Src/Thread.o: Src/Thread.cpp 
	${MKDIR} -p ${OBJECTDIR}/Src
	${RM} "$@.d"
	${CXX} ${CFLAGS} ${C_INCLUDE} -MF "$@.d" -o ${OBJECTDIR}/Src/Thread.o Src/Thread.cpp

${OBJECTDIR}/Src/ScriptXml.o: Src/ScriptXml.cpp 
	${MKDIR} -p ${OBJECTDIR}/Src
	${RM} "$@.d"
	${CXX} ${CFLAGS} ${C_INCLUDE} -MF "$@.d" -o ${OBJECTDIR}/Src/ScriptXml.o Src/ScriptXml.cpp

${OBJECTDIR}/Src/VodXml.o: Src/VodXml.cpp 
	${MKDIR} -p ${OBJECTDIR}/Src
	${RM} "$@.d"
	${CXX} ${CFLAGS} ${C_INCLUDE} -MF "$@.d" -o ${OBJECTDIR}/Src/VodXml.o Src/VodXml.cpp

${OBJECTDIR}/Src/Xml.o: Src/Xml.cpp 
	${MKDIR} -p ${OBJECTDIR}/Src
	${RM} "$@.d"
	${CXX} ${CFLAGS} ${C_INCLUDE} -MF "$@.d" -o ${OBJECTDIR}/Src/Xml.o Src/Xml.cpp

${OBJECTDIR}/mnsl/mns_alm.o: mnsl/mns_alm.c 
	${MKDIR} -p ${OBJECTDIR}/mnsl
	${RM} "$@.d"
	${CC} ${CFLAGS} ${C_INCLUDE} -MF "$@.d" -o ${OBJECTDIR}/mnsl/mns_alm.o mnsl/mns_alm.c

${OBJECTDIR}/mnsl/mns_ams.o: mnsl/mns_ams.c 
	${MKDIR} -p ${OBJECTDIR}/mnsl
	${RM} "$@.d"
	${CC} ${CFLAGS} ${C_INCLUDE} -MF "$@.d" -o ${OBJECTDIR}/mnsl/mns_ams.o mnsl/mns_ams.c

${OBJECTDIR}/mnsl/mns_amsmessage.o: mnsl/mns_amsmessage.c 
	${MKDIR} -p ${OBJECTDIR}/mnsl
	${RM} "$@.d"
	${CC} ${CFLAGS} ${C_INCLUDE} -MF "$@.d" -o ${OBJECTDIR}/mnsl/mns_amsmessage.o mnsl/mns_amsmessage.c

${OBJECTDIR}/mnsl/mns_amspool.o: mnsl/mns_amspool.c 
	${MKDIR} -p ${OBJECTDIR}/mnsl
	${RM} "$@.d"
	${CC} ${CFLAGS} ${C_INCLUDE} -MF "$@.d" -o ${OBJECTDIR}/mnsl/mns_amspool.o mnsl/mns_amspool.c

${OBJECTDIR}/mnsl/mns_base.o: mnsl/mns_base.c 
	${MKDIR} -p ${OBJECTDIR}/mnsl
	${RM} "$@.d"
	${CC} ${CFLAGS} ${C_INCLUDE} -MF "$@.d" -o ${OBJECTDIR}/mnsl/mns_base.o mnsl/mns_base.c

${OBJECTDIR}/mnsl/mns_dl.o: mnsl/mns_dl.c 
	${MKDIR} -p ${OBJECTDIR}/mnsl
	${RM} "$@.d"
	${CC} ${CFLAGS} ${C_INCLUDE} -MF "$@.d" -o ${OBJECTDIR}/mnsl/mns_dl.o mnsl/mns_dl.c

${OBJECTDIR}/mnsl/mns_eh.o: mnsl/mns_eh.c 
	${MKDIR} -p ${OBJECTDIR}/mnsl
	${RM} "$@.d"
	${CC} ${CFLAGS} ${C_INCLUDE} -MF "$@.d" -o ${OBJECTDIR}/mnsl/mns_eh.o mnsl/mns_eh.c

${OBJECTDIR}/mnsl/mns_encoder.o: mnsl/mns_encoder.c 
	${MKDIR} -p ${OBJECTDIR}/mnsl
	${RM} "$@.d"
	${CC} ${CFLAGS} ${C_INCLUDE} -MF "$@.d" -o ${OBJECTDIR}/mnsl/mns_encoder.o mnsl/mns_encoder.c

${OBJECTDIR}/mnsl/mns_lldpool.o: mnsl/mns_lldpool.c 
	${MKDIR} -p ${OBJECTDIR}/mnsl
	${RM} "$@.d"
	${CC} ${CFLAGS} ${C_INCLUDE} -MF "$@.d" -o ${OBJECTDIR}/mnsl/mns_lldpool.o mnsl/mns_lldpool.c

${OBJECTDIR}/mnsl/mns_message.o: mnsl/mns_message.c 
	${MKDIR} -p ${OBJECTDIR}/mnsl
	${RM} "$@.d"
	${CC} ${CFLAGS} ${C_INCLUDE} -MF "$@.d" -o ${OBJECTDIR}/mnsl/mns_message.o mnsl/mns_message.c

${OBJECTDIR}/mnsl/mns_misc.o: mnsl/mns_misc.c 
	${MKDIR} -p ${OBJECTDIR}/mnsl
	${RM} "$@.d"
	${CC} ${CFLAGS} ${C_INCLUDE} -MF "$@.d" -o ${OBJECTDIR}/mnsl/mns_misc.o mnsl/mns_misc.c

${OBJECTDIR}/mnsl/mns_obs.o: mnsl/mns_obs.c 
	${MKDIR} -p ${OBJECTDIR}/mnsl
	${RM} "$@.d"
	${CC} ${CFLAGS} ${C_INCLUDE} -MF "$@.d" -o ${OBJECTDIR}/mnsl/mns_obs.o mnsl/mns_obs.c

${OBJECTDIR}/mnsl/mns_pmchannel.o: mnsl/mns_pmchannel.c 
	${MKDIR} -p ${OBJECTDIR}/mnsl
	${RM} "$@.d"
	${CC} ${CFLAGS} ${C_INCLUDE} -MF "$@.d" -o ${OBJECTDIR}/mnsl/mns_pmchannel.o mnsl/mns_pmchannel.c

${OBJECTDIR}/mnsl/mns_pmfifo.o: mnsl/mns_pmfifo.c 
	${MKDIR} -p ${OBJECTDIR}/mnsl
	${RM} "$@.d"
	${CC} ${CFLAGS} ${C_INCLUDE} -MF "$@.d" -o ${OBJECTDIR}/mnsl/mns_pmfifo.o mnsl/mns_pmfifo.c

${OBJECTDIR}/mnsl/mns_pmp.o: mnsl/mns_pmp.c 
	${MKDIR} -p ${OBJECTDIR}/mnsl
	${RM} "$@.d"
	${CC} ${CFLAGS} ${C_INCLUDE} -MF "$@.d" -o ${OBJECTDIR}/mnsl/mns_pmp.o mnsl/mns_pmp.c

${OBJECTDIR}/mnsl/mns_pool.o: mnsl/mns_pool.c 
	${MKDIR} -p ${OBJECTDIR}/mnsl
	${RM} "$@.d"
	${CC} ${CFLAGS} ${C_INCLUDE} -MF "$@.d" -o ${OBJECTDIR}/mnsl/mns_pool.o mnsl/mns_pool.c

${OBJECTDIR}/mnsl/mns_scheduler.o: mnsl/mns_scheduler.c 
	${MKDIR} -p ${OBJECTDIR}/mnsl
	${RM} "$@.d"
	${CC} ${CFLAGS} ${C_INCLUDE} -MF "$@.d" -o ${OBJECTDIR}/mnsl/mns_scheduler.o mnsl/mns_scheduler.c

${OBJECTDIR}/mnsl/mns_segmentation.o: mnsl/mns_segmentation.c 
	${MKDIR} -p ${OBJECTDIR}/mnsl
	${RM} "$@.d"
	${CC} ${CFLAGS} ${C_INCLUDE} -MF "$@.d" -o ${OBJECTDIR}/mnsl/mns_segmentation.o mnsl/mns_segmentation.c

${OBJECTDIR}/mnsl/mns_timer.o: mnsl/mns_timer.c 
	${MKDIR} -p ${OBJECTDIR}/mnsl
	${RM} "$@.d"
	${CC} ${CFLAGS} ${C_INCLUDE} -MF "$@.d" -o ${OBJECTDIR}/mnsl/mns_timer.o mnsl/mns_timer.c

${OBJECTDIR}/mnsl/mns_telqueue.o: mnsl/mns_telqueue.c 
	${MKDIR} -p ${OBJECTDIR}/mnsl
	${RM} "$@.d"
	${CC} ${CFLAGS} ${C_INCLUDE} -MF "$@.d" -o ${OBJECTDIR}/mnsl/mns_telqueue.o mnsl/mns_telqueue.c
${OBJECTDIR}/mnsl/mns_transceiver.o: mnsl/mns_transceiver.c 
	${MKDIR} -p ${OBJECTDIR}/mnsl
	${RM} "$@.d"
	${CC} ${CFLAGS} ${C_INCLUDE} -MF "$@.d" -o ${OBJECTDIR}/mnsl/mns_transceiver.o mnsl/mns_transceiver.c

${OBJECTDIR}/mnsl/mns_pmcmd.o: mnsl/mns_pmcmd.c 
	${MKDIR} -p ${OBJECTDIR}/mnsl
	${RM} "$@.d"
	${CC} ${CFLAGS} ${C_INCLUDE} -MF "$@.d" -o ${OBJECTDIR}/mnsl/mns_pmcmd.o mnsl/mns_pmcmd.c

${OBJECTDIR}/mnsl/mns_pmfifos.o: mnsl/mns_pmfifos.c 
	${MKDIR} -p ${OBJECTDIR}/mnsl
	${RM} "$@.d"
	${CC} ${CFLAGS} ${C_INCLUDE} -MF "$@.d" -o ${OBJECTDIR}/mnsl/mns_pmfifos.o mnsl/mns_pmfifos.c

${OBJECTDIR}/mnsl/mnsl.o: mnsl/mnsl.c 
	${MKDIR} -p ${OBJECTDIR}/mnsl
	${RM} "$@.d"
	${CC} ${CFLAGS} ${C_INCLUDE} -MF "$@.d" -o ${OBJECTDIR}/mnsl/mnsl.o mnsl/mnsl.c

# Subprojects
.build-subprojects:

# Clean Targets
.clean-conf: ${CLEAN_SUBPROJECTS}
	${RM} -r ${CND_BUILDDIR}/${CND_CONF}
	${RM} NetworkManager

# Subprojects
.clean-subprojects:

# Enable dependency checking
.dep.inc: .depcheck-impl

include .dep.inc
