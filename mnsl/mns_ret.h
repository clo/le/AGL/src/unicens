/*
 * MOST NetServices "Light" V3.2.7.0.1796 MultiInstance Patch
 *
 * Copyright (C) 2015 Microchip Technology Germany II GmbH & Co. KG
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * You may also obtain this software under a propriety license from Microchip.
 * Please contact Microchip for further information.
 *
 */

/*!
 * \file
 * \brief This header file contains standard return values used by MOST NetServices functions 
 *        and methods.
 * \addtogroup G_MNS_MISC_RET_RES
 * @{
 */

#ifndef MNS_RET_H
#define MNS_RET_H

#ifdef __cplusplus
extern "C"
{
#endif

/*------------------------------------------------------------------------------------------------*/
/* Enumerations                                                                                   */
/*------------------------------------------------------------------------------------------------*/
/*! \brief Standard return codes used for synchronous response */
typedef enum Mns_Return_
{
    MNS_RET_SUCCESS             = 0x00,     /*!< \brief Operation successfully completed */
    MNS_RET_ERR_PARAM           = 0x01,     /*!< \brief At least one parameter exceeds its 
                                                        admissible range */
    MNS_RET_ERR_BUFFER_OVERFLOW = 0x02,     /*!< \brief Buffer overflow or service busy */
    MNS_RET_ERR_NOT_AVAILABLE   = 0x03,     /*!< \brief Functionality not available */
    MNS_RET_ERR_NOT_SUPPORTED   = 0x04,     /*!< \brief This function is not supported by this 
                                                        derivative of INIC / physical layer / MOST 
                                                        speed */
    MNS_RET_ERR_INVALID_SHADOW  = 0x05,     /*!< \brief The requested information is not yet 
                                                        available */
    MNS_RET_ERR_ALREADY_SET     = 0x06,     /*!< \brief The value to be set is already set. The 
                                                        application can therefore be aware that no 
                                                        message will be send to INIC and no 
                                                        callback will be called */
    MNS_RET_ERR_API_LOCKED      = 0x07,     /*!< \brief INIC performs already requested function. */
    MNS_RET_ERR_NOT_INITIALIZED = 0x08      /*!< \brief MOST NetServices is not initialized */

} Mns_Return_t;

/*! \brief Result codes used for asynchronous response */
typedef enum Mns_Result_
{
    MNS_RES_SUCCESS           = 0x00,       /*!< \brief Operation successfully completed */
    MNS_RES_ERR_MOST_STANDARD = 0x01,       /*!< \brief MOST standard error occurred */
    MNS_RES_ERR_BUSY          = 0x02,       /*!< \brief Function currently busy */
    MNS_RES_ERR_PROCESSING    = 0x03,       /*!< \brief Processing error occurred */
    MNS_RES_ERR_CONFIGURATION = 0x04,       /*!< \brief Configuration error occurred */
    MNS_RES_ERR_SYSTEM        = 0x05,       /*!< \brief System error occurred */
    MNS_RES_ERR_TIMEOUT       = 0x06,       /*!< \brief Timeout occurred */
    MNS_RES_ERR_TRANSMISSION  = 0x07        /*!< \brief Transmission error occurred */

} Mns_Result_t;

/*------------------------------------------------------------------------------------------------*/
/* Structures                                                                                     */
/*------------------------------------------------------------------------------------------------*/
/*! \brief  Standard result structure which provides fields for detailed status and 
 *          error information
 *  \details \mns_ic_started{ The \ref P_UM_SYNC_AND_ASYNC_RESULTS section in \c Getting \c Started will provide you with more detailed information concerning the info pointer and the error code. }
 */
typedef struct Mns_StdResult_
{
    Mns_Result_t code;  /*!< \brief Result/Error code */
    uint8_t *info_ptr;  /*!< \brief INIC error data */
    uint8_t info_size;  /*!< \brief Size of the INIC error data in bytes */

} Mns_StdResult_t;

/*------------------------------------------------------------------------------------------------*/
/* Types                                                                                          */
/*------------------------------------------------------------------------------------------------*/
/*! \brief  Function signature used for MOST NetServices standard result callbacks
 *  \param  result  Result of the callback
 */
typedef void (*Mns_StdResultCb_t)(Mns_StdResult_t result);

#ifdef __cplusplus
}   /* extern "C" */
#endif

#endif  /* #ifndef MNS_RET_H */

/*!
 * @}
 */

/*------------------------------------------------------------------------------------------------*/
/* End of file                                                                                    */
/*------------------------------------------------------------------------------------------------*/

