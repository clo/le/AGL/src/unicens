/*
 * MOST NetServices "Light" V3.2.7.0.1796 MultiInstance Patch
 *
 * Copyright (C) 2015 Microchip Technology Germany II GmbH & Co. KG
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * You may also obtain this software under a propriety license from Microchip.
 * Please contact Microchip for further information.
 *
 */

/* parasoft suppress item * reason "not part of MOST NetServices delivery" */

/*!
 * \file
 * \brief Header file of MOST NetServices Light
 */

#ifndef MNSL_H
#define MNSL_H

#ifdef __cplusplus
extern "C"
{
#endif

/*------------------------------------------------------------------------------------------------*/
/* Includes                                                                                       */
/*------------------------------------------------------------------------------------------------*/
#include "mns_transceiver.h"
#include "mns_ams.h"
#include "mns_pmfifos.h"

/*------------------------------------------------------------------------------------------------*/
/* Definitions                                                                                    */
/*------------------------------------------------------------------------------------------------*/
/*! \brief MNS Major Version Number */
#define MNSL_VERSION_MAJOR   3
/*! \brief MNS Minor Version Number */
#define MNSL_VERSION_MINOR   2
/*! \brief MNS Release Version Number */
#define MNSL_VERSION_RELEASE 7
/*! \brief MNS Service Release Number */
#define MNSL_VERSION_SR      0
/*! \brief MNS Build Number */
#define MNSL_VERSION_BUILD   1796

/*------------------------------------------------------------------------------------------------*/
/* Types                                                                                          */
/*------------------------------------------------------------------------------------------------*/
/*! \brief Defines which FIFOs shall be built on the Port Message Channel */
typedef enum Mnsl_ActiveFifos_
{
    MNSL_FIFOS_MCM         = 0x01U,     /*!< \brief Initialize PM channel with MCM FIFO only */
    MNSL_FIFOS_ICM         = 0x02U,     /*!< \brief Initialize PM channel with ICM FIFO only */
    MNSL_FIFOS_MCM_ICM     = 0x03U,     /*!< \brief Initialize PM channel with MCM and ICM FIFO */
    MNSL_FIFOS_RCM         = 0x04U,     /*!< \brief Initialize PM channel with RCM FIFO only */
    MNSL_FIFOS_MCM_RCM     = 0x05U,     /*!< \brief Initialize PM channel with MCM and RCM FIFO */
    MNSL_FIFOS_ICM_RCM     = 0x06U,     /*!< \brief Initialize PM channel with ICM and RCM FIFO */
    MNSL_FIFOS_MCM_ICM_RCM = 0x07U      /*!< \brief Initialize PM channel with MCM, ICM and RCM FIFO */

} Mnsl_ActiveFifos_t;

/*! \brief Possible MNSL events */
typedef enum Mnsl_Event_
{
    MNSL_EVENT_SYNC_COMPLETE    = 0,    /*!< \brief The initial synchronization completed successfully */
    MNSL_EVENT_SYNC_FAILED      = 1,    /*!< \brief The initial synchronization failed */
    MNSL_EVENT_SYNC_LOST        = 2,    /*!< \brief The synchronization to the INIC was lost irrecoverably */
    MNSL_EVENT_UNSYNC_COMPLETE  = 3,    /*!< \brief The un-synchronization was completed successfully */
    MNSL_EVENT_UNSYNC_FAILED    = 4     /*!< \brief The un-synchronization failed */

} Mnsl_Event_t;

typedef uint16_t (*Mnsl_GetTickCountCb_t)(void);
typedef void (*Mnsl_EventCb_t)(Mnsl_Event_t event_code, void *inst_ptr);
typedef void (*Mnsl_ServiceRequestCb_t)(void *inst_ptr);
typedef void (*Mnsl_SetAppTimerCb_t)(uint16_t timeout, void *inst_ptr);

/*! \brief General initialization settings */
typedef struct Mnsl_General_InitData_
{
    Mnsl_EventCb_t          event_fptr;         /*!< \brief Reports an MNSL event (mandatory callback) */
    Mnsl_GetTickCountCb_t   get_tickcount_fptr; /*!< \brief Requests the current tick count in milliseconds (mandatory callback) */
    Mnsl_ServiceRequestCb_t request_service_fptr;/*!< \brief Reports that the application shall call Mnsl_Service() (optional callback) */
    Mnsl_SetAppTimerCb_t    set_app_timer_fptr; /*!< \brief Triggers the application to call Mnsl_ReportTimeout() after a specific time (mandatory callback) */

} Mnsl_General_InitData_t;

/*! \brief Port Message Service (PMS) settings */
typedef struct Mnsl_Pms_InitData_
{
    Mnsl_ActiveFifos_t  active_fifos;           /*!< \brief Defines which FIFOs shall be initialized */
    Fifo_Config_t       icm_config;             /*!< \brief Setup of ICM FIFO */
    Fifo_Config_t       mcm_config;             /*!< \brief Setup of MCM FIFO */
    Fifo_Config_t       rcm_config;             /*!< \brief Setup of RCM FIFO */
    bool                compressed;             /*!< \brief Set to \c true in order to use OS81118 Rev.C */

} Mnsl_Pms_InitData_t;

/*! \brief Application Message Service (AMS) settings */
typedef struct Mnsl_Ams_InitData_
{
    Mns_Ams_AllocMemCb_t rx_alloc_mem_fptr;     /*!< \brief Required to allocate memory for an AMS Rx message */
    Mns_Ams_FreeMemCb_t rx_free_mem_fptr;       /*!< \brief Required to free memory for an AMS Rx message */

} Mnsl_Ams_InitData_t;

/*! \brief Initialization settings */
typedef struct Mnsl_InitData_
{
    Mnsl_General_InitData_t general;    /*!< \brief Basic settings to drive MNSL */
    Mns_Lld_Callbacks_t     lld;        /*!< \brief Mandatory callback functions of the driver */
    Mnsl_Pms_InitData_t     pms;        /*!< \brief Port Message Service settings */
    Mnsl_Ams_InitData_t     ams;        /*!< \brief Application Message Service settings */    

} Mnsl_InitData_t;

/*------------------------------------------------------------------------------------------------*/
/* Helper Macros                                                                                  */
/*------------------------------------------------------------------------------------------------*/
#define MNSL_UNUSED(p)   ((p) = (p)) /* parasoft-suppress  MISRA2004-19_7 "suppress warning" */

/*------------------------------------------------------------------------------------------------*/
/* Internal structures                                                                            */
/*------------------------------------------------------------------------------------------------*/
typedef struct CMnsl_
{
    uint8_t inst_id;
    CBase base;
    CPmChannel pm_channel;
    CPmFifos fifos;
    CPmFifo icm_fifo;
    CPmFifo rcm_fifo;
    CPmFifo mcm_fifo;

    Mnsl_GetTickCountCb_t get_tick_count_fptr;
    CSingleObserver get_tick_count_obs;

    Mnsl_ServiceRequestCb_t srv_request_fptr;
    CSingleObserver srv_request_obs;

    Mnsl_SetAppTimerCb_t set_app_timer_fptr;
    CSingleObserver set_app_timer_obs;

    CTransceiver icm_transceiver;
    CTransceiver rcm_transceiver;
    CTransceiver mcm_transceiver;
    CAms ams;
    CAmsMsgPool ams_pool;
    Ams_MemAllocator_t ams_allocator;

    Mnsl_EventCb_t  event_fptr;
    CObserver       pms_observer;
    
    void *inst_ptr; //TKU: added

} CMnsl;

/*------------------------------------------------------------------------------------------------*/
/* Prototypes                                                                                     */
/*------------------------------------------------------------------------------------------------*/
extern void Mnsl_SetDefaultConfig(Mnsl_InitData_t *init_ptr, bool enable_watchdog);
extern void Mnsl_Init(CMnsl *mnsl, Mnsl_InitData_t *init_ptr, void *inst_ptr);
extern void Mnsl_Synchronize(CMnsl *mnsl);
extern void Mnsl_Unsynchronize(CMnsl *mnsl, bool initial);
extern void Mnsl_Service(CMnsl *mnsl);
extern void Mnsl_ReportTimeout(CMnsl *mnsl);
extern CTransceiver * Mnsl_GetIcmTransceiver(CMnsl *mnsl);
extern CTransceiver * Mnsl_GetRcmTransceiver(CMnsl *mnsl);
extern CTransceiver * Mnsl_GetMcmTransceiver(CMnsl *mnsl);
extern CAms         * Mnsl_GetAmsTransceiver(CMnsl *mnsl);

#ifdef __cplusplus
}   /* extern "C" */
#endif

#endif  /* #ifndef MNSL_H */

/*------------------------------------------------------------------------------------------------*/
/* End of file                                                                                    */
/*------------------------------------------------------------------------------------------------*/

