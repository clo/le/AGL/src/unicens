/*
 * MOST NetServices "Light" V3.2.7.0.1796 MultiInstance Patch
 *
 * Copyright (C) 2015 Microchip Technology Germany II GmbH & Co. KG
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * You may also obtain this software under a propriety license from Microchip.
 * Please contact Microchip for further information.
 *
 */

/*!
 * \file
 * \brief Public header file of Application Message Service
 */
/*!
 * \addtogroup G_MNS_AMS_TYPES
 * @{
 */

#ifndef MNS_AMS_PB_H
#define MNS_AMS_PB_H

/*------------------------------------------------------------------------------------------------*/
/* Includes                                                                                       */
/*------------------------------------------------------------------------------------------------*/
#include "mns_cfg.h"
#include "mns_message_pb.h"

#ifdef __cplusplus
extern "C"
{
#endif

/*! \brief   Defines which address type was used by the transmitter of a message. */
typedef enum Mns_AmsRx_ReceiveType_
{
    MNS_AMSRX_RCT_SINGLECAST    = 0U,           /*!< \brief Message was transmitted as singlecast */
    MNS_AMSRX_RCT_GROUPCAST     = 1U,           /*!< \brief Message was transmitted as groupcast */
    MNS_AMSRX_RCT_BROADCAST     = 2U            /*!< \brief Message was transmitted as broadcast */

} Mns_AmsRx_ReceiveType_t;

/*------------------------------------------------------------------------------------------------*/
/* Types                                                                                          */
/*------------------------------------------------------------------------------------------------*/
/*! \brief Application message Tx type */
typedef struct Mns_AmsTx_Msg_
{
    uint16_t        destination_address;        /*!< \brief   Destination address. Find some predefined addresses \ref G_MNS_AMS "here". */
    uint8_t         fblock_id;                  /*!< \brief   Function block ID (MOST FBlockID). */
    uint8_t         instance_id;                /*!< \brief   Instance ID (MOST InstID). */
    uint16_t        function_id;                /*!< \brief   Function ID (MOST FktID). */
    Mns_OpType_t    op_type;                    /*!< \brief   Operation type (MOST OpType). */
    uint8_t         llrbc;                      /*!< \brief   Specifies the "Low-Level Retry Block Count" (LLRBC)
                                                 *   \details Valid values: 0..100. Default value: configurable via \ref Mns_AmsTx_InitData_t "default_llrbc"
                                                 *            of the initialization structure \ref Mns_AmsTx_InitData_t.
                                                 *            \mns_ic_inic{ See also <i>INIC API User's Guide</i>, section \ref SEC_OS81118_19. }
                                                 */
    uint8_t        *data_ptr;                   /*!< \brief   Payload data */
    uint16_t        data_size;                  /*!< \brief   The size of payload data in bytes */
    void           *custom_info_ptr;            /*!< \brief   Customer specific reference 
                                                 *   \details The application is allowed to use this attribute to assign an
                                                 *            own reference to the message object. The reference is initialized 
                                                 *            by MOST NetServices with \c NULL and will not alter until the 
                                                 *            transmission has finished.
                                                 */
} Mns_AmsTx_Msg_t;

/*! \brief Application message Rx type */
typedef struct Mns_AmsRx_Msg_
{
    uint16_t        source_address;             /*!< \brief Source address */
    uint8_t         fblock_id;                  /*!< \brief Function block ID (MOST FBlockID). */
    uint8_t         instance_id;                /*!< \brief Instance ID (MOST InstID). */
    uint16_t        function_id;                /*!< \brief Function ID (MOST FktID). */
    Mns_OpType_t    op_type;                    /*!< \brief Operation type (MOST OpType). */
    uint8_t        *data_ptr;                   /*!< \brief Reference to payload */
    uint16_t        data_size;                  /*!< \brief Payload size in bytes */
    void           *custom_info_ptr;            /*!< \brief Customer specific reference */
    Mns_AmsRx_ReceiveType_t receive_type;       /*!< \brief Defines which address type was used by the transmitter of this message */

} Mns_AmsRx_Msg_t;

/*! \brief Transmission result of an application message */
typedef enum Mns_AmsTx_Result_
{
    MNS_AMSTX_RES_SUCCESS             = 0x00U,/*!< \brief   The transmission succeeded. */

    MNS_AMSTX_RES_ERR_RETRIES_EXP     = 0x01U,/*!< \brief   The transmission including all retries have failed.
                                               *   \details The following issues may have caused the failure:
                                               *            - message corruption
                                               *            - transmission timeouts
                                               *            - full receive buffers of the destination device
                                               *            - full receive buffers of the local device if the
                                               *              destination was the own address, own group or broadcast
                                               *              address
                                               *            .
                                               */
    MNS_AMSTX_RES_ERR_INVALID_TGT     = 0x02U,/*!< \brief   The transmission failed because the specified destination 
                                               *            address is not found or not valid.
                                               *   \details The following issues may have caused the failure:
                                               *            - device with the given destination address is not found 
                                               *            - destination address is reserved (for future use) 
                                               *            - destination address is 0xFFFF (un-initialized logical 
                                               *              node address is not supported)
                                               *            .
                                               */
    MNS_AMSTX_RES_ERR_NOT_AVAILABLE   = 0x03U,/*!< \brief   The transmission failed since the network or the INIC
                                               *            is not available.
                                               */
    MNS_AMSTX_RES_ERR_BUF_INTERNAL    = 0xFEU,/*!< \brief   The transmission failed because the allocation of an Rx message object failed.
                                               *            The Rx message object is required to receive the message via the own Rx message queue.
                                               *   \details This is possible in the following cases:
                                               *            - A message is transmitted to the own node address and the allocation 
                                               *              of an Rx message object failed.
                                               *            - The network transmission to the own group address or broadcast address
                                               *              succeeded but the allocation of an Rx message object failed. The application
                                               *              has to decide whether to retransmit the message to the own address again.
                                               */
    MNS_AMSTX_RES_ERR_UNEXPECTED      = 0xFFU /*!< \brief   The transmission failed due to an unexpected error.
                                               *            The cause of this failure may be an invalid INIC configuration,
                                               *            or an INIC to MOST NetServices incompatibility issue.
                                               */
} Mns_AmsTx_Result_t;


/*! \brief  Detailed INIC transmission information which might be useful for debugging purposes. */
typedef enum Mns_AmsTx_Info_
{
    MNS_AMSTX_I_SUCCESS             = 0x00U, /*!< \brief   The transmission succeeded.
                                              *   \details The corresponding transmission result is \ref MNS_AMSTX_RES_SUCCESS. 
                                              */
    MNS_AMSTX_I_ERR_CFG_NORECEIVER  = 0x01U, /*!< \brief   The transmission failed because the MOST network is not accessible for
                                              *            MCM in the current attach state or for ICM in general.
                                              *   \details The corresponding transmission result is \ref MNS_AMSTX_RES_ERR_UNEXPECTED. 
                                              */
    MNS_AMSTX_I_ERR_BF              = 0x08U, /*!< \brief   The transmission failed because the receivers buffer is full.
                                              *   \details The corresponding transmission result is \ref MNS_AMSTX_RES_ERR_RETRIES_EXP. 
                                              */ 
    MNS_AMSTX_I_ERR_CRC             = 0x09U, /*!< \brief   The transmission failed because of a failed CRC.
                                              *   \details The corresponding transmission result is \ref MNS_AMSTX_RES_ERR_RETRIES_EXP. 
                                              */ 
    MNS_AMSTX_I_ERR_ID              = 0x0AU, /*!< \brief   The transmission failed because of corrupted identifiers.
                                              *   \details The corresponding transmission result is \ref MNS_AMSTX_RES_ERR_RETRIES_EXP. 
                                              */ 
    MNS_AMSTX_I_ERR_ACK             = 0x0BU, /*!< \brief   The transmission failed because of corrupted PACK or CACK.
                                              *   \details The corresponding transmission result is \ref MNS_AMSTX_RES_ERR_RETRIES_EXP. 
                                              */ 
    MNS_AMSTX_I_ERR_TIMEOUT         = 0x0CU, /*!< \brief   The transmission failed because of a transmission timeout.
                                              *   \details The corresponding transmission result is \ref MNS_AMSTX_RES_ERR_RETRIES_EXP. 
                                              */
    MNS_AMSTX_I_ERR_FATAL_WT        = 0x10U, /*!< \brief   The transmission failed because of destination is not available.
                                              *   \details The corresponding transmission result is \ref MNS_AMSTX_RES_ERR_INVALID_TGT. 
                                              */ 
    MNS_AMSTX_I_ERR_FATAL_OA        = 0x11U, /*!< \brief   The transmission failed because of the destination is the own node address. 
                                              *   \details The corresponding transmission result is \ref MNS_AMSTX_RES_ERR_INVALID_TGT. 
                                              */
    MNS_AMSTX_I_ERR_UNAVAIL_TRANS   = 0x18U, /*!< \brief   The transmission canceled during the transition from network interface state
                                              *            "available" to "not available".
                                              *   \details The corresponding transmission result is \ref MNS_AMSTX_RES_ERR_NOT_AVAILABLE. 
                                              */ 
    MNS_AMSTX_I_ERR_UNAVAIL_OFF     = 0x19U, /*!< \brief   The transmission failed because the network interface state is "not available".
                                              *   \details The corresponding transmission result is \ref MNS_AMSTX_RES_ERR_NOT_AVAILABLE. 
                                              */
    MNS_AMSTX_I_ERR_UNKNOWN         = 0xFEU, /*!< \brief   The transmission failed because of an unknown INIC error code.
                                              *   \details The corresponding transmission result is \ref MNS_AMSTX_RES_ERR_UNEXPECTED.
                                              *            Please check if the MNS version is compatible with the applied INIC firmware version.
                                              */
    MNS_AMSTX_I_ERR_UNSYNCED        = 0xFFU  /*!< \brief   The transmission failed because the communication between the EHC (MOST NetServices) 
                                              *            and the INIC is lost. 
                                              *   \details The reason can be a communication error between the EHC and the INIC or that 
                                              *            the application has called Mns_Stop().\n
                                              *            The corresponding transmission result is \ref MNS_AMSTX_RES_ERR_NOT_AVAILABLE. 
                                              */
} Mns_AmsTx_Info_t;


/*! \brief  Defines the usage of a requested memory chunk */
typedef enum Mns_Ams_MemUsage_
{
    MNS_AMS_RX_OBJECT,              /*!< \brief  Memory is required to allocate an Rx message object */
    MNS_AMS_RX_PAYLOAD,             /*!< \brief  Memory is required to allocate Rx message payload */
    MNS_AMS_TX_OBJECT,              /*!< \brief  Memory is required to allocate a Tx message object */
    MNS_AMS_TX_PAYLOAD              /*!< \brief  Memory is required to allocate Tx message payload */

} Mns_Ams_MemUsage_t;

/*! \brief  Callback function type that is invoked to allocate external payload for a segmented Rx message
 *  \param  inst_ptr            Reference to the (external) memory management
 *  \param  mem_size            Reference to the required memory size in bytes. Valid values: 0..65535.
 *  \param  type                Declares how the memory is used by the MOST NetServices 
 *  \param  custom_info_pptr    Reference which is related to the memory chunk and can be set by 
 *                              the application.
 *  \return Pointer to the provided memory chunk. The application has to guarantee that the memory size
 *          is equal or greater than \c mem_size. The application has to return \c NULL if it is not able 
 *          to allocate the required memory at this moment.
 */
typedef void* (*Mns_Ams_AllocMemCb_t)(void *inst_ptr, uint16_t mem_size, Mns_Ams_MemUsage_t type, void** custom_info_pptr);

/*! \brief  Callback function type that is invoked to free external payload for a segmented Rx message
 *  \param  inst_ptr        Reference to the (external) memory management
 *  \param  mem_ptr         Reference to the external payload memory
 *  \param  type            Declares how the memory is used by the MOST NetServices 
 *  \param  custom_info_ptr Reference to memory related information which was set by the application 
 *                          during memory allocation
 */
typedef void (*Mns_Ams_FreeMemCb_t)(void *inst_ptr, void *mem_ptr, Mns_Ams_MemUsage_t type, void* custom_info_ptr);

/*! \brief   Type of a callback function that is invoked by the MOST NetServices as soon as a 
 *           message transmission was finished
 *  \details The callback function notifies the result of a completed transmission. If
 *           the message has external payload, the application must decide whether
 *           to re-use or to free the external payload.
 *  \param   msg_ptr Reference to the related Tx message object. When the callback function returns
 *                   the reference is no longer valid.
 *  \param   result  The transmission result.
 *  \param   info    Detailed INIC transmission result, which might be helpful for debug purposes.
 */
typedef void (*Mns_AmsTx_CompleteCb_t)(Mns_AmsTx_Msg_t* msg_ptr, Mns_AmsTx_Result_t result, Mns_AmsTx_Info_t info);

/*!
 * @}
 * \addtogroup G_MNS_AMS
 * @{
 */

/*! \brief  Type of a callback function that is invoked by MOST NetServices to notify that
 *          a Tx application message object is available again while a previous
 *          allocation using Mns_AmsTx_AllocMsg() has failed.
 */
typedef void (*Mns_AmsTx_MsgFreedCb_t)(void);

/*! \brief  Callback function type that is invoked if the MOST NetServices has received a message 
 *          completely and appended to the Rx message queue.
 */
typedef void (*Mns_AmsRx_MsgReceivedCb_t)(void);

#ifdef __cplusplus
}               /* extern "C" */
#endif

#endif  /* ifndef MNS_AMS_PB_H */

/*! @} */

/*------------------------------------------------------------------------------------------------*/
/* End of file                                                                                    */
/*------------------------------------------------------------------------------------------------*/

