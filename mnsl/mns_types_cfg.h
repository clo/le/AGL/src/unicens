/*
 * MOST NetServices "Light" V3.2.7.0.1796 MultiInstance Patch
 *
 * Copyright (C) 2015 Microchip Technology Germany II GmbH & Co. KG
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * You may also obtain this software under a propriety license from Microchip.
 * Please contact Microchip for further information.
 *
 */

/*!
 * \file
 * \brief MOST NetServices data types.
 */

#ifndef MNS_TYPES_H
#define MNS_TYPES_H

/*------------------------------------------------------------------------------------------------*/
/* Includes                                                                                       */
/*------------------------------------------------------------------------------------------------*/
#include <stdint.h>
#include <stdbool.h>
#include <stddef.h>

#ifdef __cplusplus
extern "C"
{
#endif

/*------------------------------------------------------------------------------------------------*/
/* Data Types                                                                                     */
/*------------------------------------------------------------------------------------------------*/
/* Definition of standard integer typed, typically defined in <stdint.h> */
/*   typedef signed char int8_t; */
/*   typedef short int16_t; */
/*   typedef int int32_t; */
/*   typedef unsigned char uint8_t; */
/*   typedef unsigned short uint16_t; */
/*   typedef unsigned int uint32_t; */

/* Definition of size_t, typically defined in <stddef.h> */
/*   typedef uint32_t size_t; */

/* Definition of character type */
     typedef char char_t;

#ifdef __cplusplus
}           /* extern "C" */
#endif

#endif      /* #ifndef MNS_TYPES_H */

/*------------------------------------------------------------------------------------------------*/
/* End of file                                                                                    */
/*------------------------------------------------------------------------------------------------*/

