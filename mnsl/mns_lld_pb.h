/*
 * MOST NetServices "Light" V3.2.7.0.1796 MultiInstance Patch
 *
 * Copyright (C) 2015 Microchip Technology Germany II GmbH & Co. KG
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * You may also obtain this software under a propriety license from Microchip.
 * Please contact Microchip for further information.
 *
 */

/*!
 * \file
 * \brief       Declaration of the low-level driver interface
 * \defgroup    G_LLD Low-Level Driver
 * \brief       API functions to be used by the low-level driver.
 * \details     The MOST NetServices provides a certain set of functions which are only dedicated to the low-level driver.
 *              The low-level driver \em API is a set of functions which shall be used by the low-level driver.
 *              The low-level driver \em callbacks is a set of function that shall be implemented by the low-level driver.
 *              The low-level driver \em callbacks shall be assigned to the MOST NetServices initialization structure.
 *              During initialization MOST NetServices invokes the callback \ref Mns_Lld_Callbacks_t "start_fptr" and
 *              passes the low-level driver \em API as pointer to \ref Mns_Lld_Api_t.
 *              \mns_ic_started{ See also Getting Started with \ref P_UM_STARTED_LLD. }
 *              \mns_ic_examples{ See also <i>Examples</i>, section \ref P_UM_EXAMPLE_LLD_01, \ref P_UM_EXAMPLE_LLD_02 and \ref P_UM_EXAMPLE_LLD_03. }
 * \ingroup     G_MNS_API
 * \defgroup    G_LLD_TYPES Referred Types 
 * \brief       Referred types used by the low-level driver interface
 * \ingroup     G_LLD
 * \defgroup    G_LLD_API Low-Level Driver API
 * \brief       Function pointers to be used by the low-level driver
 * \ingroup     G_LLD
 */

#ifndef MNS_LLD_PB_H
#define MNS_LLD_PB_H

/*------------------------------------------------------------------------------------------------*/
/* Includes                                                                                       */
/*------------------------------------------------------------------------------------------------*/
#include "mns_types_cfg.h"
#include "mns_memory_pb.h"

#ifdef __cplusplus
extern "C"
{
#endif

/*!
 * \addtogroup  G_LLD_TYPES
 * @{
 */

/*------------------------------------------------------------------------------------------------*/
/* Structures                                                                                     */
/*------------------------------------------------------------------------------------------------*/
/*! \brief  Tx message object providing the raw port message byte stream */
typedef struct Mns_Lld_TxMsg_
{
    struct Mns_Lld_TxMsg_ *custom_next_msg_ptr;/*!< \brief     Shall be used by the LLD implementation to queue messages for
                                                *              asynchronous transmission
                                                *   \details   MOST NetServices will set this value to \c NULL since only 
                                                *              single messages are forwarded to the LLD. Within the transmit function 
                                                *              it is recommended that the LLD queues the message for asynchronous 
                                                *              transmission. Despite a driver's transmit function might signal busy for 
                                                *              a short term MOST NetServices might forward multiple messages for 
                                                *              transmission. If a driver works asynchronously (interrupt driven) it
                                                *              can easily use this pointer build a queue of waiting messages. 
                                                *              Nonetheless, it is important that \ref Mns_Lld_Api_t::tx_release_fptr
                                                *              "tx_release_fptr" is invoked for every message separately. The Interface 
                                                *              between MOST NetServices and the LLD does only support single messages.
                                                */
    Mns_Mem_Buffer_t *memory_ptr;              /*!< \brief     Points to the data buffer */

} Mns_Lld_TxMsg_t;

/*! \brief  Rx message object pointing to the raw port message byte stream. */
typedef struct Mns_Lld_RxMsg_
{
    uint8_t*        data_ptr;       /*!< \brief Points to a MOST NetServices allocated memory chunk. */
    uint16_t        data_size;      /*!< \brief Size of the memory chunk in bytes. Valid values: 6..72. */

} Mns_Lld_RxMsg_t;

/*!
 * @} 
 * \addtogroup  G_LLD_API
 * @{
 */

/*------------------------------------------------------------------------------------------------*/
/* Low-level driver API                                                                           */
/*------------------------------------------------------------------------------------------------*/
/*! \brief  Allocates an Rx message object 
 *  \param  inst_ptr    Reference to internal MOST NetServices handler
 *  \param  buffer_size The size in bytes of the received Rx message. 
 *                      Valid values: 6..72.
 *  \return The Rx message object or \c NULL if no message object is available. In the latter
 *          case the low-level driver can wait until Mns_Lld_RxMsgAvailableCb_t() is invoked.
 *          The low-level driver is allowed to pre-allocate Rx messages with the maximum size
 *          of 72 bytes. After writing received data into Mns_Lld_RxMsg_t::data_ptr the
 *          low-level driver must set Mns_Lld_RxMsg_t::data_size to the actual message size.
 *          \warning
 *          The function will also return \c NULL if the requested \c buffer_size exceeds the valid range.
 *          In such a case the MOST NetServices cannot guarantee that Mns_Lld_RxMsgAvailableCb_t() is 
 *          called as expected. Received messages exceeding the valid range must be discarded by the LLD.
 */
typedef Mns_Lld_RxMsg_t* (*Mns_Lld_RxAllocateCb_t)(void *inst_ptr, uint16_t buffer_size);

/*! \brief  Frees an unused Rx message object
 *  \param  inst_ptr    Reference to internal MOST NetServices handler
 *  \param  msg_ptr     Reference to the unused Rx message object
 */
typedef void (*Mns_Lld_RxFreeUnusedCb_t)(void *inst_ptr, Mns_Lld_RxMsg_t *msg_ptr);

/*! \brief  Pass an Rx message to MOST NetServices 
 *  \param  inst_ptr    Reference to internal MOST NetServices handler
 *  \param  msg_ptr     Reference to the Rx message object containing the received
 *                      message.
 */
typedef void (*Mns_Lld_RxReceiveCb_t)(void *inst_ptr, Mns_Lld_RxMsg_t *msg_ptr);

/*! \brief  Notifies that the LLD no longer needs to access the Tx message object
 *  \param  inst_ptr    Reference to internal MOST NetServices handler
 *  \param  msg_ptr     Reference to the Tx message object which is no longer accessed
 *                      by the low-level driver
 */
typedef void (*Mns_Lld_TxReleaseCb_t)(void *inst_ptr, Mns_Lld_TxMsg_t *msg_ptr);

/*! \brief   Initialization required for one communication channel (control or packet)
 */
typedef struct Mns_Lld_Api_
{
    Mns_Lld_RxAllocateCb_t   rx_allocate_fptr;     /*!< \brief  Allocates an Rx message object */
    Mns_Lld_RxFreeUnusedCb_t rx_free_unused_fptr;  /*!< \brief  Frees an unused Rx message object */
    Mns_Lld_RxReceiveCb_t    rx_receive_fptr;      /*!< \brief  Pass an Rx message to MOST NetServices */
    Mns_Lld_TxReleaseCb_t    tx_release_fptr;      /*!< \brief  Notifies that the LLD no longer needs to access the Tx message object */

} Mns_Lld_Api_t;

/*!
 * @} 
 * \addtogroup  G_LLD
 * @{
 */

/*------------------------------------------------------------------------------------------------*/
/* LLD interface functions                                                                        */
/*------------------------------------------------------------------------------------------------*/

/*! \brief      Notifies the LLD to start transmitting and receiving messages
 *  \param      api_ptr         Reference to MOST NetServices LLD interface
 *  \param      ns_ptr          Reference to internal MOST NetServices handler
 */
typedef void (*Mns_Lld_StartCb_t)(Mns_Lld_Api_t* api_ptr, void *ns_ptr, void *inst_ptr);

/*! \brief      Notifies the LLD to stop/abort transmitting and receiving messages
 *  \details    As soon as this function is called the low-level driver is not allowed 
 *              to call any MOST NetServices function.
 */
typedef void (*Mns_Lld_StopCb_t)(void *inst_ptr);

/*! \brief      Notifies the LLD to reset the INIC
 *  \details    If this function is called the low-level driver is responsible to 
 *              perform an INIC hardware reset.
 */
typedef void (*Mns_Lld_ResetInicCb_t)(void *inst_ptr);

/*! \brief      Callback function which is invoked as soon as port message objects are available again.
 *  \details    By implementing this callback function the low-level driver can avoid polling for 
 *              Rx message objects. The low-level driver should wait for the function call as soon 
 *              as Mns_Lld_RxAllocateCb_t() returns NULL. Only then it shall call those functions again.
 */
typedef void (*Mns_Lld_RxMsgAvailableCb_t)(void *inst_ptr);

/*! \brief      Callback function which is invoked to transmit a single message to the INIC
  * \param      msg_ptr     Reference to a single Tx message.
  */
typedef void (*Mns_Lld_TxTransmitCb_t)(Mns_Lld_TxMsg_t *msg_ptr, void *inst_ptr);

/*!
 * @} 
 * \addtogroup  G_LLD_TYPES
 * @{
 */

/*! \brief  Set of functions implemented by the low-level driver
 */
typedef struct Mns_Lld_Callbacks_
{
    Mns_Lld_StartCb_t          start_fptr;         /*!< \brief    Callback function to initialize the low-level driver and 
                                                    *             start the transmission and reception of messages */
    Mns_Lld_StopCb_t           stop_fptr;          /*!< \brief    Callback function to stop/abort the transmission and reception of messages */
    Mns_Lld_RxMsgAvailableCb_t rx_available_fptr;  /*!< \brief    Callback function which is invoked as soon as Rx message objects are available again */
    Mns_Lld_TxTransmitCb_t     tx_transmit_fptr;   /*!< \brief    Callback function to transmit one or multiple messages to the INIC */

} Mns_Lld_Callbacks_t;

/*! @} */

#ifdef __cplusplus
}                                                   /* extern "C" */
#endif

#endif /* #ifndef MNS_LLD_PB_H */

/*------------------------------------------------------------------------------------------------*/
/* End of file                                                                                    */
/*------------------------------------------------------------------------------------------------*/

