/*
 * MOST NetServices "Light" V3.2.7.0.1796 MultiInstance Patch
 *
 * Copyright (C) 2015 Microchip Technology Germany II GmbH & Co. KG
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * You may also obtain this software under a propriety license from Microchip.
 * Please contact Microchip for further information.
 *
 */

/*!
 * \file
 * \brief Declaration of public message types
 */

#ifndef MNS_MESSAGE_PB_H
#define MNS_MESSAGE_PB_H

#ifdef __cplusplus
extern "C"
{
#endif

/*!
 * \addtogroup G_MNS_AMS
 * @{
 */
/*------------------------------------------------------------------------------------------------*/
/* Defines                                                                                        */
/*------------------------------------------------------------------------------------------------*/
#define MNS_ADDR_INTERNAL                0x0000U  /*!< \brief   Internal transmission destination address
                                                   *   \details Can be used for internal message transmission
                                                   *            to avoid possible race conditions during 
                                                   *            recalculation of the own node address.
                                                   */
#define MNS_ADDR_BROADCAST_BLOCKING     0x03C8U  /*!< \brief Blocking broadcast destination address */
#define MNS_ADDR_BROADCAST_UNBLOCKING   0x03FFU  /*!< \brief Unblocking broadcast destination address */
#define MNS_ADDR_DEBUG                   0x0FF0U  /*!< \brief Optional debug destination address */
/*! @} */

/*------------------------------------------------------------------------------------------------*/
/* Types                                                                                          */
/*------------------------------------------------------------------------------------------------*/
/*! \brief   Message transmission status for internal/debug use
 *  \ingroup G_MNS_MISC_RET_RES
 */
typedef enum Mns_MsgTxStatus_
{
    MNS_MSG_STAT_OK                     = 0x00U, /*!< \brief Transmission succeeded */
    MNS_MSG_STAT_ERROR_CFG_NO_RCVR      = 0x01U, /*!< \brief No internal receiver exists */
    MNS_MSG_STAT_ERROR_BF               = 0x08U, /*!< \brief Buffer full */ 
    MNS_MSG_STAT_ERROR_CRC              = 0x09U, /*!< \brief CRC */ 
    MNS_MSG_STAT_ERROR_ID               = 0x0AU, /*!< \brief Corrupted identifiers */ 
    MNS_MSG_STAT_ERROR_ACK              = 0x0BU, /*!< \brief Corrupted PACK or CACK */ 
    MNS_MSG_STAT_ERROR_TIMEOUT          = 0x0CU, /*!< \brief TX timeout */
    MNS_MSG_STAT_ERROR_FATAL_WT         = 0x10U, /*!< \brief Wrong target */ 
    MNS_MSG_STAT_ERROR_FATAL_OA         = 0x11U, /*!< \brief Own node address */
    MNS_MSG_STAT_ERROR_NA_TRANS         = 0x18U, /*!< \brief Control channel was switched off and 
                                                  *          a pending transmission was canceled */ 
    MNS_MSG_STAT_ERROR_NA_OFF           = 0x19U, /*!< \brief Control channel not available */
    MNS_MSG_STAT_ERROR_UNKNOWN          = 0xFEU, /*!< \brief Unknown error status */
    MNS_MSG_STAT_ERROR_SYNC             = 0xFFU  /*!< \brief Internal error which is notified if 
                                                  *          communication link with INIC is lost
                                                  */
} Mns_MsgTxStatus_t;

/*! \brief   Operation Types 
 *  \ingroup G_MNS_AMS_TYPES
 */
typedef enum Mns_OpType_
{
    MNS_OP_SET              = 0x0,  /*!< \brief Operation Set (Property) */
    MNS_OP_GET              = 0x1,  /*!< \brief Operation Get (Property) */
    MNS_OP_SETGET           = 0x2,  /*!< \brief Operation SetGet (Property) */
    MNS_OP_INC              = 0x3,  /*!< \brief Operation Increment (Property) */
    MNS_OP_DEC              = 0x4,  /*!< \brief Operation Decrement (Property) */
    MNS_OP_STATUS           = 0xC,  /*!< \brief Operation Status (Property) */

    MNS_OP_START            = 0x0,  /*!< \brief Operation Start (Method) */
    MNS_OP_ABORT            = 0x1,  /*!< \brief Operation Abort (Method) */
    MNS_OP_STARTRESULT      = 0x2,  /*!< \brief Operation StartResult (Method) */
    MNS_OP_PROCESSING       = 0xB,  /*!< \brief Operation Processing (Method) */
    MNS_OP_RESULT           = 0xC,  /*!< \brief Operation Result (Method) */

    MNS_OP_STARTACK         = 0x8,  /*!< \brief Operation StartAck (Method) */
    MNS_OP_ABORTACK         = 0x7,  /*!< \brief Operation AbortAck (Method) */
    MNS_OP_STARTRESULTACK   = 0x6,  /*!< \brief Operation StartResultAck (Method) */
    MNS_OP_PROCESSINGACK    = 0xA,  /*!< \brief Operation ProcessingAck (Method) */
    MNS_OP_RESULTACK        = 0xD,  /*!< \brief Operation ResultAck (Method) */

    MNS_OP_GETINTERFACE     = 0x5,  /*!< \brief Operation GetInterface (Property/Method) */
    MNS_OP_INTERFACE        = 0xE,  /*!< \brief Operation Interface (Property/Method) */
    MNS_OP_ERROR            = 0xF,  /*!< \brief Operation Error (Property/Method) */
    MNS_OP_ERRORACK         = 0x9   /*!< \brief Operation ErrorAck (Property/Method) */

} Mns_OpType_t;

#ifdef __cplusplus
}                                   /* extern "C" */
#endif

#endif /* #ifndef MNS_MESSAGE_PB_H */


/*------------------------------------------------------------------------------------------------*/
/* End of file                                                                                    */
/*------------------------------------------------------------------------------------------------*/

