/*
 * MOST NetServices "Light" V3.2.7.0.1796 MultiInstance Patch
 *
 * Copyright (C) 2015 Microchip Technology Germany II GmbH & Co. KG
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * You may also obtain this software under a propriety license from Microchip.
 * Please contact Microchip for further information.
 *
 */

/*!
 * \file
 * \brief Internal header file of the trace interface
 */

#ifndef MNSL_TRACE_H
#define MNSL_TRACE_H

#ifdef __cplusplus
extern "C"
{
#endif

/*------------------------------------------------------------------------------------------------*/
/* Unit and entry ids                                                                             */
/*------------------------------------------------------------------------------------------------*/
#define TR_MNSL_ASSERT             "ASSERT failed in line %d"
#define TR_MNSL_INIC_RESULT_ID_1   "INIC error data:"
#define TR_MNSL_INIC_RESULT_ID_2   "--> Data[%u]: 0x%02X"

/*------------------------------------------------------------------------------------------------*/
/* Internal macros                                                                                */
/*------------------------------------------------------------------------------------------------*/

/* parasoft suppress MISRA2004-19_7 MISRA2004-19_4 reason "function-like macros are allowed for tracing" */
#ifdef MNS_TR_ERROR
#   define TR_ERROR(args) MNS_TR_ERROR args;
#   define TR_FAILED_ASSERT(mns_inst_id, unit) TR_ERROR(((mns_inst_id), (unit), TR_MNSL_ASSERT, 1U, __LINE__))
#   define TR_ASSERT(mns_inst_id, unit, expr)  if (!(expr)) {TR_FAILED_ASSERT((mns_inst_id), (unit));}
#   define TR_ERROR_INIC_RESULT(mns_inst_id, unit, info_ptr, info_size)                                  \
            {                                                                                       \
                uint8_t i;                                                                          \
                TR_ERROR(((mns_inst_id), (unit), TR_MNSL_INIC_RESULT_ID_1, 0U));                    \
                for(i=0U; i<info_size; i++)                                                              \
                {                                                                                   \
                    TR_ERROR(((mns_inst_id), (unit), TR_MNSL_INIC_RESULT_ID_2, 2U, i, info_ptr[i])) \
                }                                                                                   \
            }
#else
#   define MNS_TR_ERROR
#   define TR_ERROR(args)
#   define TR_FAILED_ASSERT(mns_inst_id, unit)
#   define TR_ASSERT(mns_inst_id, unit, expr)
#   define TR_ERROR_INIC_RESULT(mns_inst_id, unit, info_ptr, info_size)
#endif

#ifdef MNS_TR_INFO
#   define TR_INFO(args) MNS_TR_INFO args;
#else
#   define MNS_TR_INFO
#   define TR_INFO(args)
#endif
/* parasoft unsuppress item MISRA2004-19_7 item MISRA2004-19_4 reason "function-like macros are allowed for tracing" */

#ifdef __cplusplus
}       /* extern "C" */
#endif

#endif  /* #ifndef MNSL_TRACE_H */

/*------------------------------------------------------------------------------------------------*/
/* End of file                                                                                    */
/*------------------------------------------------------------------------------------------------*/

