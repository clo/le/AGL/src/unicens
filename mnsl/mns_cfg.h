/*
 * MOST NetServices "Light" V3.2.7.0.1796 MultiInstance Patch
 *
 * Copyright (C) 2015 Microchip Technology Germany II GmbH & Co. KG
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * You may also obtain this software under a propriety license from Microchip.
 * Please contact Microchip for further information.
 *
 */

/*!
 * \file
 * \brief Configuration header file of MOST NetServices Light
 */

#ifndef MNSL_CFG_H
#define MNSL_CFG_H

#ifdef __cplusplus
extern "C"
{
#endif

/* File only needed for other includes. */
#include "mns_types_cfg.h"

/*------------------------------------------------------------------------------------------------*/
/* Message Pool                                                                                   */
/*------------------------------------------------------------------------------------------------*/
/* Sets the number of pre-allocated Rx messages which are shared by all FIFOs. Default value: 35*/
/* #define MNSL_CHANNEL_POOL_SIZE_RX 35 */

/*------------------------------------------------------------------------------------------------*/
/* Tracing & Debugging                                                                            */
/*------------------------------------------------------------------------------------------------*/
/* Define the following macros to map info and error trace output to user defined functions. 
 * The purpose of these functions is debugging. It is not recommended to define these functions 
 * in a production system.
 */
#define MNS_TR_ERROR     My_TraceError
// #define MNS_TR_INFO      My_TraceInfo

extern void My_TraceError(uint8_t mns_inst_id, const char module_str[], const char entry_str[], uint16_t vargs_cnt, ...);
extern void My_TraceInfo(uint8_t mns_inst_id, const char module_str[], const char entry_str[], uint16_t vargs_cnt, ...);

#ifdef __cplusplus
}   /* extern "C" */
#endif

#endif  /* #ifndef MNSL_CFG_H */

/*------------------------------------------------------------------------------------------------*/
/* End of file                                                                                    */
/*------------------------------------------------------------------------------------------------*/
