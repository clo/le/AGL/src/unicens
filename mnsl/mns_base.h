/*
 * MOST NetServices "Light" V3.2.7.0.1796 MultiInstance Patch
 *
 * Copyright (C) 2015 Microchip Technology Germany II GmbH & Co. KG
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * You may also obtain this software under a propriety license from Microchip.
 * Please contact Microchip for further information.
 *
 */

/*!
 * \file
 * \brief Internal header file of the Base class.
 *
 * \cond MNS_INTERNAL_DOC
 * \addtogroup G_BASE
 * @{
 */

#ifndef MNS_BASE_H
#define MNS_BASE_H

/*------------------------------------------------------------------------------------------------*/
/* Includes                                                                                       */
/*------------------------------------------------------------------------------------------------*/
#include "mns_cfg.h"
#include "mns_timer.h"
#include "mns_scheduler.h"
#include "mns_trace.h"
#include "mns_eh.h"
#include "mns_alm.h"

#ifdef __cplusplus
extern "C"
{
#endif

/*------------------------------------------------------------------------------------------------*/
/* Structures                                                                                     */
/*------------------------------------------------------------------------------------------------*/
/*! \brief  Initialization structure of the Base class. */
typedef struct Base_InitData_
{
    Scd_InitData_t scd;             /*!< \brief Initialization data of the scheduler */
    Tm_InitData_t tm;               /*!< \brief Initialization data of the timer management */
    uint8_t mns_inst_id;            /*!< \brief MOST NetServices instance ID */

} Base_InitData_t;

/*! \brief   Class structure of the Base class. */
typedef struct CBase_
{
    CScheduler scd;                 /*!< \brief Scheduler instance */
    CTimerManagement tm;            /*!< \brief Timer management instance */
    CEventHandler eh;               /*!< \brief Event handler instance */
    CApiLockingManager alm;         /*!< \brief API locking manager instance */
    uint8_t mns_inst_id;            /*!< \brief MOST NetServices instance ID */

} CBase;

/*------------------------------------------------------------------------------------------------*/
/* Prototypes                                                                                     */
/*------------------------------------------------------------------------------------------------*/
void Base_Ctor(CBase *self, Base_InitData_t *init_ptr);

#ifdef __cplusplus
}   /* extern "C" */
#endif

#endif  /* #ifndef MNS_BASE_H */

/*!
 * @}
 * \endcond
 */

/*------------------------------------------------------------------------------------------------*/
/* End of file                                                                                    */
/*------------------------------------------------------------------------------------------------*/

